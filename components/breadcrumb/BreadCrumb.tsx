import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styles from "./BreadCrumb.module.scss";

const BreadCrumb = () => {
  const router = useRouter();
  const [breadcrumbs, setBreadcrumbs] = useState<string[] | null>(null);

  useEffect(() => {
    if (router) {
      const linkPath = router.asPath
        .split("&")[0]
        .split("/")
        .map((link) => decodeURIComponent(link).replaceAll("-", " "));

      setBreadcrumbs(linkPath);
    }
  }, [router]);

  return (
    <div className={styles.wrapperBreadCrumb}>
      {breadcrumbs?.map((breadcrumb, index) => (
        <span
          key={index}
          className={clsx(
            "pr-2 text-[#8d8d8d] text-[0.875rem] last:text-[#222222]",
            styles.breadcrumb
          )}
        >
          {breadcrumb === "" ? (
            <Link href="/">Home</Link>
          ) : (
            <span className="capitalize">{breadcrumb}</span>
          )}
        </span>
      ))}
    </div>
  );
};

export default BreadCrumb;
