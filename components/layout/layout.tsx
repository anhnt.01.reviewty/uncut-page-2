import { useRouter } from "next/router";
import { Props } from "next/script";
import { FC, useRef } from "react";
import Header from "../Header";

const Layout: FC<Props> = ({ children }) => {
  const ref = useRef(null);
  const router = useRouter();

  return (
    <div className="w-screen" ref={ref}>
      <div className="section">
        <Header />
      </div>
      {children}
      <div>footer</div>
    </div>
  );
};
export default Layout;
