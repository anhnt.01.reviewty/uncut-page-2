const InprogressBar = (props: { bgcolor: string; completed: number }) => {
  const { bgcolor, completed } = props;

  const containerStyles = {
    height: 6,
    width: "100%",
    backgroundColor: "#e0e0de",
    borderRadius: 50,
    // margin: 16,
  };

  const fillerStyles = {
    height: "100%",
    width: `${completed}%`,
    backgroundColor: bgcolor,
    borderRadius: "inherit",
  };

  return (
    <div className="relative" style={containerStyles}>
      <div className="" style={fillerStyles}>
        <span className="absolute right-0 -top-6 padding-1 text-[#8d8d8d] ">{`${completed}%`}</span>
      </div>
    </div>
  );
};

export default InprogressBar;
