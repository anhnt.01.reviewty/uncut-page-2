import Image from "next/legacy/image";
import { useRouter } from "next/router";
import { Props } from "next/script";
import { FC, useRef } from "react";
import reviewtyImg from "@public/reviewty.png";
import Link from "next/link";
import {
  LoginIconHeader,
  MyPageIcon,
  NavigationMobileIcon,
  WishListIconHeader,
} from "@/assets/icon/icons";
import styles from "./Header.module.scss";

const Header: FC<Props> = ({ children }) => {
  const ref = useRef(null);
  const router = useRouter();

  const navigation = [
    {
      icon: <MyPageIcon />,
      label: "my page",
      href: "/my-page",
    },
    {
      icon: <WishListIconHeader />,
      href: "/my-like",
      label: "my like",
    },
    {
      icon: <LoginIconHeader />,
      href: "/login",
      label: "login",
    },
  ];

  return (
    <div className="w-screen md:p-4 p-4 pl-0" ref={ref}>
      <div className="flex justify-between font-medium items-center max-w-screen-xl m-auto">
        <div className={styles.mainImg}>
          <Link href="/">
            <Image src={reviewtyImg} alt="main-logo" objectFit="contain" />
          </Link>
        </div>

        <div className="hidden md:flex pb-2">
          {navigation.map((nav, index) => {
            return (
              <Link key={index} href={nav.href}>
                <div className="flex px-3 items-center">
                  <div className="pr-2">{nav.icon}</div>

                  <div className="uppercase text-[0.875rem] text-[#404040]">
                    {nav.label}
                  </div>
                </div>
              </Link>
            );
          })}
        </div>

        <div className="block md:hidden pb-2">
          <NavigationMobileIcon />
        </div>
      </div>
    </div>
  );
};
export default Header;
