const toCurrency = (price: number): string => {
  // return price.toLocaleString("vi", { style: "currency", currency: "VND" });
  return price?.toLocaleString("en-US") + "đ";
};
export default toCurrency;
