import "../public/fonts/styles.css";
import "../app/globals.scss";
import Layout from "@/components/layout";

import { ApolloProvider } from "@apollo/client";
import { useApollo } from "@/hooks/useApollo";

// import { Layout } from "../components/common";

function MyApp({ Component, pageProps }: any) {
  const apolloClient = useApollo(pageProps.initialApolloState);

  return (
    <ApolloProvider client={apolloClient}>
      <Layout>
        <Component />
      </Layout>
    </ApolloProvider>
  );
}

export default MyApp;
