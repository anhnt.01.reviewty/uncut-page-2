import mainImg2 from "@public/banner-test-01.jpg";
import mainImg3 from "@public/banner-test-02.jpg";
import mainImg4 from "@public/banner-test-03.jpg";
import minImg from "@public/exam/min-1.jpeg";
import minImg2 from "@public/exam/min-2.jpeg";
import minImg3 from "@public/exam/min-3.jpeg";
import minImg4 from "@public/exam/min-4.jpeg";
import minImg5 from "@public/exam/min-5.jpeg";
import minImg6 from "@public/exam/min-6.jpeg";
import mainImg from "@public/main-banner.png";
import { NextPage } from "next";
import Image from "next/legacy/image";

import { useEffect } from "react";

import "swiper/css";
import "swiper/css/pagination";

import { useMemo } from "react";
import "../app/globals.scss";

import { FilterIcon, WishlistIcon } from "@/assets/icon/icons";
import { handleDiffDate } from "@/ultis/ultis";
import { clsx } from "clsx";
import Link from "next/link";

import toCurrency from "@/common/toCurrency";
import {
  FundingStatus,
  OrderBy,
  useFundingsLazyQuery,
} from "@/graphql/reviewty/graphql";

import categories2 from "@public/categories.svg";
import wishlistIcon2 from "@public/wishlist-2.svg";

const smallImg = [minImg, minImg5, minImg3, minImg2, minImg6, minImg4];

const Home: NextPage = () => {
  const [getFundings, { data, loading, variables }] = useFundingsLazyQuery({
    variables: {
      orderBy: {
        id: OrderBy.Asc,
      },
    },
  });

  useEffect(() => {
    getFundings();
  }, [getFundings]);
  const barItems = useMemo(() => {
    return [
      {
        id: "all",
        label: "Tất cả",
        status: undefined,
      },
      {
        id: "inprogress",
        label: "Đang diễn ra",
        status: FundingStatus.Running,
      },
      {
        id: "up-comming",
        label: "Sắp diễn ra",
        status: FundingStatus.Created,
      },
      {
        id: "took-place",
        label: "Đã diễn ra",
        status: FundingStatus.Stopped,
      },
    ];
  }, []);

  const a = [
    { src: mainImg },
    { src: mainImg2 },
    { src: mainImg3 },
    { src: mainImg4 },
  ];

  const handleClickBarITems = (statusFilter?: FundingStatus) => {
    getFundings({
      variables: {
        where: {
          statusFilter: statusFilter,
        },
      },
    });
  };

  return (
    <div>
      <div className="w-full">
        {/* <Swiper
          pagination={{
            type: "progressbar",
          }}
          navigation={{
            nextEl: ".next-btn",
            prevEl: ".prev-btn",
          }}
          modules={[Pagination, Navigation]}
          className={styles.mySwiper}
          // cssMode={true}
          loop={true}
        >
          {a.map((item, index) => {
            return (
              <SwiperSlide key={Math.random()}>
                <Image src={item.src} layout="responsive" alt="main-img" />
              </SwiperSlide>
            );
          })}

          <div className="absolute right-[30%] bottom-[10%]">
            <div
              className={clsx(
                "absolute prev-btn left-0 top-1/2 -translate-y-1/2 z-10",
                styles.prevBtn
              )}
            >
              <BtnNextSlider className="rotate-180 w-9 h-9 cursor-pointer" />
            </div>

            <div
              className={clsx(
                "absolute next-btn left-9 top-1/2 -translate-y-1/2 z-10",
                styles.nextBtn
              )}
            >
              <BtnPrevSlider className="w-9 h-9 cursor-pointer" />
            </div>
          </div>
        </Swiper> */}
        <div className="w-full overflow-x-auto">
          <div className="sm:hidden min-w-[450px] cursor-pointer grid grid-cols-12">
            {barItems.map((item, index) => (
              <span
                key={item.id}
                onClick={() => {
                  handleClickBarITems(item.status);
                }}
                className={clsx(
                  "sm:p-3 cursor-pointer text-[1.125rem] scrollbar py-2 col-span-3 text-center border-b-2",
                  {
                    "text-[#BA92D9] border-b-2 border-b-[#BA92D9]":
                      item.status === variables?.where?.statusFilter,
                    "border-b-[#ececec]":
                      item.status !== variables?.where?.statusFilter,
                  }
                )}
              >
                {item.label}
              </span>
            ))}
          </div>

          <div className="hidden sm:flex  max-w-screen-xl m-auto justify-end py-4">
            <div className="flex">
              <FilterIcon /> <span className="px-1">Filter:</span>
            </div>

            {barItems.map((item, index) => (
              <span
                key={item.id}
                onClick={() => {
                  handleClickBarITems(item.status);
                }}
                className={clsx(
                  "cursor-pointer px-4 text-[#8d8d8d] border-r-2",
                  {
                    "text-[#BA92D9]":
                      item.status === variables?.where?.statusFilter,

                    "border-r-[transparent]": index === barItems.length - 1,
                  }
                )}
              >
                {item.label}
              </span>
            ))}
          </div>
        </div>

        <div className="max-w-screen-xl grid grid-cols-12 gap-4 m-auto">
          {data?.fundings.map((item, index) => (
            <Link
              href={`funding/${item.shortDescription.split(" ").join("-")}&${
                item.id
              }`}
              key={item.id}
              className="col-span-12 grid grid-cols-12 gap-2 sm:col-span-6  md:col-span-4"
            >
              <div className="col-span-12  md:col-span-12">
                <div className="relative">
                  {/* <div
                    className=""
                    style={{
                      borderRadius: "10px",
                      minHeight: "300px",
                      background: `url(${item.coverUrl})`,
                      backgroundSize: "cover",
                      backgroundRepeat: "no-repeat",
                      backgroundPosition: "center center",
                    }}
                  /> */}

                  <div className="">
                    {item.coverUrl === "" ? (
                      ""
                    ) : JSON.parse(item.coverUrl)?.[0].url?.includes(".mp4") ? (
                      <video
                        autoPlay
                        muted
                        loop
                        className="w-full md:w-[98.7%]  h-auto block md:rounded-lg"
                      >
                        <source src={JSON.parse(item.coverUrl)?.[0].url} />
                      </video>
                    ) : (
                      <Image
                        src={JSON.parse(item.coverUrl)?.[0]?.url}
                        width="0"
                        height="0"
                        sizes="100vw"
                        className="w-full h-auto block md:rounded-lg"
                        alt="img"
                        layout="responsive"
                      />
                    )}
                  </div>

                  {/* <div
                    className="absolute right-3 top-3"
                    onClick={(e) => {
                      alert("added");
                      e.preventDefault();
                    }}
                  >
                    <WishlistIcon />
                  </div> */}

                  <div
                    className="absolute right-2 bottom-2 rounded-md text-[white] px-2 py-1 block text-[12px] md:text-[0.875rem] sm:hidden"
                    style={{ background: "rgba(0,0,0,0.3)" }}
                  >
                    {handleDiffDate(item.endDate).daysDiff} days left
                  </div>
                </div>
              </div>
              <div className="col-span-12 md:col-span-12  md:h-[10rem]">
                <div className="md:hidden flex flex-col justify-around h-[100%] p-3 ">
                  <span className="text-elipsis text-[27px] leading-[2rem] tracking-[-1.5px] mb-2 font-medium text-[#222222]">
                    {item.shortDescription}
                  </span>
                  <label className="text-[#7b7a7a] ">
                    {item.product.brand.translations?.[0].name}
                  </label>
                  <p className="text-[0.875rem] text-[#8D8D8D]  pb-[1.125rem]"></p>

                  {/* <span className="relative font-semibold  text-[1rem] text-[#005A53]">
                    Chỉ từ {toCurrency(item.salePrice!)}
                    <span className="absolute ml-2 top-0 line-through text-[0.625rem] text-[#8D8D8D]">
                      {toCurrency(item.product.price!)}
                    </span>
                  </span> */}
                  <div className="grid grid-cols-12 gap-2 relative">
                    <span className="absolute right-0 top-[13px]">
                      <Image src={wishlistIcon2} alt="wishlist-2" />
                    </span>

                    <span className="absolute right-0 top-[49px]">
                      <Image src={categories2} alt="categories-2" />
                    </span>

                    <div
                      className="col-span-3 "
                      style={{
                        borderRadius: "5px",
                      }}
                    >
                      <Image
                        style={{
                          border: "1px solid green",
                          borderRadius: "5px",
                        }}
                        src={smallImg[index]}
                        width="0"
                        height="0"
                        sizes="100vw"
                        layout="responsive"
                        alt="img"
                      />
                    </div>
                    <div className="col-span-9 flex flex-col justify-evenly">
                      <div className="mr-6 text-elipsis text-[gray]">
                        <span className="text-black">
                          [{item.product.brand.translations[0].name}]
                        </span>{" "}
                        {item.product.translations[0].name}
                      </div>

                      <div className="flex">
                        <span className="mr-1 text-[#D10017]  text-[13px]">
                          {" "}
                          {Math.floor(
                            ((item.product.price! - item.salePrice) /
                              item.product.price!) *
                              100
                          ) + "%"}
                        </span>
                        <p className="mr-1 text-[#222222] text-[13px] font-semibold">
                          {toCurrency(item.salePrice!)}
                        </p>
                        <p className="mr-1 line-through text-[#CCCCCC]  text-[11px]">
                          {toCurrency(item.product.price!)}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="hidden md:flex h-[100%] flex-col justify-around sm:justify-start ">
                  <span className="relative font-semibold text-[1.5rem] pb-4 text-[#005A53]">
                    Chỉ từ {toCurrency(item.salePrice)}
                    <span className="absolute ml-2 top-0 line-through text-[1rem] text-[#8D8D8D]">
                      {toCurrency(item.product.price!)}
                    </span>
                    <div
                      className="absolute right-1  top-0 rounded-md text-[0.875rem] font-normal px-2 py-1"
                      style={{ background: "rgba(0,90,83,0.06)" }}
                    >
                      {handleDiffDate(item.endDate).daysDiff} days left
                    </div>
                  </span>

                  <p className="text-[1rem] font-medium text-elipsis text-[#222222]">
                    {item.shortDescription}
                  </p>

                  <p className="text-[0.875rem] font-normal text-[#99a1a8]">
                    {item.product.brand.translations?.[0].name}
                  </p>
                </div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
