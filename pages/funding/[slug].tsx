import Breadcrumb from "@/components/breadcrumb";
import Image from "next/legacy/image";
import ReactDOMServer from "react-dom/server";

import { useEffect, useLayoutEffect } from "react";
import styles from "./funding-detail.module.scss";
// import present from "@public/present.svg";

import { SwiperSlide } from "swiper/react";
import {
  BtnNextSlider,
  BtnPrevSlider,
  ShareIcon,
  WishListIconFill,
} from "@/assets/icon/icons";
import InprogressBar from "@/components/inprogressbar/Inprogressbar";
import { useFundingDetailLazyQuery } from "@/graphql/reviewty/graphql";
import peacholic from "@public/brands/peacholic.png";
import webpImg from "@public/TV.FreshUpLightSunMilk1.webp";
import clsx from "clsx";
import { useRouter } from "next/router";
import { Navigation, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper } from "swiper/react";
import { handleDiffDate } from "@/ultis/ultis";
import toCurrency from "@/common/toCurrency";

const FundingDetails = () => {
  const router = useRouter();
  const [getDetails, { data, loading, error }] = useFundingDetailLazyQuery({
    variables: {
      where: {
        id: Number(router.asPath.split("&")[1]),
      },
    },
  });

  useLayoutEffect(() => {
    console.log(router.asPath.split("&")[1]);

    if (Number(router.asPath.split("&")[1])) {
      getDetails();
    }
  }, [getDetails, router.asPath]);
  console.log(data);

  return (
    <div className="max-w-screen-xl relative m-auto h-[1800px]">
      <div className="pl-4">
        <Breadcrumb />
      </div>

      {!loading ? (
        <div className="grid grid-cols-12 gap-4 pt-4">
          <div className="col-span-12  md:col-span-6 md:rounded-lg overflow-hidden">
            <div className="relative">
              <Swiper
                modules={[Pagination, Navigation]}
                loop={true}
                pagination={{
                  type: "custom",
                  renderCustom(swiper, current, total) {
                    return ReactDOMServer.renderToString(
                      <span
                        key={Math.random()}
                        className={clsx(
                          "absolute right-3 bottom-4 py-1 px-3 rounded-xl",
                          styles.customPagination
                        )}
                      >
                        <span className={styles.fraction__current}>
                          {current}{" "}
                        </span>{" "}
                        / <span>{total}</span>
                      </span>
                    );
                  },
                }}
                navigation={{
                  nextEl: ".next-btn",
                  prevEl: ".prev-btn",
                }}
              >
                <div
                  className={clsx(
                    "absolute next-btn right-0 top-1/2 -translate-y-1/2 z-10",
                    styles.nextBtn,
                    {
                      hidden: true,
                    }
                  )}
                >
                  <BtnPrevSlider className="w-9 h-9 cursor-pointer" />
                </div>

                <div
                  className={clsx(
                    "absolute prev-btn left-0 top-1/2 -translate-y-1/2 z-10",
                    styles.prevBtn,
                    {
                      hidden: true,
                    }
                  )}
                >
                  <BtnNextSlider className="rotate-180 w-9 h-9 cursor-pointer" />
                </div>
                {[data?.funding].map((content) => {
                  if (content) {
                    return (
                      <SwiperSlide key={content?.id}>
                        <div>
                          {content?.coverUrl === "" ? (
                            ""
                          ) : JSON.parse(content?.coverUrl!)?.[0].url?.includes(
                              ".mp4"
                            ) ? (
                            <video
                              autoPlay
                              muted
                              loop
                              className="w-full md:w-[98.7%]  h-auto block md:rounded-lg"
                            >
                              <source
                                src={JSON.parse(content?.coverUrl!)?.[0].url}
                              />
                            </video>
                          ) : (
                            <Image
                              src={JSON.parse(content?.coverUrl!)?.[0]?.url}
                              width="0"
                              height="0"
                              sizes="100vw"
                              className="w-full h-auto block md:rounded-lg"
                              alt="img"
                              layout="responsive"
                            />
                          )}
                        </div>
                      </SwiperSlide>
                    );
                  }

                  return;
                })}
              </Swiper>

              <div className="hidden sm:block mt-9 sm:p-0 col-span-12 md:col-span-6">
                <p className=" text-[#222222] pl-4 font-semibold text-[1.25rem] pb-4">
                  Project Story
                </p>
                <div
                  dangerouslySetInnerHTML={{
                    __html: data?.funding?.longDescription || "",
                  }}
                ></div>
              </div>
            </div>
          </div>
          <div className="relative p-4 col-span-12 md:col-span-6 h-[100%]] ">
            <div className="flex flex-col sticky  top-20 col-span-12">
              <h2 className="text-[1.25rem] font-semibold pb-4 text-[#222222]">
                {data?.funding.shortDescription}
              </h2>

              <div className="relative pb-5 text-[#005A53]">
                <span className="text-[1.5rem] font-semibold pr-1">12</span>
                <span className="text-[0.875rem] ">Người tham gia</span>
                <label
                  className="absolute ml-6  rounded-md px-2 py-1 right-0 md:right-auto"
                  style={{ background: "rgba(0,90,83,0.06)" }}
                >
                  {handleDiffDate(data?.funding.endDate).daysDiff} days left
                </label>
              </div>

              <span className="relative font-semibold text-[1.5rem] pb-10 text-[#222222]">
                Chỉ từ {toCurrency(data?.funding.salePrice!)}
                <span className="absolute ml-2 top-0 line-through text-[1rem] text-[#8D8D8D]">
                  {toCurrency(data?.funding.product.price!)}
                </span>
              </span>

              <div
                className="rounded-lg overflow-hidden mb-8 border-[0.2px] border-[#ececec] "
                // style={{ border: "0.2px solid #D99292" }}
              >
                <div className="p-4 font-medium uppercase bg-[#fedede] text-[#d10017]">
                  freeship
                </div>
                <div className="p-4 pt-8">
                  <InprogressBar completed={60} bgcolor={"red"} />
                  <div className="text-center pt-4">
                    Chỉ còn{" "}
                    <span className="font-semibold text-[0.875rem]">80</span>{" "}
                    lượt mua được
                    <span className="text-[0.875rem] text-[#d10017] font-semibold">
                      {" "}
                      FREESHIP!
                    </span>
                  </div>
                </div>
              </div>

              <div className="flex items-center pb-4">
                <div className={clsx("cursor-pointer", styles.btn)}>
                  <WishListIconFill />
                </div>
                <div className={clsx("cursor-pointer", styles.btn)}>
                  <ShareIcon />
                </div>

                <div className={clsx("cursor-pointer", styles.buyNow)}>
                  Đặt mua ngay
                </div>
              </div>

              <div className="block w-full sm:hidden col-span-12">
                <div className={styles.brandName}>
                  <div className="flex justify-center items-center">
                    <div
                      style={{
                        border: "1px solid #BA92D9",
                        borderRadius: "50%",
                        width: "56px",
                        height: "56px",
                      }}
                    >
                      <Image
                        style={{ border: "1px solid red", borderRadius: "50%" }}
                        // src={data?.funding.product.brand.logoUrl!}
                        src={peacholic}
                        width={56}
                        height={56}
                        alt="brand"
                      />
                    </div>
                    <span className="pl-4 text-[1.125rem] font-medium capitalize text-[#222222]">
                      peacholic
                      {/* {data?.funding.product.brand.translations[0].name} */}
                    </span>
                  </div>
                  <div className={styles.brandHome}>Brand Home</div>
                </div>
              </div>

              <div className="hidden absolute -bottom-[25%] w-full sm:block col-span-12">
                <div className={styles.brandName}>
                  <div className="flex justify-center items-center">
                    <span
                      className=""
                      style={{
                        border: "1px solid #BA92D9",
                        borderRadius: "50%",
                        width: "72px",
                        height: "72px",
                      }}
                    >
                      <Image
                        style={{ border: "1px solid red", borderRadius: "50%" }}
                        // src={data?.funding.product.brand.logoUrl!}
                        src={peacholic}
                        width={72}
                        height={72}
                        alt="brand"
                      />
                    </span>

                    <span className="pl-4">
                      {data?.funding.product.brand.translations[0].name}
                    </span>
                  </div>
                  <div className={styles.brandHome}>Brand Home</div>
                </div>
              </div>
            </div>
          </div>

          <div className="block sm:hidden sm:p-0 col-span-12 md:col-span-6">
            <p className=" text-[#222222] pl-4 font-semibold text-[1.25rem] pb-4">
              Project Story
            </p>
            <div>
              <div
                dangerouslySetInnerHTML={{
                  __html: data?.funding?.longDescription || "",
                }}
              ></div>
              {/* <Image src={webpImg} alt="main-content-img" /> */}
            </div>
          </div>
        </div>
      ) : (
        "loading"
      )}
    </div>
  );
};

// interface FundingDetailsStaticProps {
//   // product?: GetProductDetailsQuery['product'];
//   funding?: FundingsQuery["fundings"][0];
// }

// export interface FundingDetailsProps
//   extends InferGetStaticPropsType<typeof getStaticProps> {}

// export const getStaticPaths: GetStaticPaths = async () => {
//   return {
//     paths: [],
//     fallback: true,
//   };
// };

// export const getStaticProps: GetStaticProps<
//   FundingDetailsStaticProps,
//   { slug: string }
// > = async (context) => {
//   const { slug } = context.params!;
//   const apolloClient = initializeApollo();

//   const { data } = await apolloClient.query<
//     FundingDetailQuery,
//     FundingDetailQueryVariables
//   >({
//     query: FundingDetailDocument,
//     variables: {
//       where: {
//         id: Number(slug.split("&")[1]),
//       },
//     },
//   });
//   console.log("12333", data.funding);

//   return {
//     props: {
//       funding: data.funding,
//       revalidate: 1,
//     },
//   };
// };

export default FundingDetails;
