export const handleDiffDate = (endDate = "2022-06-01") => {
  // Define the two dates (replace with your desired dates)

  const myDate = new Date();

  // Get the year, month, and day from the date object
  const year = myDate.getFullYear();
  const month = String(myDate.getMonth() + 1).padStart(2, "0");
  const day = String(myDate.getDate()).padStart(2, "0");

  // Combine the year, month, and day into a YYYY-MM-DD string
  const formattedDate = `${year}-${month}-${day}`;

  // Output the formatted date

  const date1 = new Date(formattedDate);
  const date2 = new Date(endDate);

  // Calculate the time difference in milliseconds
  const timeDiff = Math.abs(date2.getTime() - date1.getTime());

  // Calculate the number of days, hours, minutes, and seconds in the time difference
  const daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
  const hoursDiff = Math.floor((timeDiff / (1000 * 60 * 60)) % 24);
  const minutesDiff = Math.floor((timeDiff / (1000 * 60)) % 60);
  const secondsDiff = Math.floor((timeDiff / 1000) % 60);

  return { daysDiff, hoursDiff, minutesDiff, secondsDiff };

  // Output the time difference in a human-readable format
};

// Define the date object (replace with your desired date)
