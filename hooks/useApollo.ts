import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  NormalizedCache,
  concat,
} from "@apollo/client";
import merge from "deepmerge";
import isEqual from "lodash.isequal";
import { useMemo } from "react";

export const AUTH_TOKEN = "token";

// import { AUTH_TOKEN, VENDURE_HEADER_TOKEN } from "../components/auth/authSlice";

let apolloClient: ApolloClient<NormalizedCache>;

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      Authorization:
        typeof window === "undefined"
          ? null
          : localStorage.getItem(AUTH_TOKEN)
          ? `Bearer ${localStorage.getItem(AUTH_TOKEN)}`
          : null,
    },
  }));
  return forward(operation);
});

// const afterwareLink = new ApolloLink((operation, forward) => {
//   return forward(operation).map((response) => {
//     const context = operation.getContext();
//     const token = context.response.headers.get(VENDURE_HEADER_TOKEN);
//     if (token) {
//       localStorage.setItem(AUTH_TOKEN, token);
//     }
//     return response;
//   });
// });

// const mallLink = new HttpLink({
//   uri: process.env.NEXT_PUBLIC_MALL_GRAPHQL_API_ENDPOINT,
//   credentials: "same-origin",
// });

const reviewtyLink = new HttpLink({
  uri: process.env.NEXT_PUBLIC_REVIEWTY_GRAPHQL_API_ENDPOINT,
  credentials: "same-origin",
});

export const REVIEWTY_CLIENT = "reviewty";

function createApolloClient() {
  return new ApolloClient({
    ssrMode: typeof window === "undefined",
    link: reviewtyLink,
    cache: new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            reviews: {
              keyArgs: false,
              merge(existing = [], incoming) {
                return [...existing, ...incoming];
              },
            },
            products: {
              keyArgs: false,
              merge(existing, incoming) {
                if (!incoming) return existing;
                if (!existing) return incoming;

                const { items, ...rest } = incoming;
                let result = rest;
                result.items = [...existing.items, ...items];
                return result;
              },
            },
          },
        },
      },
    }),
  });
}

export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract();

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache, {
      // combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) =>
          sourceArray.every((s) => !isEqual(d, s))
        ),
      ],
    });

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export function useApollo(initialState = null) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
}
