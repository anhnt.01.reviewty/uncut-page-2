/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  experimental: {
    outputStandalone: true,
  },
  productionBrowserSourceMaps: true,
  // experimental: {
  //   appDir: true,
  // },
  images: {
    allowFutureImage: true,

    domains: [
      "image.oliveyoung.co.kr",
      "d27j9dvp3kyb6u.cloudfront.net",
      "d1ip8wajnedch4.cloudfront.net",
      "https://d9vmi5fxk1gsw.cloudfront.net",
    ],
  },
  // deviceSizes: [390, 640],
  // imageSizes: [16, 32, 48, 64, 96],
  minimumCacheTTL: 3600 * 24 * 30,
};

module.exports = nextConfig;
