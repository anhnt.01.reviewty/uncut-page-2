import gql from 'graphql-tag';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: any;
};

export enum ApiMethod {
  Delete = 'DELETE',
  Get = 'GET',
  Patch = 'PATCH',
  Post = 'POST',
  Put = 'PUT'
}

export enum AccountType {
  Organization = 'ORGANIZATION',
  Person = 'PERSON'
}

export type Address = {
  __typename?: 'Address';
  address: Scalars['String'];
  country: Scalars['String'];
  district: Scalars['String'];
  fullName: Scalars['String'];
  phoneNumber: Scalars['String'];
  province: Scalars['String'];
  ward: Scalars['String'];
};

export type Admin = {
  __typename?: 'Admin';
  accountName?: Maybe<Scalars['String']>;
  avatar?: Maybe<Image>;
  displayName?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  lastName?: Maybe<Scalars['String']>;
  phoneNumber: Scalars['String'];
  role: Scalars['String'];
  roleId?: Maybe<Scalars['Int']>;
  userPath?: Maybe<Scalars['String']>;
  userResources?: Maybe<Array<UserResource>>;
};

export type AdultRdi = {
  __typename?: 'AdultRdi';
  maxAge?: Maybe<Scalars['Int']>;
  minAge?: Maybe<Scalars['Int']>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type AdultRdiCreateInput = {
  maxAge?: InputMaybe<Scalars['Int']>;
  minAge?: InputMaybe<Scalars['Int']>;
  rdi?: InputMaybe<Scalars['Float']>;
  rdiUnit?: InputMaybe<Unit>;
  ul?: InputMaybe<Scalars['Float']>;
  ulUnit?: InputMaybe<Unit>;
};

export type AuthError = Error & {
  __typename?: 'AuthError';
  code: AuthErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum AuthErrorCode {
  AccountAlreadyInUse = 'ACCOUNT_ALREADY_IN_USE',
  AccountIsBlocked = 'ACCOUNT_IS_BLOCKED',
  AppleAlreadyInUse = 'APPLE_ALREADY_IN_USE',
  AppleNotExists = 'APPLE_NOT_EXISTS',
  EmailAlreadyInUse = 'EMAIL_ALREADY_IN_USE',
  FacebookAlreadyInUse = 'FACEBOOK_ALREADY_IN_USE',
  FacebookNotExists = 'FACEBOOK_NOT_EXISTS',
  InvalidReferralCode = 'INVALID_REFERRAL_CODE',
  OtpNotValid = 'OTP_NOT_VALID',
  ReferralCodeInvalid = 'REFERRAL_CODE_INVALID',
  Unauthorized = 'UNAUTHORIZED'
}

export type AuthPayload = {
  __typename?: 'AuthPayload';
  token: Scalars['String'];
  user: Admin;
};

export type BabyRdi = {
  __typename?: 'BabyRdi';
  maxMonth?: Maybe<Scalars['Int']>;
  minMonth?: Maybe<Scalars['Int']>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type BabyRdiCreateInput = {
  maxMonth?: InputMaybe<Scalars['Int']>;
  minMonth?: InputMaybe<Scalars['Int']>;
  rdi?: InputMaybe<Scalars['Float']>;
  rdiUnit?: InputMaybe<Unit>;
  ul?: InputMaybe<Scalars['Float']>;
  ulUnit?: InputMaybe<Unit>;
};

export type Banner = {
  __typename?: 'Banner';
  liveStreamId: Scalars['Int'];
  thumbnailUrl: Scalars['String'];
};

export enum BannerCategoriesType {
  BannerOnBar = 'BANNER_ON_BAR',
  BannerOnCommunity = 'BANNER_ON_COMMUNITY',
  BannerOnEvent = 'BANNER_ON_EVENT',
  BannerOnHomePage = 'BANNER_ON_HOME_PAGE',
  BannerOnProduct = 'BANNER_ON_PRODUCT'
}

export type BannerImage = {
  __typename?: 'BannerImage';
  bannerType: BannerCategoriesType;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUid?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  imageUrl?: Maybe<Scalars['String']>;
  redirectLink?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Boolean']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  width?: Maybe<Scalars['Float']>;
};

export type BannerImageConnection = {
  __typename?: 'BannerImageConnection';
  aggregate: BannerImageConnectionAggregate;
};

export type BannerImageConnectionAggregate = {
  __typename?: 'BannerImageConnectionAggregate';
  count: Scalars['Int'];
};

export type BannerImageInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  height?: InputMaybe<Scalars['Float']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Float']>;
};

export type BannerImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BannerImageUpdateInput = {
  bannerType: Scalars['String'];
  content?: InputMaybe<Scalars['String']>;
  imageUrl: Scalars['String'];
  redirectLink?: InputMaybe<Scalars['String']>;
  status: Scalars['Boolean'];
};

export type BannerImageWhereInput = {
  banner_type?: InputMaybe<Scalars['String']>;
  created_uid?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type BannerInput = {
  liveStreamId: Scalars['Int'];
  thumbnailUrl: Scalars['String'];
};

export type Barcode = {
  __typename?: 'Barcode';
  id: Scalars['Int'];
  value: Scalars['String'];
};

export type BarcodeInput = {
  id?: InputMaybe<Scalars['ID']>;
  value: Scalars['String'];
};

export type BarcodeOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BarcodeWhereInput = {
  product?: InputMaybe<ProductWhereInput>;
};

export type BarcodesAggregate = {
  __typename?: 'BarcodesAggregate';
  count: Scalars['Int'];
};

export type BarcodesConnection = {
  __typename?: 'BarcodesConnection';
  aggregate: BarcodesAggregate;
};

export type BaseOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BaumannAnswer = {
  __typename?: 'BaumannAnswer';
  questionId: Scalars['ID'];
  value: BaumannOptionValue;
};

export type BaumannOptionValue = BooleanBox | StringBox;

export enum BaumannSkinType {
  Drnt = 'DRNT',
  Drnw = 'DRNW',
  Drpt = 'DRPT',
  Drpw = 'DRPW',
  Dsnt = 'DSNT',
  Dsnw = 'DSNW',
  Dspt = 'DSPT',
  Dspw = 'DSPW',
  Ornt = 'ORNT',
  Ornw = 'ORNW',
  Orpt = 'ORPT',
  Orpw = 'ORPW',
  Osnt = 'OSNT',
  Osnw = 'OSNW',
  Ospt = 'OSPT',
  Ospw = 'OSPW'
}

export type BaumannSkinTypeFilter = {
  equals?: InputMaybe<BaumannSkinType>;
  in?: InputMaybe<Array<BaumannSkinType>>;
};

export type BooleanBox = {
  __typename?: 'BooleanBox';
  value: Scalars['Boolean'];
};

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<Scalars['Boolean']>;
};

export type Brand = {
  __typename?: 'Brand';
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  coo?: Maybe<Scalars['String']>;
  fixedLogoUrl?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isFavouriteBrandOfViewer?: Maybe<Scalars['Boolean']>;
  isFollowed?: Maybe<Scalars['Boolean']>;
  logoUrl: Scalars['String'];
  mallId?: Maybe<Scalars['ID']>;
  status: BrandStatus;
  translations: Array<BrandTranslation>;
  types: Array<BrandType>;
  /** @deprecated should be removed in the future, just use id instead */
  uid: Scalars['ID'];
};


export type BrandBrandAdminsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandAdminWhereInput>;
};


export type BrandFixedLogoUrlArgs = {
  width: FixedSize;
};


export type BrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};

export type BrandAdmin = {
  __typename?: 'BrandAdmin';
  bannerUrl?: Maybe<Scalars['String']>;
  brandId?: Maybe<Scalars['ID']>;
  coverUrl?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  facebookUrl?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  instagramUrl?: Maybe<Scalars['String']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  logoUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  owner: User;
  ownerId: Scalars['ID'];
  promotionalProducts?: Maybe<Array<BrandPromotionalProduct>>;
  slogan?: Maybe<Scalars['String']>;
  status: BrandAdminStatus;
  storyUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  webSite?: Maybe<Scalars['String']>;
};

export type BrandAdminCreateInput = {
  brandId?: InputMaybe<Scalars['ID']>;
  name: Scalars['String'];
  ownerId: Scalars['ID'];
};

export type BrandAdminError = Error & {
  __typename?: 'BrandAdminError';
  code: BrandAdminErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum BrandAdminErrorCode {
  AccountAlreadyInUse = 'ACCOUNT_ALREADY_IN_USE'
}

export type BrandAdminMutationInput = {
  bannerUrl?: InputMaybe<Scalars['String']>;
  brandId?: InputMaybe<Scalars['Int']>;
  facbookUrl?: InputMaybe<Scalars['String']>;
  instagramUrl?: InputMaybe<Scalars['String']>;
  logoUrl?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  slogan?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<BrandAdminStatus>;
  storyUrl?: InputMaybe<Scalars['String']>;
  webSite?: InputMaybe<Scalars['String']>;
};

export type BrandAdminOrError = BrandAdmin | BrandAdminError;

export enum BrandAdminStatus {
  Approved = 'APPROVED',
  Pending = 'PENDING',
  Rejected = 'REJECTED'
}

export type BrandAdminUpdateInput = {
  bannerUrl?: InputMaybe<Scalars['String']>;
  coverUrl?: InputMaybe<Scalars['String']>;
  facbookUrl?: InputMaybe<Scalars['String']>;
  instagramUrl?: InputMaybe<Scalars['String']>;
  logoUrl?: InputMaybe<Scalars['String']>;
  slogan?: InputMaybe<Scalars['String']>;
  storyUrl?: InputMaybe<Scalars['String']>;
  webSite?: InputMaybe<Scalars['String']>;
};

export type BrandAdminWhereInput = {
  brandId?: InputMaybe<Scalars['ID']>;
  id?: InputMaybe<Scalars['ID']>;
  ownerId?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<BrandAdminStatus>;
};

export type BrandAdminWhereUniqueInput = {
  id: Scalars['ID'];
};

export type BrandCreateInput = {
  coo?: InputMaybe<Scalars['String']>;
  logoUrl: Scalars['String'];
  mallId?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<BrandStatus>;
  translations: BrandTranslationCreateManyWithoutBrandInput;
  types: BrandTypeCreateManyWithoutBrandInput;
};

export type BrandOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type BrandPromotionalProducWhereInput = {
  productId: Scalars['Int'];
};

export type BrandPromotionalProduct = {
  __typename?: 'BrandPromotionalProduct';
  brandAdminId: Scalars['ID'];
  product?: Maybe<Product>;
  productId: Scalars['ID'];
  productName?: Maybe<Scalars['String']>;
};

export type BrandPromotionalProductInput = {
  productId?: InputMaybe<Scalars['ID']>;
  productName?: InputMaybe<Scalars['String']>;
};

export type BrandSearchResult = {
  __typename?: 'BrandSearchResult';
  brands: Array<Brand>;
  total: Scalars['Int'];
};

export enum BrandStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type BrandStore = {
  __typename?: 'BrandStore';
  address: Scalars['String'];
  brandCode: Scalars['String'];
  code: Scalars['String'];
  id: Scalars['Int'];
  mapCoord: Scalars['String'];
  name: Scalars['String'];
  telephone: Scalars['String'];
};

export type BrandTranslation = {
  __typename?: 'BrandTranslation';
  brand: Brand;
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type BrandTranslationCreateManyWithoutBrandInput = {
  create: Array<BrandTranslationCreateWithoutBrandInput>;
};

export type BrandTranslationCreateWithoutBrandInput = {
  description: Scalars['String'];
  isOriginal?: InputMaybe<Scalars['Boolean']>;
  language: LanguageCode;
  name: Scalars['String'];
  slug: Scalars['String'];
};

export type BrandTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
  name?: InputMaybe<OrderBy>;
};

export type BrandTranslationUpdateManyWithoutBrandInput = {
  create?: InputMaybe<Array<BrandTranslationCreateWithoutBrandInput>>;
  set?: InputMaybe<Array<BrandTranslationUpdateWithoutBrandInput>>;
};

export type BrandTranslationUpdateWithoutBrandInput = {
  description: Scalars['String'];
  id: Scalars['Int'];
  isOriginal?: InputMaybe<Scalars['Boolean']>;
  language: LanguageCode;
  name: Scalars['String'];
  slug: Scalars['String'];
};

export type BrandTranslationWhereInput = {
  brand?: InputMaybe<BrandWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type BrandType = {
  __typename?: 'BrandType';
  id: Scalars['ID'];
  value: BrandTypeValue;
};

export type BrandTypeCreateManyWithoutBrandInput = {
  connect: Array<BrandTypeWhereUniqueInput>;
};

export type BrandTypeUpdateManyWithoutBrandInput = {
  set: Array<BrandTypeWhereUniqueInput>;
};

export enum BrandTypeValue {
  DepartmentStore = 'DEPARTMENT_STORE',
  DrugStore = 'DRUG_STORE',
  OnlineOther = 'ONLINE_OTHER',
  RoadStore = 'ROAD_STORE'
}

export type BrandTypeWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  value?: InputMaybe<BrandTypeValue>;
};

export type BrandUpdateInput = {
  coo?: InputMaybe<Scalars['String']>;
  logoUrl: Scalars['String'];
  mallId?: InputMaybe<Scalars['ID']>;
  status?: InputMaybe<BrandStatus>;
  translations: BrandTranslationUpdateManyWithoutBrandInput;
  types: BrandTypeUpdateManyWithoutBrandInput;
};

export type BrandWhereInput = {
  brandIdFilters?: InputMaybe<Array<Scalars['Int']>>;
  followedByUser?: InputMaybe<UserWhereUniqueInput>;
  followersSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<StringFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<BrandStatus>;
  text?: InputMaybe<Scalars['String']>;
  translationsSome?: InputMaybe<BrandTranslationWhereInput>;
  type?: InputMaybe<BrandTypeWhereUniqueInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type BrandWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  mallId?: InputMaybe<Scalars['ID']>;
};

export type BrandsAggregate = {
  __typename?: 'BrandsAggregate';
  count: Scalars['Int'];
  following: Scalars['Int'];
};

export type BrandsConnection = {
  __typename?: 'BrandsConnection';
  aggregate: BrandsAggregate;
};

export type BrandsFollowingConnection = {
  __typename?: 'BrandsFollowingConnection';
  aggregate: BrandsFollowingsAggregate;
};

export type BrandsFollowingsAggregate = {
  __typename?: 'BrandsFollowingsAggregate';
  count: Scalars['Int'];
};

export type CartItem = {
  __typename?: 'CartItem';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  liveStreamProduct: LiveStreamProduct;
  note?: Maybe<Scalars['String']>;
  order?: Maybe<Order>;
  quantity: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
};

export type CartItemCreateInput = {
  liveStreamProduct: LiveStreamProductCreateOneWithoutCartItemsInput;
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderCreateOneWithoutCartItemInput>;
  quantity: Scalars['Int'];
};

export type CartItemCreateManyWithoutOrderInput = {
  connect: Array<CartItemWhereUniqueInput>;
};

export type CartItemOrError = CartItem | CommonError | OrderError;

export type CartItemOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CartItemUpdateInput = {
  note?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<OrderUpdateOneWithoutCartItemsInput>;
  quantity?: InputMaybe<Scalars['Int']>;
};

export type CartItemUpdateManyWithoutOrderInput = {
  set: Array<CartItemWhereUniqueInput>;
};

export type CartItemWhereInput = {
  liveStreamProduct?: InputMaybe<LiveStreamProductWhereInput>;
  order?: InputMaybe<OrderWhereInput>;
  user?: InputMaybe<UserWhereInput>;
};

export type CartItemWhereUniqueInput = {
  id: Scalars['Int'];
};

export type CartItemsAggregate = {
  __typename?: 'CartItemsAggregate';
  count: Scalars['Int'];
  sum: CartItemsSum;
};

export type CartItemsConnection = {
  __typename?: 'CartItemsConnection';
  aggregate: CartItemsAggregate;
};

export type CartItemsSum = {
  __typename?: 'CartItemsSum';
  price: Scalars['Int'];
};

export type Cast = {
  __typename?: 'Cast';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  /** @deprecated use uid instead */
  id: Scalars['Int'];
  name: Scalars['String'];
  products: Array<Product>;
  savedByViewer: Scalars['Boolean'];
  tags: Array<Tag>;
  thumbnailUrl: Scalars['String'];
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
};


export type CastProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type CastAggregate = {
  __typename?: 'CastAggregate';
  count: Scalars['Int'];
};

export type CastComment = {
  __typename?: 'CastComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type CastCommentCreateInput = {
  cast: CastCreateOneWithoutCommentsInput;
  content: Scalars['String'];
};

export type CastCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastCommentWhereInput = {
  cast: CastWhereUniqueInput;
};

export type CastCommentsAggregate = {
  __typename?: 'CastCommentsAggregate';
  count: Scalars['Int'];
};

export type CastCommentsConnection = {
  __typename?: 'CastCommentsConnection';
  aggregate: CastCommentsAggregate;
};

export type CastConnection = {
  __typename?: 'CastConnection';
  aggregate: CastAggregate;
};

export type CastCreateOneWithoutCommentsInput = {
  connect: CastWhereUniqueInput;
};

export type CastOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type CastWhereInput = {
  nameContains?: InputMaybe<Scalars['String']>;
};

export type CastWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoriesAggregate = {
  __typename?: 'CategoriesAggregate';
  count: Scalars['Int'];
};

export type CategoriesConnection = {
  __typename?: 'CategoriesConnection';
  aggregate: CategoriesAggregate;
};

export type Category = {
  __typename?: 'Category';
  activeLogo?: Maybe<Image>;
  children?: Maybe<Array<Category>>;
  id: Scalars['Int'];
  inactiveLogo?: Maybe<Image>;
  parent?: Maybe<Category>;
  productRankings?: Maybe<Array<ProductRanking>>;
  productRankingsConnection: ProductRankingsConnection;
  productRankingsV2?: Maybe<Array<ProductRanking>>;
  reviewQuestionSet?: Maybe<ReviewQuestionSet>;
  reviewQuestionSetId?: Maybe<Scalars['ID']>;
  status: CategoryStatus;
  translations: Array<CategoryTranslation>;
  uid: Scalars['ID'];
};


export type CategoryChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type CategoryProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type CategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryTranslationWhereInput>;
};

export type CategoryCreateInput = {
  activeLogo?: InputMaybe<ImageCreateOneWithoutCategoryInput>;
  inactiveLogo?: InputMaybe<ImageCreateOneWithoutCategoryInput>;
  parent?: InputMaybe<CategoryCreateOneWithoutChildrenInput>;
  questionSetId?: InputMaybe<Scalars['ID']>;
  status: CategoryStatus;
  translations: CategoryTranslationCreateManyWithoutCategoryInput;
};

export type CategoryCreateOneWithoutChildrenInput = {
  connect: CategoryWhereUniqueInput;
};

export type CategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  mixedRows?: InputMaybe<Scalars['Boolean']>;
};

export enum CategoryStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type CategoryTranslation = {
  __typename?: 'CategoryTranslation';
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
};

export type CategoryTranslationCreateManyWithoutCategoryInput = {
  create: Array<CategoryTranslationCreateWithoutCategoryInput>;
};

export type CategoryTranslationCreateWithoutCategoryInput = {
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: InputMaybe<Scalars['String']>;
};

export type CategoryTranslationUpdateManyWithoutCategoryInput = {
  create?: InputMaybe<Array<CategoryTranslationCreateWithoutCategoryInput>>;
  set?: InputMaybe<Array<CategoryTranslationUpdateWithoutCategoryInput>>;
};

export type CategoryTranslationUpdateWithoutCategoryInput = {
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  slug?: InputMaybe<Scalars['String']>;
};

export type CategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  slug?: InputMaybe<Scalars['String']>;
};

export type CategoryUpdateInput = {
  activeLogo?: InputMaybe<ImageUpdateOneWithoutCategoryInput>;
  inactiveLogo?: InputMaybe<ImageUpdateOneWithoutCategoryInput>;
  parent?: InputMaybe<CategoryUpdateOneWithoutChildrenInput>;
  questionSetId?: InputMaybe<Scalars['ID']>;
  status: CategoryStatus;
  translations?: InputMaybe<CategoryTranslationUpdateManyWithoutCategoryInput>;
};

export type CategoryUpdateOneWithoutChildrenInput = {
  connect: CategoryWhereUniqueInput;
};

export type CategoryWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idNot?: InputMaybe<Scalars['Int']>;
  isLeaf?: InputMaybe<Scalars['Boolean']>;
  parent?: InputMaybe<CategoryWhereUniqueInput>;
  productType?: InputMaybe<ProductType>;
  status?: InputMaybe<CategoryStatus>;
  translationsSome?: InputMaybe<CategoryTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type CautionCreateInput = {
  icon?: InputMaybe<ImageCreateNestedOneWithoutCautionInput>;
  productType: ProductType;
  translations: CautionTranslationCreateManyWithoutCautionInput;
};

export type CautionCreateManyWithoutIngredientInput = {
  connect: Array<CautionWhereUniqueInput>;
};

export type CautionTranslationCreateManyWithoutCautionInput = {
  create: Array<CautionTranslationCreateWithoutCautionInput>;
};

export type CautionTranslationCreateWithoutCautionInput = {
  description: Scalars['String'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type CautionTranslationUpdateManyWithoutCautionInput = {
  create?: InputMaybe<Array<CautionTranslationCreateWithoutCautionInput>>;
  set?: InputMaybe<Array<CautionTranslationUpdateWithoutCautionInput>>;
};

export type CautionTranslationUpdateWithoutCautionInput = {
  description: Scalars['String'];
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type CautionUpdateInput = {
  icon?: InputMaybe<ImageCreateNestedOneWithoutCautionInput>;
  productType: ProductType;
  translations: CautionTranslationUpdateManyWithoutCautionInput;
};

export type CautionUpdateManyWithoutIngredientInput = {
  set: Array<CautionWhereUniqueInput>;
};

export type CautionWhereInput = {
  id?: InputMaybe<IntFilter>;
  productType?: InputMaybe<ProductType>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type CautionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Channel = {
  __typename?: 'Channel';
  id: Scalars['ID'];
};

export enum ChannelCommand {
  Start = 'START',
  Status = 'STATUS',
  Stop = 'STOP'
}

export type ChannelError = Error & {
  __typename?: 'ChannelError';
  command: ChannelCommand;
  id?: Maybe<Scalars['String']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export type ChannelOrError = Channel | ChannelError | CommonError;

export type CommonError = Error & {
  __typename?: 'CommonError';
  code: CommonErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum CommonErrorCode {
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND'
}

export type CommonResponse = {
  __typename?: 'CommonResponse';
  code: Scalars['String'];
  message: Scalars['String'];
  status: Scalars['Float'];
};

export enum Currency {
  Vnd = 'VND'
}

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<Scalars['DateTime']>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type Device = {
  __typename?: 'Device';
  deviceType: DeviceType;
  id: Scalars['Int'];
  token: Scalars['String'];
};

export type DeviceCreateInput = {
  deviceType: DeviceType;
  token: Scalars['String'];
};

export enum DeviceType {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export enum EcommerceSite {
  Lazada = 'LAZADA',
  Shopee = 'SHOPEE',
  Tiki = 'TIKI'
}

export type Error = {
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ErrorStatus {
  BadRequest = 'BAD_REQUEST',
  Forbidden = 'FORBIDDEN',
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  NotFound = 'NOT_FOUND',
  Unauthorized = 'UNAUTHORIZED',
  Unprocessable = 'UNPROCESSABLE'
}

export type Event = {
  __typename?: 'Event';
  commentsConnection: EventCommentConnection;
  condition: Scalars['String'];
  content: Scalars['String'];
  coverUrl: Scalars['String'];
  currentNumberOfWinners: Scalars['Int'];
  endedAt: Scalars['DateTime'];
  eventToProducts: Array<EventToProduct>;
  id: Scalars['Int'];
  /** @deprecated use maximumNumberOfWinners instead */
  maximumNumberOfParticipants?: Maybe<Scalars['Int']>;
  /** @deprecated use numberOfWinner instead */
  maximumNumberOfWinners?: Maybe<Scalars['Int']>;
  minimumNumberOfReviews?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  notifications: Array<EventNotification>;
  numberOfWinners?: Maybe<Scalars['Int']>;
  points?: Maybe<Scalars['Int']>;
  products: Array<Product>;
  productsCount: Scalars['Int'];
  referred_url?: Maybe<Scalars['String']>;
  reminderDates?: Maybe<Array<Scalars['DateTime']>>;
  reviewDeadline?: Maybe<Scalars['DateTime']>;
  startedAt: Scalars['DateTime'];
  tags: Array<Tag>;
  type: EventType;
  /** @deprecated please use id instead */
  uid: Scalars['ID'];
  visible: Scalars['Boolean'];
};


export type EventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type EventProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};

export type EventAggregate = {
  __typename?: 'EventAggregate';
  count: Scalars['Int'];
};

export type EventComment = {
  __typename?: 'EventComment';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  event: Event;
  id: Scalars['Int'];
  product?: Maybe<Product>;
  shippingAddress: ShippingAddress;
  status: EventCommentStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type EventCommentAggregate = {
  __typename?: 'EventCommentAggregate';
  count: Scalars['Int'];
};

export type EventCommentConnection = {
  __typename?: 'EventCommentConnection';
  aggregate: EventCommentAggregate;
};

export type EventCommentCreateError = Error & {
  __typename?: 'EventCommentCreateError';
  code: EventCommentCreateErrorCode;
  comments?: Maybe<Array<EventComment>>;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum EventCommentCreateErrorCode {
  ExceedMaxParticipants = 'EXCEED_MAX_PARTICIPANTS',
  HaveNotLinkedSocialMedia = 'HAVE_NOT_LINKED_SOCIAL_MEDIA',
  HaveNotWrittenReviewForTrialProducts = 'HAVE_NOT_WRITTEN_REVIEW_FOR_TRIAL_PRODUCTS',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS',
  NotEnoughReviews = 'NOT_ENOUGH_REVIEWS',
  ShippingAddressNotExisted = 'SHIPPING_ADDRESS_NOT_EXISTED'
}

export type EventCommentCreateInput = {
  content: Scalars['String'];
  event: EventWhereUniqueInput;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  usePoints?: InputMaybe<Scalars['Boolean']>;
};

export type EventCommentOrError = CommonError | EventComment | EventCommentCreateError;

export type EventCommentOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum EventCommentStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type EventCommentUpdateManyWithoutEventInput = {
  update: Array<EventCommentUpdateWithoutEventInput>;
};

export type EventCommentUpdateWithoutEventInput = {
  id: Scalars['Int'];
  product?: InputMaybe<ProductUpdateOneWithoutEventCommentInput>;
};

export type EventCommentWhereInput = {
  event?: InputMaybe<EventWhereInput>;
  status?: InputMaybe<EventCommentStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type EventCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type EventConnection = {
  __typename?: 'EventConnection';
  aggregate: EventAggregate;
};

export type EventCreateInput = {
  condition: Scalars['String'];
  content: Scalars['String'];
  coverUrl: Scalars['String'];
  endedAt: Scalars['DateTime'];
  eventToProducts?: InputMaybe<EventToProductCreateManyWithoutEventInput>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  numberOfWinners?: InputMaybe<Scalars['Int']>;
  pointValue?: InputMaybe<Scalars['Int']>;
  referred_url?: InputMaybe<Scalars['String']>;
  reminderDates?: InputMaybe<Array<Scalars['DateTime']>>;
  reviewDeadline?: InputMaybe<Scalars['DateTime']>;
  startedAt: Scalars['DateTime'];
  tags?: InputMaybe<TagCreateManyWithoutEventInput>;
  type: EventType;
  visible?: InputMaybe<Scalars['Boolean']>;
};

export type EventNotification = {
  __typename?: 'EventNotification';
  content: Scalars['String'];
  id: Scalars['String'];
  notifiedAt?: Maybe<Scalars['DateTime']>;
  title: Scalars['String'];
};

export type EventNotificationCreateInput = {
  content: Scalars['String'];
  event: EventWhereUniqueInput;
  notifiedAt?: InputMaybe<Scalars['DateTime']>;
  title: Scalars['String'];
};

export type EventNotificationUpdateInput = {
  content: Scalars['String'];
  event: EventWhereUniqueInput;
  notifiedAt?: InputMaybe<Scalars['DateTime']>;
  title: Scalars['String'];
};

export type EventNotificationWhereUniqueInput = {
  id: Scalars['String'];
};

export type EventOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type EventToProduct = {
  __typename?: 'EventToProduct';
  event: Event;
  product: Product;
  quantity?: Maybe<Scalars['Int']>;
};

export type EventToProductCreateManyWithoutEventInput = {
  create: Array<EventToProductCreateOneWithoutEventInput>;
};

export type EventToProductCreateOneWithoutEventInput = {
  product: ProductWhereUniqueInput;
  quantity: Scalars['Int'];
};

export type EventToProductUpdateManyWithoutEventInput = {
  create: Array<EventToProductCreateOneWithoutEventInput>;
};

export enum EventType {
  Sale = 'SALE',
  Trial = 'TRIAL'
}

export type EventUpdateInput = {
  condition: Scalars['String'];
  content: Scalars['String'];
  coverUrl?: InputMaybe<Scalars['String']>;
  endedAt: Scalars['DateTime'];
  eventToProducts?: InputMaybe<EventToProductUpdateManyWithoutEventInput>;
  minimumNumberOfReviews?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  numberOfWinners?: InputMaybe<Scalars['Int']>;
  pointValue?: InputMaybe<Scalars['Int']>;
  referred_url?: InputMaybe<Scalars['String']>;
  reminderDates?: InputMaybe<Array<Scalars['DateTime']>>;
  reviewDeadline?: InputMaybe<Scalars['DateTime']>;
  startedAt: Scalars['DateTime'];
  tags?: InputMaybe<TagUpdateManyWithoutEventInput>;
  type: EventType;
  visible?: InputMaybe<Scalars['Boolean']>;
};

export type EventWhereInput = {
  conditionContains?: InputMaybe<Scalars['String']>;
  hasReferredUrl?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['Int']>;
  isExpired?: InputMaybe<Scalars['Boolean']>;
  isVisible?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  type?: InputMaybe<EventType>;
  visible?: InputMaybe<Scalars['Boolean']>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export enum EwgRiskType {
  High = 'HIGH',
  Low = 'LOW',
  Medium = 'MEDIUM',
  Unknown = 'UNKNOWN'
}

export type ExternalLink = {
  __typename?: 'ExternalLink';
  affiliateUrl?: Maybe<Scalars['String']>;
  image?: Maybe<Image>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkCreateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export type ExternalLinkCreateWithoutShopInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkCreateWithoutShopToProductInput = {
  affiliateUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageCreateOneWithoutExternalLinkInput>;
  title: Scalars['String'];
  url: Scalars['String'];
};

export type ExternalLinkUpdateManyWithoutShopInput = {
  create: Array<ExternalLinkCreateWithoutShopInput>;
};

export type ExternalLinkUpdateManyWithoutShopToProductInput = {
  create: Array<ExternalLinkCreateWithoutShopToProductInput>;
};

export type Feature = {
  __typename?: 'Feature';
  liveStream?: Maybe<Scalars['Boolean']>;
  video?: Maybe<Scalars['Boolean']>;
};

export enum FixedSize {
  Large = 'LARGE',
  Medium = 'MEDIUM',
  Small = 'SMALL'
}

export type FollowersAggregate = {
  __typename?: 'FollowersAggregate';
  count: Scalars['Int'];
};

export type FollowersConnection = {
  __typename?: 'FollowersConnection';
  aggregate: FollowersAggregate;
};

export type FollowingsAggregate = {
  __typename?: 'FollowingsAggregate';
  count: Scalars['Int'];
};

export type FollowingsConnection = {
  __typename?: 'FollowingsConnection';
  aggregate: FollowingsAggregate;
};

export type Function = {
  __typename?: 'Function';
  id: Scalars['Int'];
  productType: ProductType;
  symbolUrl?: Maybe<Scalars['String']>;
  translations: Array<FunctionTranslation>;
  type: FunctionType;
  uid: Scalars['ID'];
};


export type FunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};

export type FunctionCreateInput = {
  productType?: InputMaybe<ProductType>;
  symbolUrl: Scalars['String'];
  translations: FunctionTranslationCreateManyWithoutFunctionInput;
  type: FunctionType;
};

export type FunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type FunctionTranslation = {
  __typename?: 'FunctionTranslation';
  description?: Maybe<Scalars['String']>;
  function: Function;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type FunctionTranslationCreateManyWithoutFunctionInput = {
  create: Array<FunctionTranslationCreateWithoutFunctionInput>;
};

export type FunctionTranslationCreateWithoutFunctionInput = {
  description?: InputMaybe<Scalars['String']>;
  language: LanguageCode;
  name: Scalars['String'];
};

export type FunctionTranslationUpdateManyWithoutFunctionInput = {
  create?: InputMaybe<Array<FunctionTranslationCreateWithoutFunctionInput>>;
  set?: InputMaybe<Array<FunctionTranslationUpdateWithoutFunctionInput>>;
};

export type FunctionTranslationUpdateWithoutFunctionInput = {
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type FunctionTranslationWhereInput = {
  function?: InputMaybe<FunctionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
};

export enum FunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type FunctionUpdateInput = {
  productType?: InputMaybe<ProductType>;
  symbolUrl?: InputMaybe<Scalars['String']>;
  translations: FunctionTranslationUpdateManyWithoutFunctionInput;
  type: FunctionType;
};

export type FunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  productType?: InputMaybe<ProductType>;
  translationsSome?: InputMaybe<FunctionTranslationWhereInput>;
  type?: InputMaybe<FunctionType>;
};

export type FunctionWhereUniqueInput = {
  id: Scalars['Int'];
};

export type FunctionsAggregate = {
  __typename?: 'FunctionsAggregate';
  count: Scalars['Int'];
};

export type FunctionsConnection = {
  __typename?: 'FunctionsConnection';
  aggregate: FunctionsAggregate;
};

export type Funding = {
  __typename?: 'Funding';
  coverUrl: Scalars['String'];
  endDate: Scalars['DateTime'];
  id: Scalars['Int'];
  longDescription: Scalars['String'];
  product: Product;
  productId: Scalars['Int'];
  salePrice: Scalars['Float'];
  shortDescription: Scalars['String'];
  startDate: Scalars['DateTime'];
};

export type FundingCreateInput = {
  coverUrl: Scalars['String'];
  endDate: Scalars['DateTime'];
  longDescription: Scalars['String'];
  productId: Scalars['Int'];
  salePrice: Scalars['Float'];
  shortDescription: Scalars['String'];
  startDate: Scalars['DateTime'];
};

export type FundingOrderByInput = {
  id?: InputMaybe<OrderBy>;
  startDate?: InputMaybe<OrderBy>;
};

export enum FundingStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type FundingUpdateInput = {
  coverUrl?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['DateTime']>;
  longDescription?: InputMaybe<Scalars['String']>;
  productId?: InputMaybe<Scalars['Int']>;
  salePrice?: InputMaybe<Scalars['Float']>;
  shortDescription?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['DateTime']>;
};

export type FundingWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  statusFilter?: InputMaybe<FundingStatus>;
};

export enum Gender {
  Female = 'FEMALE',
  Male = 'MALE',
  Other = 'OTHER'
}

export type GoodsList = {
  __typename?: 'GoodsList';
  brandCode: Scalars['String'];
  brandLogo?: Maybe<Scalars['String']>;
  brandName: Scalars['String'];
  cateCode: Scalars['String'];
  commtGuide: Scalars['String'];
  commtGuideEn: Scalars['String'];
  commtGuideKr: Scalars['String'];
  commtProduct: Scalars['String'];
  commtProductEn: Scalars['String'];
  commtProductKr: Scalars['String'];
  enGoodsName: Scalars['String'];
  expiryDate: Scalars['String'];
  expiryDateEn: Scalars['String'];
  expiryDateKr: Scalars['String'];
  goodsId: Scalars['String'];
  goodsName: Scalars['String'];
  goodsType: Scalars['String'];
  id: Scalars['Int'];
  krGoodsName: Scalars['String'];
  listPrice: Scalars['Float'];
  modDate: Scalars['String'];
  originImg: Scalars['String'];
  rpPoint: Scalars['Int'];
  saleFeeType: Scalars['String'];
  salePrice: Scalars['Float'];
  useYn: Scalars['String'];
  vendorId?: Maybe<Scalars['Int']>;
  vendorLogo?: Maybe<Scalars['String']>;
  vendorName?: Maybe<Scalars['String']>;
  voucherBrand: VoucherBrand;
  voucherCategories?: Maybe<VoucherCategories>;
  voucherVendor: VoucherVendor;
};

export type GoodsListArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
  rp_point?: InputMaybe<OrderBy>;
};

export type GoodsListWhereInput = {
  categories?: InputMaybe<Array<Scalars['String']>>;
  id?: InputMaybe<Scalars['Int']>;
};

export type Hashtag = {
  __typename?: 'Hashtag';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isBookmarkedByViewer?: Maybe<Scalars['Boolean']>;
  postCount: Scalars['Int'];
  reviewCount: Scalars['Int'];
  value: Scalars['String'];
};

export type HashtagInput = {
  id?: InputMaybe<Scalars['ID']>;
  value: Scalars['String'];
};

export type HashtagWhereInput = {
  value: StringFilter;
};

export type HighlightProduct = {
  __typename?: 'HighlightProduct';
  product: Product;
  ranking?: Maybe<Scalars['Int']>;
  rankingChange?: Maybe<Scalars['Int']>;
};

export type IamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type IamUpsertInput = {
  userResourceAPIData?: InputMaybe<Array<UserResourceApiUpsertInput>>;
  userResourceData?: InputMaybe<Array<UserResourceUpsertInput>>;
};

export type Image = {
  __typename?: 'Image';
  createdAt: Scalars['DateTime'];
  fixed: Image;
  height?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width?: Maybe<Scalars['Float']>;
};


export type ImageFixedArgs = {
  width: FixedSize;
};

export type ImageCreateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
};

export type ImageCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
};

export type ImageCreateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
};

export type ImageCreateNestedOneWithoutCautionInput = {
  create: ImageCreateWithoutCautionInput;
};

export type ImageCreateOneWithoutCategoryInput = {
  create?: InputMaybe<ImageCreateWithoutCategoryInput>;
};

export type ImageCreateOneWithoutExternalLinkInput = {
  create?: InputMaybe<ImageCreateWithoutExternalLinkInput>;
};

export type ImageCreateOneWithoutLiveStreamInput = {
  create: ImageCreateWithoutLiveStreamInput;
};

export type ImageCreateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageCreateWithoutCategoryInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutCautionInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutExternalLinkInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutLiveStreamInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostCommentInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutPostInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutReviewInput = {
  height: Scalars['Int'];
  name: Scalars['String'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type ImageCreateWithoutShopInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageCreateWithoutUserInput = {
  height?: InputMaybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageInput = {
  height?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
  url?: InputMaybe<Scalars['String']>;
  width?: InputMaybe<Scalars['Int']>;
};

export type ImageOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ImageUpdateManyWithoutPostCommentInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostCommentInput>>;
  disconnect?: InputMaybe<Array<ImageWhereUniqueInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutPostInput = {
  create?: InputMaybe<Array<ImageCreateWithoutPostInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutReviewInput = {
  create?: InputMaybe<Array<ImageCreateWithoutReviewInput>>;
  set?: InputMaybe<Array<ImageWhereUniqueInput>>;
};

export type ImageUpdateManyWithoutUserInput = {
  create?: InputMaybe<ImageCreateWithoutUserInput>;
};

export type ImageUpdateOneWithoutCategoryInput = {
  connect?: InputMaybe<ImageWhereUniqueInput>;
  create?: InputMaybe<ImageCreateWithoutCategoryInput>;
  disconnect?: InputMaybe<ImageWhereUniqueInput>;
};

export type ImageUpdateOneWithoutShopInput = {
  create: ImageCreateWithoutShopInput;
};

export type ImageWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export type ImageWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ImagesAggregate = {
  __typename?: 'ImagesAggregate';
  count: Scalars['Int'];
};

export type ImagesConnection = {
  __typename?: 'ImagesConnection';
  aggregate: ImagesAggregate;
};

export type Ingredient = {
  __typename?: 'Ingredient';
  attributes?: Maybe<IngredientAttribute>;
  /** @deprecated please use cosmeticCautions or functionalFoodCautions */
  cautions?: Maybe<Array<IngredientCaution>>;
  cosmeticCautions?: Maybe<Array<IngredientCaution>>;
  cosmeticFunctions?: Maybe<Array<Function>>;
  /** @deprecated please use description of translations instead */
  description?: Maybe<Scalars['String']>;
  ewg?: Maybe<Scalars['String']>;
  ewgRiskType?: Maybe<EwgRiskType>;
  functionalFoodCautions?: Maybe<Array<IngredientCaution>>;
  functionalFoodFunctions?: Maybe<Array<Function>>;
  id: Scalars['Int'];
  /** @deprecated please use cosmeticFunctions or functionalFoodFunctions */
  specialFunctions?: Maybe<Array<SpecialIngredientFunction>>;
  translations: Array<IngredientTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientCosmeticFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientFunctionalFoodCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type IngredientFunctionalFoodFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type IngredientSpecialFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type IngredientTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientTranslationWhereInput>;
};

export type IngredientAttribute = {
  __typename?: 'IngredientAttribute';
  demographic?: Maybe<IngredientDemographic>;
  rdi?: Maybe<Scalars['Float']>;
  rdiUnit?: Maybe<Unit>;
  ul?: Maybe<Scalars['Float']>;
  ulUnit?: Maybe<Unit>;
};

export type IngredientAttributeCreateNestedOneWithoutIngredientInput = {
  create: IngredientAttributeCreateWithoutIngredientInput;
};

export type IngredientAttributeCreateWithoutIngredientInput = {
  demographic?: InputMaybe<IngredientDemograhicCreateInput>;
  rdi?: InputMaybe<Scalars['Float']>;
  rdiUnit?: InputMaybe<Unit>;
  ul?: InputMaybe<Scalars['Float']>;
  ulUnit?: InputMaybe<Unit>;
};

export type IngredientCaution = {
  __typename?: 'IngredientCaution';
  icon?: Maybe<Image>;
  id: Scalars['Int'];
  productType: ProductType;
  translations: Array<IngredientCautionTranslation>;
  uid: Scalars['ID'];
};


export type IngredientCautionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientCautionTranslationWhereInput>;
};

export type IngredientCautionTranslation = {
  __typename?: 'IngredientCautionTranslation';
  caution: IngredientCaution;
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type IngredientCautionTranslationWhereInput = {
  caution?: InputMaybe<CautionWhereInput>;
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type IngredientCautionsAggregate = {
  __typename?: 'IngredientCautionsAggregate';
  count: Scalars['Int'];
};

export type IngredientCautionsConnection = {
  __typename?: 'IngredientCautionsConnection';
  aggregate: IngredientCautionsAggregate;
};

export type IngredientCosmeticAttribute = {
  __typename?: 'IngredientCosmeticAttribute';
  description?: Maybe<Scalars['String']>;
  effect?: Maybe<Scalars['String']>;
};

export type IngredientCosmeticAttributeCreateNestedOneWithoutIngredientTranslation = {
  description?: InputMaybe<Scalars['String']>;
  effect?: InputMaybe<Scalars['String']>;
};

export type IngredientCreateInput = {
  attribute?: InputMaybe<IngredientAttributeCreateNestedOneWithoutIngredientInput>;
  cosmeticCautions?: InputMaybe<CautionCreateManyWithoutIngredientInput>;
  cosmeticFunctions?: InputMaybe<IngredientToFunctionCreateNestedManyWithoutIngredientInput>;
  ewg?: InputMaybe<Scalars['String']>;
  functionalFoodCautions?: InputMaybe<CautionCreateManyWithoutIngredientInput>;
  functionalFoodFunctions?: InputMaybe<IngredientToFunctionCreateNestedManyWithoutIngredientInput>;
  isCosmetic?: InputMaybe<Scalars['Boolean']>;
  isFunctionalFood?: InputMaybe<Scalars['Boolean']>;
  translations?: InputMaybe<IngredientTranslationCreateManyWithoutIngredientInput>;
};

export type IngredientDemograhicCreateInput = {
  baby?: InputMaybe<Array<BabyRdiCreateInput>>;
  breastfeedingWoman?: InputMaybe<AdultRdiCreateInput>;
  man?: InputMaybe<Array<AdultRdiCreateInput>>;
  pregnantWoman?: InputMaybe<AdultRdiCreateInput>;
  woman?: InputMaybe<Array<AdultRdiCreateInput>>;
};

export type IngredientDemographic = {
  __typename?: 'IngredientDemographic';
  baby?: Maybe<Array<BabyRdi>>;
  breastfeedingWoman?: Maybe<AdultRdi>;
  man?: Maybe<Array<AdultRdi>>;
  pregnantWoman?: Maybe<AdultRdi>;
  woman?: Maybe<Array<AdultRdi>>;
};

export type IngredientFunctionFoodAttribute = {
  __typename?: 'IngredientFunctionFoodAttribute';
  attention?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  dose?: Maybe<Scalars['String']>;
};

export type IngredientFunctionalFoodAttributeCreateNestedOneWithoutIngredientTranslation = {
  attention?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  dose?: InputMaybe<Scalars['String']>;
};

export type IngredientOrderByInput = {
  id?: InputMaybe<OrderBy>;
  priority?: InputMaybe<OrderBy>;
};

export type IngredientSearchResult = {
  __typename?: 'IngredientSearchResult';
  ingredients: Array<Maybe<Ingredient>>;
  total: Scalars['Int'];
};

export type IngredientToFunctionCreateNestedManyWithoutIngredientInput = {
  create?: InputMaybe<Array<IngredientToFunctionCreateWithoutIngredientInput>>;
};

export type IngredientToFunctionCreateWithoutIngredientInput = {
  function: FunctionWhereUniqueInput;
  level?: InputMaybe<IngredientToFunctionLevel>;
};

export enum IngredientToFunctionLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type IngredientToFunctionUpdateNestedManyWithoutIngredientInput = {
  set?: InputMaybe<Array<IngredientToFunctionCreateWithoutIngredientInput>>;
};

export type IngredientToFunctionWhereInput = {
  functionId?: InputMaybe<IntFilter>;
  functionType?: InputMaybe<FunctionType>;
  ingredient?: InputMaybe<IngredientWhereInput>;
  level?: InputMaybe<IngredientToFunctionLevel>;
  productType?: InputMaybe<ProductType>;
};

export type IngredientTranslation = {
  __typename?: 'IngredientTranslation';
  cosmeticAttributes?: Maybe<IngredientCosmeticAttribute>;
  /** @deprecated please use functionalFoodAttributes.description or cosmeticAttributes.description */
  description?: Maybe<Scalars['String']>;
  functionalFoodAttributes?: Maybe<IngredientFunctionFoodAttribute>;
  id: Scalars['Int'];
  ingredient: Ingredient;
  language: LanguageCode;
  name: Scalars['String'];
  /** @deprecated use description instead */
  purpose?: Maybe<Scalars['String']>;
};

export type IngredientTranslationCreateManyWithoutIngredientInput = {
  create: Array<IngredientTranslationCreateWithoutIngredientInput>;
};

export type IngredientTranslationCreateWithoutIngredientInput = {
  cosmeticAttributes?: InputMaybe<IngredientCosmeticAttributeCreateNestedOneWithoutIngredientTranslation>;
  functionalFoodAttributes?: InputMaybe<IngredientFunctionalFoodAttributeCreateNestedOneWithoutIngredientTranslation>;
  language: LanguageCode;
  name: Scalars['String'];
};

export type IngredientTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type IngredientTranslationUpdateManyWithoutIngredientInput = {
  create?: InputMaybe<Array<IngredientTranslationCreateWithoutIngredientInput>>;
  set?: InputMaybe<Array<IngredientTranslationUpdateWithoutIngredientInput>>;
};

export type IngredientTranslationUpdateWithoutIngredientInput = {
  cosmeticAttributes?: InputMaybe<IngredientCosmeticAttributeCreateNestedOneWithoutIngredientTranslation>;
  functionalFoodAttributes?: InputMaybe<IngredientFunctionalFoodAttributeCreateNestedOneWithoutIngredientTranslation>;
  id: Scalars['Int'];
  language: LanguageCode;
  name: Scalars['String'];
};

export type IngredientTranslationWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  language?: InputMaybe<LanguageCode>;
  name?: InputMaybe<StringFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type IngredientUpdateInput = {
  attribute?: InputMaybe<IngredientAttributeCreateNestedOneWithoutIngredientInput>;
  cosmeticCautions?: InputMaybe<CautionUpdateManyWithoutIngredientInput>;
  cosmeticFunctions?: InputMaybe<IngredientToFunctionUpdateNestedManyWithoutIngredientInput>;
  ewg?: InputMaybe<Scalars['String']>;
  functionalFoodCautions?: InputMaybe<CautionUpdateManyWithoutIngredientInput>;
  functionalFoodFunctions?: InputMaybe<IngredientToFunctionUpdateNestedManyWithoutIngredientInput>;
  isCosmetic?: InputMaybe<Scalars['Boolean']>;
  isFunctionalFood?: InputMaybe<Scalars['Boolean']>;
  translations?: InputMaybe<IngredientTranslationUpdateManyWithoutIngredientInput>;
};

export type IngredientWhereInput = {
  cautionsSome?: InputMaybe<CautionWhereInput>;
  ewg?: InputMaybe<Scalars['String']>;
  ewgRiskType?: InputMaybe<EwgRiskType>;
  functionsSome?: InputMaybe<SpecialIngredientFunctionWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientToFunctionsSome?: InputMaybe<IngredientToFunctionWhereInput>;
  isCosmetic?: InputMaybe<BooleanFilter>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  productsSome?: InputMaybe<ProductWhereInput>;
  translationsSome?: InputMaybe<IngredientTranslationWhereInput>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type IngredientWhereUniqueInput = {
  id: Scalars['Int'];
};

export type IngredientsAggregate = {
  __typename?: 'IngredientsAggregate';
  count: Scalars['Int'];
};

export type IngredientsConnection = {
  __typename?: 'IngredientsConnection';
  aggregate: IngredientsAggregate;
};

export type IntFilter = {
  equals?: InputMaybe<Scalars['Int']>;
  gt?: InputMaybe<Scalars['Int']>;
  gte?: InputMaybe<Scalars['Int']>;
  in?: InputMaybe<Array<Scalars['Int']>>;
  lt?: InputMaybe<Scalars['Int']>;
  lte?: InputMaybe<Scalars['Int']>;
  ne?: InputMaybe<Scalars['Int']>;
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

export enum LanguageCode {
  En = 'EN',
  Ko = 'KO',
  Vi = 'VI'
}

export type LanguageCodeFilter = {
  equals?: InputMaybe<LanguageCode>;
  in?: InputMaybe<Array<LanguageCode>>;
};

export type Level = {
  __typename?: 'Level';
  id: Scalars['Int'];
  maxPoints?: Maybe<Scalars['Int']>;
  minPoints: Scalars['Int'];
};

export type LiveStream = {
  __typename?: 'LiveStream';
  background?: Maybe<Image>;
  channelId?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  host: User;
  id: Scalars['Int'];
  liveUrl?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  roomId?: Maybe<Scalars['String']>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  startedAt?: Maybe<Scalars['DateTime']>;
  status: LiveStreamStatus;
  streamUrl?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  vodUrl?: Maybe<Scalars['String']>;
};

export type LiveStreamCreateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductCreateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamError = Error & {
  __typename?: 'LiveStreamError';
  code: LiveStreamErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum LiveStreamErrorCode {
  HaveNotCreatedShop = 'HAVE_NOT_CREATED_SHOP',
  NotEnoughChannels = 'NOT_ENOUGH_CHANNELS',
  ShopIsBlocked = 'SHOP_IS_BLOCKED',
  ShopNotApprovedYet = 'SHOP_NOT_APPROVED_YET'
}

export type LiveStreamOrError = CommonError | LiveStream | LiveStreamError;

export type LiveStreamOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProduct = {
  __typename?: 'LiveStreamProduct';
  code: Scalars['String'];
  createdAt: Scalars['DateTime'];
  currency: Currency;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Int']>;
  product?: Maybe<Product>;
  updatedAt: Scalars['DateTime'];
};

export type LiveStreamProductCreateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
};

export type LiveStreamProductCreateOneWithoutCartItemsInput = {
  connect: LiveStreamProductWhereUniqueInput;
};

export type LiveStreamProductCreateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductCreateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductOrError = CommonError | LiveStreamProduct;

export type LiveStreamProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type LiveStreamProductUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductUpdateManyWithoutLiveStreamInput = {
  create?: InputMaybe<Array<LiveStreamProductCreateWithoutLiveStreamInput>>;
  set?: InputMaybe<Array<LiveStreamProductWhereUniqueInput>>;
  update?: InputMaybe<Array<LiveStreamProductUpdateWithoutLiveStreamInput>>;
};

export type LiveStreamProductUpdateWithoutLiveStreamInput = {
  code: Scalars['String'];
  currency?: InputMaybe<Currency>;
  id: Scalars['Int'];
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Int']>;
  product?: InputMaybe<ProductUpdateOneWithoutLiveStreamProductInput>;
};

export type LiveStreamProductWhereInput = {
  liveStream: LiveStreamWhereInput;
};

export type LiveStreamProductWhereUniqueInput = {
  id: Scalars['Int'];
};

export type LiveStreamStartInput = {
  roomId: Scalars['String'];
};

export enum LiveStreamStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type LiveStreamUpdateInput = {
  background?: InputMaybe<ImageCreateOneWithoutLiveStreamInput>;
  description?: InputMaybe<Scalars['String']>;
  lsProducts?: InputMaybe<LiveStreamProductUpdateManyWithoutLiveStreamInput>;
  name: Scalars['String'];
  startedAt?: InputMaybe<Scalars['DateTime']>;
};

export type LiveStreamWhereInput = {
  host?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<LiveStreamStatus>;
  statusIn?: InputMaybe<Array<LiveStreamStatus>>;
};

export type LiveStreamWhereUniqueInput = {
  id: Scalars['Int'];
};

export type LivestreamProduct = {
  __typename?: 'LivestreamProduct';
  discount?: Maybe<Scalars['Float']>;
  id: Scalars['Int'];
  livestreamID: Scalars['Int'];
  product: Product;
  productId: Scalars['Int'];
};

export type LivestreamProductUpsertInput = {
  discount?: InputMaybe<Scalars['Float']>;
  productId?: InputMaybe<Scalars['Int']>;
};

export type LivestreamSchedule = {
  __typename?: 'LivestreamSchedule';
  bannerURL?: Maybe<Scalars['String']>;
  channelId: Scalars['Int'];
  detailImageURL?: Maybe<Scalars['String']>;
  endedAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  livestream: LivestreamSchedule;
  livestreamProducts: Array<LivestreamProduct>;
  livestreamStatus: LivestreamScheduleStatus;
  livestreamUrl?: Maybe<Scalars['String']>;
  platformType?: Maybe<PlatformType>;
  products: Array<Product>;
  startedAt?: Maybe<Scalars['DateTime']>;
  streamer: Streamer;
  thumbnail?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};


export type LivestreamScheduleLivestreamArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type LivestreamScheduleProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type LivestreamScheduleStreamerArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<StreamerWhereInput>;
};

export type LivestreamScheduleBanners = {
  __typename?: 'LivestreamScheduleBanners';
  banner?: Maybe<Scalars['String']>;
  livestreamId: Scalars['Int'];
};

export type LivestreamScheduleCreateInput = {
  bannerURL?: InputMaybe<Scalars['String']>;
  channelId: Scalars['Int'];
  detailImageURL?: InputMaybe<Scalars['String']>;
  endedAt: Scalars['DateTime'];
  livestreamScheduleProducts?: InputMaybe<Array<LivestreamProductUpsertInput>>;
  livestreamUrl: Scalars['String'];
  platformType?: InputMaybe<PlatformType>;
  startedAt: Scalars['DateTime'];
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export enum LivestreamScheduleStatus {
  Created = 'CREATED',
  Running = 'RUNNING',
  Stopped = 'STOPPED'
}

export type LivestreamScheduleUpdateInput = {
  bannerURL?: InputMaybe<Scalars['String']>;
  channelId?: InputMaybe<Scalars['Int']>;
  detailImageURL?: InputMaybe<Scalars['String']>;
  endedAt?: InputMaybe<Scalars['DateTime']>;
  livestreamScheduleProducts?: InputMaybe<Array<LivestreamProductUpsertInput>>;
  livestreamUrl?: InputMaybe<Scalars['String']>;
  platformType?: InputMaybe<PlatformType>;
  startedAt?: InputMaybe<Scalars['DateTime']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type LivestreamScheduleWhereInput = {
  id: Scalars['Int'];
};

export type LivestreamScheduleWithDay = {
  __typename?: 'LivestreamScheduleWithDay';
  livestreams: Array<LivestreamSchedule>;
  time: Scalars['String'];
};

export type LivestreamScheduleWithHour = {
  __typename?: 'LivestreamScheduleWithHour';
  livestreams: Array<LivestreamSchedule>;
  time: Scalars['String'];
};

export type LivestreamSchedulesOrderByInput = {
  id?: InputMaybe<OrderBy>;
  startedAt?: InputMaybe<OrderBy>;
};

export type LivestreamSchedulesWhereInput = {
  channelId?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  platformType?: InputMaybe<Array<PlatformType>>;
  textSearch?: InputMaybe<Scalars['String']>;
  timeFilter?: InputMaybe<Scalars['DateTime']>;
};

export type Location = {
  __typename?: 'Location';
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type LocationInput = {
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type M_LikeReviewResult = {
  __typename?: 'M_LikeReviewResult';
  count: Scalars['Int'];
  message: Scalars['String'];
  status: Scalars['Boolean'];
};

export type MediaEntityUnion = LiveStream | Post;

export type MediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export enum MediaType {
  LiveStream = 'LIVE_STREAM',
  Video = 'VIDEO'
}

export type MediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
  user: UserWhereUniqueInput;
};

export type MediaWhereUniqueInput = {
  liveStream?: InputMaybe<LiveStreamWhereUniqueInput>;
  post?: InputMaybe<PostWhereUniqueInput>;
};

export type MediasAggregate = {
  __typename?: 'MediasAggregate';
  count: Scalars['Int'];
};

export type MediasConnection = {
  __typename?: 'MediasConnection';
  aggregate: MediasAggregate;
};

export type Mutation = {
  __typename?: 'Mutation';
  approvePost: Post;
  approvePostCommentReport: PostCommentReport;
  approvePostReport: PostReport;
  approveReview: Review;
  approveReviewReport: ReviewReport;
  approveShop: Shop;
  awardEventComment: EventComment;
  awardEventComments: Scalars['Boolean'];
  blockShop: Shop;
  blockUser: User;
  bookmarkPost: Scalars['Boolean'];
  bookmarkProduct: Scalars['Boolean'];
  bookmarkReview: Scalars['Boolean'];
  brandAdminCreate: BrandAdminOrError;
  brandAdminDelete: BrandAdmin;
  brandAdminMutationUpdate: BrandAdmin;
  brandAdminUpdate: BrandAdmin;
  brandPromotionalProductCreate?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductDelete?: Maybe<BrandPromotionalProduct>;
  brandPromotionalProductUpdate?: Maybe<BrandPromotionalProduct>;
  cancelEventComment: EventComment;
  cancelOrder: OrderOrError;
  changePasswordAdmin: AuthPayload;
  checkImportUser: CommonResponse;
  confirmOrder: OrderOrError;
  convertPermission: Scalars['Boolean'];
  createBannerImage: BannerImage;
  createBanners: Scalars['Boolean'];
  createBrand: Brand;
  createCartItem: CartItemOrError;
  createCastComment: CastComment;
  createCategory: Category;
  createCaution: IngredientCaution;
  createChannel: ChannelOrError;
  createDevice: Device;
  createEvent: Event;
  createEventComment: EventComment;
  createEventCommentV2: EventCommentOrError;
  createEventNotification: EventNotification;
  createFunction: Function;
  createFunding: Funding;
  createIngredient: Ingredient;
  createLiveStream: LiveStreamOrError;
  createLivestreamSchedule: LivestreamSchedule;
  createOrder: OrderOrError;
  createPost: PostOrError;
  createPostComment: PostComment;
  /** @deprecated please use createPost instead */
  createPostV2: PostOrError;
  createProductEdit: ProductEdit;
  createProductRequest: ProductRequest;
  createResource: ResourceOrError;
  createResourceAPI: ResourceApiOrError;
  /** @deprecated please use reviewCreate instead, this mutation will be removed on 1/4/2022 */
  createReview: Review;
  createReviewReport: ReviewReport;
  createRole: Scalars['Boolean'];
  createRoleResource: RoleResource;
  createShippingAddress: ShippingAddressOrError;
  /** @deprecated use createShopV2 instead */
  createShop: Shop;
  createShopToProducts: Array<ShopToProduct>;
  createShopV2: ShopOrMutationError;
  createSlot: PostReservedSlot;
  createStreamer: Streamer;
  createSystemConfig: SystemConfig;
  deleteAllUserId: Scalars['Boolean'];
  deleteBannerImage: BannerImage;
  deleteCartItem: CartItemOrError;
  deleteEvent: Event;
  deleteEventComment: EventComment;
  deleteEventNotification: EventNotification;
  deleteEventResource: RoleResource;
  deleteEventRole: Roles;
  deleteFunding: Funding;
  deleteLiveStream: LiveStreamOrError;
  deleteLiveStreamProduct: LiveStreamProductOrError;
  deleteLivestreamSchedule: LivestreamSchedule;
  deleteNotificationEvent: Scalars['Boolean'];
  deletePost: Post;
  deletePostComment: PostComment;
  deletePostReservedSlot: PostReservedSlot;
  deleteResource: Resource;
  deleteResourceAPI: ResourceApi;
  deleteReview: Review;
  deleteShippingAddress: ShippingAddressOrError;
  deleteShopToProduct: ShopToProduct;
  deleteStreamer: Streamer;
  deleteSystemConfig: SystemConfig;
  deleteUser: User;
  deleteUserId: Scalars['Boolean'];
  dislikeReview: Scalars['Boolean'];
  followBrand: Scalars['Boolean'];
  followUser: User;
  generatePresignedUrl: PresignedUrl;
  generateRegisterMultiProductForms?: Maybe<Scalars['Boolean']>;
  /** @deprecated use checkIn instead */
  getActivePoints: Scalars['Boolean'];
  /** @deprecated use checkIn instead */
  getDailyActivePoints: Scalars['Boolean'];
  hidePostComment: PostComment;
  importCsv: Scalars['Boolean'];
  likeDislikeReview: M_LikeReviewResult;
  likeReview: M_LikeReviewResult;
  login: AuthPayload;
  markBestAnswer: Scalars['Boolean'];
  notificationEventCreate: Scalars['Boolean'];
  notifyLiveStream: Scalars['Boolean'];
  productCreate: ProductCreatePayload;
  productUpdate: ProductUpdatePayload;
  purchaseVouchers: Scalars['Boolean'];
  reactPost: Post;
  reactPostComment: PostComment;
  reclaimEventComment: EventComment;
  rejectPost: Post;
  rejectPostCommentReport: PostCommentReport;
  rejectPostReport: PostReport;
  rejectReview: Review;
  rejectReviewReport: ReviewReport;
  reportPost: PostReport;
  reportPostComment: PostCommentReport;
  resendNotificationEvent: Scalars['Boolean'];
  reviewCreate: ReviewCreatePayload;
  reviewQuestionCreate: ReviewQuestionCreatePayload;
  reviewQuestionDelete: ReviewQuestionDeletePayload;
  reviewQuestionSetAddQuestions: Array<ReviewQuestion>;
  reviewQuestionSetCreate: ReviewQuestionSetCreatePayload;
  reviewQuestionSetDelete: ReviewQuestionSetDeletePayload;
  reviewQuestionSetRemoveQuestions: Array<Scalars['Int']>;
  reviewQuestionSetUpdate: ReviewQuestionSetUpdatePayload;
  reviewQuestionUpdate: ReviewQuestionUpdatePayload;
  reviewQuestionsCreate: Array<ReviewQuestionCreatePayload>;
  reviewQuestionsDelete: Array<ReviewQuestionDeletePayload>;
  reviewQuestionsUpdate: Array<ReviewQuestionUpdatePayload>;
  reviewUpdate: ReviewUpdatePayload;
  runNotificationEvent: Scalars['Boolean'];
  saveFavouriteBrands: Scalars['Boolean'];
  saveMedia: Scalars['Boolean'];
  /** @deprecated please use bookmarkProduct instead, this field will be remove from 1/11/2021 */
  saveProduct: Scalars['Boolean'];
  sendNotification?: Maybe<Scalars['Boolean']>;
  startLiveStream: LiveStreamOrError;
  stopChannel: ChannelOrError;
  stopLiveStream: LiveStreamOrError;
  subscribePost: Scalars['Boolean'];
  unBookmarkPost: Scalars['Boolean'];
  unBookmarkProduct: Scalars['Boolean'];
  unBookmarkReview: Scalars['Boolean'];
  unblockUser: Scalars['Boolean'];
  unfollowBrand: Brand;
  unfollowUser: User;
  unhidePostComment: PostComment;
  unsaveMedia: Scalars['Boolean'];
  /** @deprecated please use unBookmarkProduct instead, this field will be remove from 1/11/2021 */
  unsaveProduct: Scalars['Boolean'];
  unsubscribePost: Scalars['Boolean'];
  updateBannerImage: BannerImage;
  updateBrand: Brand;
  updateCartItem: CartItemOrError;
  updateCategory: Category;
  updateCaution: IngredientCaution;
  updateEvent: Event;
  updateEventNotification: EventNotification;
  updateFunction: Function;
  updateFunding: Funding;
  updateIngredient: Ingredient;
  updateLiveStream: LiveStreamOrError;
  updateLiveStreamProduct: LiveStreamProductOrError;
  updateLivestreamSchedule: LivestreamSchedule;
  updateNotificationEvent: Scalars['Boolean'];
  updateOrder: OrderOrError;
  updatePost: Post;
  updatePostComment: PostComment;
  updateProductRequest: ProductRequest;
  updateProductUserRequestByAdmin?: Maybe<UserRequestUpdateResponse>;
  updateProductUserStatus?: Maybe<UserRequestUpdateResponse>;
  updateResource: ResourceOrError;
  updateResourceAPI: ResourceApiOrError;
  /** @deprecated please use reviewUpdate instead, this mutation will be removed on 1/4/2022 */
  updateReview: Review;
  updateRole: Roles;
  updateRoleUser: User;
  updateShippingAddress: ShippingAddressOrError;
  /** @deprecated use updateShopV2 instead */
  updateShop: Shop;
  updateShopToProduct: ShopToProduct;
  updateShopV2: ShopOrMutationError;
  updateSlot: PostReservedSlot;
  updateStreamer: Streamer;
  updateSystemConfig: SystemConfig;
  updateUser: User;
  updateVoucherStatus: Scalars['Boolean'];
  upgradeUser: User;
  uploadShopToProducts: Array<ShopToProduct>;
  upsertIAM: Scalars['Boolean'];
  upsertUserResource: Array<UserResource>;
  upsertUserResourceApi: Array<UserResourceApi>;
  userCreateUser: UserCreateUserAuthPayloadOrError;
  userRequestCreate: UserRequest;
  viewPost: Scalars['Boolean'];
  viewedPost: Scalars['Boolean'];
  voucherBrand: Scalars['Boolean'];
};


export type MutationApprovePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationApprovePostCommentReportArgs = {
  where: PostCommentReportWhereUniqueInput;
};


export type MutationApprovePostReportArgs = {
  where: PostReportWhereUniqueInput;
};


export type MutationApproveReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationApproveReviewReportArgs = {
  where: ReviewReportWhereUniqueInput;
};


export type MutationApproveShopArgs = {
  where: ShopWhereUniqueInput;
};


export type MutationAwardEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationAwardEventCommentsArgs = {
  data: EventCommentUpdateManyWithoutEventInput;
};


export type MutationBlockShopArgs = {
  reason: Scalars['String'];
  where: ShopWhereUniqueInput;
};


export type MutationBlockUserArgs = {
  isBlocked: Scalars['Boolean'];
  where: UserWhereUniqueInput;
};


export type MutationBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationBrandAdminCreateArgs = {
  data: BrandAdminCreateInput;
};


export type MutationBrandAdminDeleteArgs = {
  where: BrandAdminWhereUniqueInput;
};


export type MutationBrandAdminMutationUpdateArgs = {
  data: BrandAdminMutationInput;
  where: BrandAdminWhereUniqueInput;
};


export type MutationBrandAdminUpdateArgs = {
  data: BrandAdminUpdateInput;
  where: BrandAdminWhereUniqueInput;
};


export type MutationBrandPromotionalProductCreateArgs = {
  data: BrandPromotionalProductInput;
};


export type MutationBrandPromotionalProductDeleteArgs = {
  where: BrandPromotionalProducWhereInput;
};


export type MutationBrandPromotionalProductUpdateArgs = {
  data: BrandPromotionalProductInput;
  where: BrandPromotionalProducWhereInput;
};


export type MutationCancelEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationCancelOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationChangePasswordAdminArgs = {
  currentPassword: Scalars['String'];
  newPassword: Scalars['String'];
};


export type MutationCheckImportUserArgs = {
  ids: NotificationEventCheckIdsUsers;
};


export type MutationConfirmOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type MutationConvertPermissionArgs = {
  userIds: Array<Scalars['Int']>;
  where: UserWhereUniqueInput;
};


export type MutationCreateBannerImageArgs = {
  data: BannerImageInput;
};


export type MutationCreateBannersArgs = {
  data: Array<BannerInput>;
};


export type MutationCreateBrandArgs = {
  data: BrandCreateInput;
};


export type MutationCreateCartItemArgs = {
  data: CartItemCreateInput;
};


export type MutationCreateCastCommentArgs = {
  data: CastCommentCreateInput;
};


export type MutationCreateCategoryArgs = {
  data: CategoryCreateInput;
};


export type MutationCreateCautionArgs = {
  data: CautionCreateInput;
};


export type MutationCreateDeviceArgs = {
  data: DeviceCreateInput;
};


export type MutationCreateEventArgs = {
  data: EventCreateInput;
};


export type MutationCreateEventCommentArgs = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventCommentV2Args = {
  data: EventCommentCreateInput;
};


export type MutationCreateEventNotificationArgs = {
  data: EventNotificationCreateInput;
};


export type MutationCreateFunctionArgs = {
  data: FunctionCreateInput;
};


export type MutationCreateFundingArgs = {
  data: FundingCreateInput;
};


export type MutationCreateIngredientArgs = {
  data: IngredientCreateInput;
};


export type MutationCreateLiveStreamArgs = {
  data: LiveStreamCreateInput;
};


export type MutationCreateLivestreamScheduleArgs = {
  data: LivestreamScheduleCreateInput;
};


export type MutationCreateOrderArgs = {
  data: OrderCreateInput;
};


export type MutationCreatePostArgs = {
  data: PostCreateInput;
};


export type MutationCreatePostCommentArgs = {
  data: PostCommentCreateInput;
};


export type MutationCreatePostV2Args = {
  data: PostCreateInput;
};


export type MutationCreateProductEditArgs = {
  data: ProductEditCreateInput;
};


export type MutationCreateProductRequestArgs = {
  data: ProductRequestCreateInput;
};


export type MutationCreateResourceArgs = {
  data: ResourceCreateInput;
};


export type MutationCreateResourceApiArgs = {
  data: ResourceApiCreateInput;
};


export type MutationCreateReviewArgs = {
  data: ReviewCreateInput;
};


export type MutationCreateReviewReportArgs = {
  data: ReviewReportCreateInput;
};


export type MutationCreateRoleArgs = {
  data: RoleCreateInput;
};


export type MutationCreateRoleResourceArgs = {
  data: RoleResourceCreateInput;
};


export type MutationCreateShippingAddressArgs = {
  data: ShippingAddressCreateInput;
};


export type MutationCreateShopArgs = {
  data: ShopCreateInput;
};


export type MutationCreateShopToProductsArgs = {
  data: Array<ShopToProductCreateInput>;
};


export type MutationCreateShopV2Args = {
  data: ShopCreateInput;
};


export type MutationCreateSlotArgs = {
  data: PostReservedSlotCreateInput;
};


export type MutationCreateStreamerArgs = {
  data: StreamerCreateInput;
};


export type MutationCreateSystemConfigArgs = {
  data: SystemConfigCreateInput;
};


export type MutationDeleteAllUserIdArgs = {
  where: NotificationEventDeleteInput;
};


export type MutationDeleteBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type MutationDeleteCartItemArgs = {
  where: CartItemWhereUniqueInput;
};


export type MutationDeleteEventArgs = {
  where: EventWhereUniqueInput;
};


export type MutationDeleteEventCommentArgs = {
  where: EventCommentWhereUniqueInput;
};


export type MutationDeleteEventNotificationArgs = {
  where: EventNotificationWhereUniqueInput;
};


export type MutationDeleteEventResourceArgs = {
  where: RoleResourceWhereUniqueInput;
};


export type MutationDeleteEventRoleArgs = {
  where: RolesWhereUniqueInput;
};


export type MutationDeleteFundingArgs = {
  where: FundingWhereInput;
};


export type MutationDeleteLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationDeleteLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationDeleteLivestreamScheduleArgs = {
  where: LivestreamScheduleWhereInput;
};


export type MutationDeleteNotificationEventArgs = {
  where: NotificationEventDeleteInput;
};


export type MutationDeletePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationDeletePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationDeletePostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type MutationDeleteResourceArgs = {
  where: ResourceWhereInput;
};


export type MutationDeleteResourceApiArgs = {
  where: ResourceApiWhereInput;
};


export type MutationDeleteReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationDeleteShippingAddressArgs = {
  where: ShippingAddressWhereUniqueInput;
};


export type MutationDeleteShopToProductArgs = {
  where: ShopToProductWhereUniqueInput;
};


export type MutationDeleteStreamerArgs = {
  where: StreamerWhereInput;
};


export type MutationDeleteSystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type MutationDeleteUserArgs = {
  data: UserDeleteInput;
  where: UserWhereUniqueInput;
};


export type MutationDeleteUserIdArgs = {
  where: NotificationEventUserInput;
};


export type MutationDislikeReviewArgs = {
  reviewId: Scalars['Int'];
};


export type MutationFollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationFollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationHidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationImportCsvArgs = {
  data: NotificationEventUserInput;
};


export type MutationLikeDislikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reactStatus?: InputMaybe<ReactStatus>;
  reviewId: Scalars['Int'];
};


export type MutationLikeReviewArgs = {
  authorId?: InputMaybe<Scalars['Int']>;
  productId: Scalars['Int'];
  productName?: InputMaybe<Scalars['String']>;
  reviewId: Scalars['Int'];
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationMarkBestAnswerArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationNotificationEventCreateArgs = {
  data: NotificationEventCreateInput;
};


export type MutationNotifyLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationProductCreateArgs = {
  data: ProductInput;
};


export type MutationProductUpdateArgs = {
  data: ProductInput;
};


export type MutationPurchaseVouchersArgs = {
  data: VoucherIssuesInput;
};


export type MutationReactPostArgs = {
  status: ReactStatus;
  where: PostWhereUniqueInput;
};


export type MutationReactPostCommentArgs = {
  status: ReactStatus;
  where: PostCommentWhereUniqueInput;
};


export type MutationReclaimEventCommentArgs = {
  payback: Scalars['Boolean'];
  where: EventCommentWhereUniqueInput;
};


export type MutationRejectPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationRejectPostCommentReportArgs = {
  where: PostCommentReportWhereUniqueInput;
};


export type MutationRejectPostReportArgs = {
  where: PostReportWhereUniqueInput;
};


export type MutationRejectReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationRejectReviewReportArgs = {
  where: ReviewReportWhereUniqueInput;
};


export type MutationReportPostArgs = {
  data: PostReportCreateInput;
};


export type MutationReportPostCommentArgs = {
  data: PostCommentReportCreateInput;
};


export type MutationResendNotificationEventArgs = {
  data: NotificationEventRunInput;
};


export type MutationReviewCreateArgs = {
  data: ReviewInput;
};


export type MutationReviewQuestionCreateArgs = {
  data: ReviewQuestionInput;
};


export type MutationReviewQuestionDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationReviewQuestionSetAddQuestionsArgs = {
  data: Array<ReviewQuestionInput>;
  reviewQuestionSetId: Scalars['String'];
};


export type MutationReviewQuestionSetCreateArgs = {
  data: ReviewQuestionSetInput;
};


export type MutationReviewQuestionSetDeleteArgs = {
  id: Scalars['Float'];
};


export type MutationReviewQuestionSetRemoveQuestionsArgs = {
  reviewQuestionSetId: Scalars['Float'];
};


export type MutationReviewQuestionSetUpdateArgs = {
  data: ReviewQuestionSetInput;
  reviewQuestionSetId: Scalars['ID'];
};


export type MutationReviewQuestionUpdateArgs = {
  data: ReviewQuestionInput;
};


export type MutationReviewQuestionsCreateArgs = {
  data: Array<ReviewQuestionInput>;
};


export type MutationReviewQuestionsDeleteArgs = {
  ids: Array<Scalars['ID']>;
};


export type MutationReviewQuestionsUpdateArgs = {
  data: Array<ReviewQuestionInput>;
};


export type MutationReviewUpdateArgs = {
  data: ReviewInput;
};


export type MutationRunNotificationEventArgs = {
  data: NotificationEventRunInput;
};


export type MutationSaveFavouriteBrandsArgs = {
  data: Array<BrandWhereUniqueInput>;
};


export type MutationSaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationSaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationSendNotificationArgs = {
  where: UserRequestUpdateInput;
};


export type MutationStartLiveStreamArgs = {
  data: LiveStreamStartInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationStopChannelArgs = {
  id: Scalars['ID'];
};


export type MutationStopLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type MutationSubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUnBookmarkPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUnBookmarkProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnBookmarkReviewArgs = {
  where: ReviewWhereUniqueInput;
};


export type MutationUnblockUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnfollowBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type MutationUnfollowUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationUnhidePostCommentArgs = {
  where: PostCommentWhereUniqueInput;
};


export type MutationUnsaveMediaArgs = {
  where: MediaWhereUniqueInput;
};


export type MutationUnsaveProductArgs = {
  where: ProductWhereUniqueInput;
};


export type MutationUnsubscribePostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationUpdateBannerImageArgs = {
  data: BannerImageUpdateInput;
  where: BannerImageWhereInput;
};


export type MutationUpdateBrandArgs = {
  data: BrandUpdateInput;
  where: BrandWhereUniqueInput;
};


export type MutationUpdateCartItemArgs = {
  data: CartItemUpdateInput;
  where: CartItemWhereUniqueInput;
};


export type MutationUpdateCategoryArgs = {
  data: CategoryUpdateInput;
  where: CategoryWhereUniqueInput;
};


export type MutationUpdateCautionArgs = {
  data: CautionUpdateInput;
  where: CautionWhereUniqueInput;
};


export type MutationUpdateEventArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};


export type MutationUpdateEventNotificationArgs = {
  data: EventNotificationUpdateInput;
  where: EventNotificationWhereUniqueInput;
};


export type MutationUpdateFunctionArgs = {
  data: FunctionUpdateInput;
  where: FunctionWhereUniqueInput;
};


export type MutationUpdateFundingArgs = {
  data: FundingUpdateInput;
  where: FundingWhereInput;
};


export type MutationUpdateIngredientArgs = {
  data: IngredientUpdateInput;
  where: IngredientWhereUniqueInput;
};


export type MutationUpdateLiveStreamArgs = {
  data: LiveStreamUpdateInput;
  where: LiveStreamWhereUniqueInput;
};


export type MutationUpdateLiveStreamProductArgs = {
  data: LiveStreamProductUpdateInput;
  where: LiveStreamProductWhereUniqueInput;
};


export type MutationUpdateLivestreamScheduleArgs = {
  data: LivestreamScheduleUpdateInput;
  where: LivestreamScheduleWhereInput;
};


export type MutationUpdateNotificationEventArgs = {
  data: NotificationEventUpdateInput;
};


export type MutationUpdateOrderArgs = {
  data: OrderUpdateInput;
  where: OrderWhereUniqueInput;
};


export type MutationUpdatePostArgs = {
  data: PostUpdateInput;
  where: PostWhereUniqueInput;
};


export type MutationUpdatePostCommentArgs = {
  data: PostCommentUpdateInput;
  where: PostCommentWhereUniqueInput;
};


export type MutationUpdateProductRequestArgs = {
  data: ProductRequestUpdateInput;
  where: ProductRequestWhereUniqueInput;
};


export type MutationUpdateProductUserRequestByAdminArgs = {
  data: UserRequestUpdateInput;
  where: UserRequestWhereUniqueInput;
};


export type MutationUpdateProductUserStatusArgs = {
  data: UserRequestUpdateStatusInput;
  where: UserRequestUpdateInput;
};


export type MutationUpdateResourceArgs = {
  data: ResourceCreateInput;
  where: ResourceWhereInput;
};


export type MutationUpdateResourceApiArgs = {
  data: ResourceApiCreateInput;
  where: ResourceApiWhereInput;
};


export type MutationUpdateReviewArgs = {
  data: ReviewUpdateInput;
  where: ReviewWhereUniqueInput;
};


export type MutationUpdateRoleArgs = {
  data: RolesUpdateInput;
  where: RolesWhereUniqueInput;
};


export type MutationUpdateRoleUserArgs = {
  roleId?: InputMaybe<Scalars['Int']>;
  where: UserWhereUniqueInput;
};


export type MutationUpdateShippingAddressArgs = {
  data: ShippingAddressUpdateInput;
  where: ShippingAddressWhereUniqueInput;
};


export type MutationUpdateShopArgs = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateShopToProductArgs = {
  data: ShopToProductUpdateInput;
  where: ShopToProductWhereUniqueInput;
};


export type MutationUpdateShopV2Args = {
  data: ShopUpdateInput;
  where: ShopWhereUniqueInput;
};


export type MutationUpdateSlotArgs = {
  data: PostReservedSlotInput;
  where: PostReservedSlotWhereInput;
};


export type MutationUpdateStreamerArgs = {
  data: StreamerUpdateInput;
  where: StreamerWhereInput;
};


export type MutationUpdateSystemConfigArgs = {
  data: SystemConfigUpdateInput;
  where: SystemConfigWhereInput;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateVoucherStatusArgs = {
  data: VoucherGiftpopListUpdateInput;
};


export type MutationUpgradeUserArgs = {
  isOfficial: Scalars['Boolean'];
  where: UserWhereUniqueInput;
};


export type MutationUploadShopToProductsArgs = {
  data: Array<ShopToProductCreateInput>;
};


export type MutationUpsertIamArgs = {
  data: IamUpsertInput;
};


export type MutationUpsertUserResourceArgs = {
  data: Array<UserResourceUpsertInput>;
};


export type MutationUpsertUserResourceApiArgs = {
  data: Array<UserResourceApiUpsertInput>;
};


export type MutationUserCreateUserArgs = {
  data: SignUpInput;
};


export type MutationUserRequestCreateArgs = {
  data: UserRequestInput;
};


export type MutationViewPostArgs = {
  where: PostWhereUniqueInput;
};


export type MutationViewedPostArgs = {
  where: PostWhereUniqueInput;
};

export type NearByInput = {
  distance: Scalars['Float'];
  latitude: Scalars['Float'];
  longitude: Scalars['Float'];
};

export type NotificationEvent = {
  __typename?: 'NotificationEvent';
  active?: Maybe<Scalars['Boolean']>;
  alarmTime?: Maybe<Scalars['DateTime']>;
  attachedLink?: Maybe<Scalars['String']>;
  buttonName?: Maybe<Scalars['String']>;
  changeBy?: Maybe<Scalars['String']>;
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  creatorId?: Maybe<Scalars['String']>;
  creatorName?: Maybe<Scalars['String']>;
  dataExcelFile?: Maybe<NotificationEventAttribute>;
  detail?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  nbUserSuccess?: Maybe<Scalars['Int']>;
  nbUserTotal?: Maybe<Scalars['Int']>;
  noEventType?: Maybe<Scalars['String']>;
  noId?: Maybe<Scalars['Int']>;
  receivedObjectData?: Maybe<NotificationObjectByUserInfo>;
  receivedObjectType?: Maybe<Scalars['String']>;
  status?: Maybe<NotificationEventStatus>;
  title?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type NotificationEventAggreate = {
  __typename?: 'NotificationEventAggreate';
  count: Scalars['Int'];
};

export type NotificationEventAttribute = {
  __typename?: 'NotificationEventAttribute';
  name?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export type NotificationEventCheckIdsUsers = {
  ids?: InputMaybe<Array<Scalars['Float']>>;
};

export type NotificationEventConnection = {
  __typename?: 'NotificationEventConnection';
  aggregate: NotificationEventAggreate;
};

export type NotificationEventCreateInput = {
  active?: InputMaybe<Scalars['Boolean']>;
  alarmTime?: InputMaybe<Scalars['DateTime']>;
  attachedLink?: InputMaybe<Scalars['String']>;
  buttonName?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  dataExcelFile?: InputMaybe<Scalars['String']>;
  detail?: InputMaybe<Scalars['String']>;
  noEventType: Scalars['String'];
  receivedObjectData?: InputMaybe<NotificationObjectByUserInfoInput>;
  receivedObjectType?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  user_id?: InputMaybe<Array<Scalars['Float']>>;
};

export type NotificationEventDeleteInput = {
  eventId: Scalars['Float'];
};

export type NotificationEventDetailUsersAggreate = {
  __typename?: 'NotificationEventDetailUsersAggreate';
  count?: Maybe<Scalars['Int']>;
};

export type NotificationEventDetailUsersConnection = {
  __typename?: 'NotificationEventDetailUsersConnection';
  aggregate: NotificationEventDetailUsersAggreate;
};

export type NotificationEventOrderInput = {
  created_at?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  updatedTime?: InputMaybe<OrderBy>;
};

export type NotificationEventRunInput = {
  eventId: Scalars['Float'];
};

export enum NotificationEventStatus {
  Pending = 'PENDING',
  Ready = 'READY',
  Success = 'SUCCESS'
}

export type NotificationEventUpdateInput = {
  active?: InputMaybe<Scalars['Boolean']>;
  alarmTime?: InputMaybe<Scalars['DateTime']>;
  attachedLink?: InputMaybe<Scalars['String']>;
  buttonName?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  dataExcelFile?: InputMaybe<Scalars['String']>;
  detail?: InputMaybe<Scalars['String']>;
  eventId: Scalars['Int'];
  noEventType: Scalars['String'];
  receivedObjectData?: InputMaybe<NotificationObjectByUserInfoInput>;
  receivedObjectType?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  user_id?: InputMaybe<Array<Scalars['Int']>>;
};

export type NotificationEventUserInput = {
  event_id: Scalars['Float'];
  user_id: Array<Scalars['Float']>;
};

export type NotificationEventWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  keyword?: InputMaybe<Scalars['String']>;
  noEventType?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<NotificationEventStatus>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type NotificationObjectByUserInfo = {
  __typename?: 'NotificationObjectByUserInfo';
  fromAge?: Maybe<Scalars['Int']>;
  skinType?: Maybe<SkinTypeObject>;
  toAge?: Maybe<Scalars['Float']>;
  userLevel?: Maybe<UserLevelObject>;
};

export type NotificationObjectByUserInfoInput = {
  fromAge?: InputMaybe<Scalars['Int']>;
  skinType?: InputMaybe<SkinTypeInput>;
  toAge?: InputMaybe<Scalars['Int']>;
  userLevel?: InputMaybe<UserLevelInput>;
};

export enum OauthProvider {
  Apple = 'Apple',
  Facebook = 'Facebook',
  Instagram = 'Instagram'
}

export type Order = {
  __typename?: 'Order';
  address?: Maybe<Address>;
  cartItems: Array<CartItem>;
  cartItemsConnection: CartItemsConnection;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  shop: Shop;
  status: OrderStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type OrderCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type OrderCartItemsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};

export enum OrderBy {
  Asc = 'ASC',
  AscNullsFirst = 'ASC_NULLS_FIRST',
  AscNullsLast = 'ASC_NULLS_LAST',
  Desc = 'DESC',
  DescNullsFirst = 'DESC_NULLS_FIRST',
  DescNullsLast = 'DESC_NULLS_LAST'
}

export type OrderCreateInput = {
  cartItems: CartItemCreateManyWithoutOrderInput;
  shippingAddress: ShippingAddressCreateOneWithoutOrderInput;
  shop: ShopCreateOneWithoutOrderInput;
};

export type OrderCreateOneWithoutCartItemInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderError = Error & {
  __typename?: 'OrderError';
  code: OrderErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum OrderErrorCode {
  OrderCancelled = 'ORDER_CANCELLED',
  OrderConfirmed = 'ORDER_CONFIRMED'
}

export type OrderOrError = CommonError | Order | OrderError;

export type OrderOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum OrderStatus {
  Cancelled = 'CANCELLED',
  Confirmed = 'CONFIRMED',
  Created = 'CREATED'
}

export type OrderUpdateInput = {
  cartItems?: InputMaybe<CartItemUpdateManyWithoutOrderInput>;
  shippingAddress?: InputMaybe<ShippingAddressCreateOneWithoutOrderInput>;
};

export type OrderUpdateOneWithoutCartItemsInput = {
  connect: OrderWhereUniqueInput;
};

export type OrderWhereInput = {
  cartItemsSome?: InputMaybe<CartItemWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<OrderStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type OrderWhereUniqueInput = {
  id: Scalars['Int'];
};

export type OrdersAggregate = {
  __typename?: 'OrdersAggregate';
  count: Scalars['Int'];
};

export type OrdersConnection = {
  __typename?: 'OrdersConnection';
  aggregate: OrdersAggregate;
};

export enum PackagingtUnit {
  Box = 'BOX',
  Pack = 'PACK',
  Pill = 'PILL',
  Tbsp = 'TBSP',
  Tube = 'TUBE'
}

export enum PlatformType {
  Facebook = 'FACEBOOK',
  Instagram = 'INSTAGRAM',
  Tiktok = 'TIKTOK',
  Youtube = 'YOUTUBE'
}

export type Platforms = {
  facebook?: InputMaybe<Scalars['String']>;
  instagram?: InputMaybe<Scalars['String']>;
  tiktok?: InputMaybe<Scalars['String']>;
  youtube?: InputMaybe<Scalars['String']>;
};

export type PlatformsObject = {
  __typename?: 'PlatformsObject';
  facebook?: Maybe<Scalars['String']>;
  instagram?: Maybe<Scalars['String']>;
  tiktok?: Maybe<Scalars['String']>;
  youtube?: Maybe<Scalars['String']>;
};

export type PointEntityUnion = Post | PostComment | Review | User | VoucherOrder;

export type PointHistoriesAggregate = {
  __typename?: 'PointHistoriesAggregate';
  count: Scalars['Int'];
  sum: Scalars['Int'];
};

export type PointHistoriesConnection = {
  __typename?: 'PointHistoriesConnection';
  aggregate: PointHistoriesAggregate;
};

export type PointHistory = {
  __typename?: 'PointHistory';
  createdAt: Scalars['DateTime'];
  entity?: Maybe<PointEntityUnion>;
  id: Scalars['Int'];
  point: Scalars['Int'];
  status: PointStatus;
  type: PointType;
};

export type PointHistoryOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PointHistoryStatistic = {
  __typename?: 'PointHistoryStatistic';
  pointTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export type PointHistoryStatisticWhereInput = {
  fromDate?: InputMaybe<Scalars['DateTime']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export type PointHistoryWhereInput = {
  pointGt?: InputMaybe<Scalars['Int']>;
  redeemable?: InputMaybe<Scalars['Boolean']>;
  review?: InputMaybe<ReviewWhereInput>;
  typeNot?: InputMaybe<PointType>;
  user?: InputMaybe<UserWhereInput>;
};

export enum PointStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PointType {
  ApprovedReviewReport = 'APPROVED_REVIEW_REPORT',
  ApprovedVideo = 'APPROVED_VIDEO',
  BestAnswer = 'BEST_ANSWER',
  DailyActive = 'DAILY_ACTIVE',
  EditProduct = 'EDIT_PRODUCT',
  EventPoints = 'EVENT_POINTS',
  ExchangePointsForCoupons = 'EXCHANGE_POINTS_FOR_COUPONS',
  FirstApprovedVideo = 'FIRST_APPROVED_VIDEO',
  FirstComment = 'FIRST_COMMENT',
  FirstPost = 'FIRST_POST',
  FirstReview = 'FIRST_REVIEW',
  Gift = 'GIFT',
  Have_20Followers = 'HAVE_20_FOLLOWERS',
  MonthlyActive = 'MONTHLY_ACTIVE',
  NormalComment = 'NORMAL_COMMENT',
  NormalReview = 'NORMAL_REVIEW',
  PostHasManyLikes = 'POST_HAS_MANY_LIKES',
  PostWithHighReactions = 'POST_WITH_HIGH_REACTIONS',
  Referee = 'REFEREE',
  Referral = 'REFERRAL',
  ReferralBonus = 'REFERRAL_BONUS',
  ReportedComment = 'REPORTED_COMMENT',
  RequestProduct = 'REQUEST_PRODUCT',
  SignUp = 'SIGN_UP',
  VideoLikes = 'VIDEO_LIKES',
  Voucher = 'VOUCHER',
  WeeklyActive = 'WEEKLY_ACTIVE'
}

export type Post = {
  __typename?: 'Post';
  author: User;
  bestAnswer?: Maybe<PostComment>;
  bookMark?: Maybe<Scalars['Boolean']>;
  category?: Maybe<PostCategory>;
  commentedAt?: Maybe<Scalars['DateTime']>;
  comments?: Maybe<Array<PostComment>>;
  commentsConnection: PostCommentsConnection;
  content?: Maybe<Scalars['String']>;
  countPostLoaderViewedByUser?: Maybe<Scalars['Float']>;
  countViewedPostConnection?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostImagesConnection;
  isBookMark?: Maybe<Scalars['Boolean']>;
  isUnsubscribedByViewer?: Maybe<Scalars['Boolean']>;
  points?: Maybe<Scalars['Int']>;
  postToProducts?: Maybe<Array<PostToProduct>>;
  products: Array<Product>;
  productsConnection: PostProductsConnection;
  /** @deprecated deprecationReason: 'use reactionOfViewer instead', */
  reactStatus: ReactStatus;
  reactionOfViewer: ReactStatus;
  reactionsConnection: PostReactionsConnection;
  review?: Maybe<Review>;
  savedByViewer?: Maybe<Scalars['Boolean']>;
  savedDateByViewer?: Maybe<Scalars['DateTime']>;
  slug?: Maybe<Scalars['String']>;
  status: PostStatus;
  title?: Maybe<Scalars['String']>;
  updatedAt: Scalars['DateTime'];
  video?: Maybe<Video>;
  videos?: Maybe<Array<VideoObject>>;
  viewedPostStatistic?: Maybe<ViewedPostStatistic>;
};


export type PostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostArgsWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostCategory = {
  __typename?: 'PostCategory';
  id: Scalars['Int'];
  isCreatable: Scalars['Boolean'];
  menuOrder: Scalars['Int'];
  translations: Array<PostCategoryTranslation>;
};


export type PostCategoryTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryTranslationWhereInput>;
};

export type PostCategoryCreateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryOrderByInput = {
  id?: InputMaybe<OrderBy>;
  menuOrder?: InputMaybe<OrderBy>;
};

export type PostCategoryTranslation = {
  __typename?: 'PostCategoryTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isOriginal: Scalars['Boolean'];
  language: Scalars['String'];
  name: Scalars['String'];
};

export type PostCategoryTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
};

export type PostCategoryUpdateOneWithoutPostsInput = {
  connect?: InputMaybe<PostCategoryWhereUniqueInput>;
};

export type PostCategoryWhereInput = {
  id?: InputMaybe<Scalars['ID']>;
  idGt?: InputMaybe<Scalars['ID']>;
  idGte?: InputMaybe<Scalars['ID']>;
  idIn?: InputMaybe<Array<Scalars['ID']>>;
  idLt?: InputMaybe<Scalars['ID']>;
  idLte?: InputMaybe<Scalars['ID']>;
  idNot?: InputMaybe<Scalars['ID']>;
  idNotIn?: InputMaybe<Array<Scalars['ID']>>;
  isCreatable?: InputMaybe<Scalars['Boolean']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type PostCategoryWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type PostComment = {
  __typename?: 'PostComment';
  author: User;
  children: Array<PostComment>;
  childrenConnection: PostCommentChildrenConnection;
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: PostCommentImagesConnection;
  parent?: Maybe<PostComment>;
  post: Post;
  /** @deprecated use reactionOfViewer instead */
  reactStatus: ReactStatus;
  reactionConnections: PostCommentReactionsConnection;
  reactionOfViewer: ReactStatus;
  status: PostCommentStatus;
  updatedAt: Scalars['DateTime'];
};


export type PostCommentChildrenArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type PostCommentChildrenConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type PostCommentImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};

export type PostCommentChildrenAggregate = {
  __typename?: 'PostCommentChildrenAggregate';
  count: Scalars['Int'];
};

export type PostCommentChildrenConnection = {
  __typename?: 'PostCommentChildrenConnection';
  aggregate: PostCommentChildrenAggregate;
};

export type PostCommentCreateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  parent?: InputMaybe<PostCommentCreateOneWithoutChildrenInput>;
  post: PostCreateOneWithoutPostCommentsInput;
  review?: InputMaybe<ReviewCreateOneWithoutPostCommentsInput>;
};

export type PostCommentCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<PostCommentCreateWithoutPostInput>>;
};

export type PostCommentCreateOneWithoutChildrenInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostCommentWhereUniqueInput>;
};

export type PostCommentCreateWithoutPostInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageCreateManyWithoutPostCommentInput>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
};

export type PostCommentImagesAggregate = {
  __typename?: 'PostCommentImagesAggregate';
  count: Scalars['Int'];
};

export type PostCommentImagesConnection = {
  __typename?: 'PostCommentImagesConnection';
  aggregate: PostCommentImagesAggregate;
};

export type PostCommentOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type PostCommentReactionsAggregate = {
  __typename?: 'PostCommentReactionsAggregate';
  sum: PostCommentReactionsSum;
};

export type PostCommentReactionsConnection = {
  __typename?: 'PostCommentReactionsConnection';
  aggregate: PostCommentReactionsAggregate;
};

export type PostCommentReactionsSum = {
  __typename?: 'PostCommentReactionsSum';
  value: Scalars['Int'];
};

export type PostCommentReport = {
  __typename?: 'PostCommentReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  postComment: PostComment;
  postCommentId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status: PostCommentReportStatus;
  type: PostCommentReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostCommentReportCreateInput = {
  postComment: PostCommentCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostCommentReportType>;
};

export type PostCommentReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum PostCommentReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostCommentReportType {
  Advertising = 'ADVERTISING',
  Harass = 'HARASS',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Spam = 'SPAM',
  Swearing = 'SWEARING'
}

export type PostCommentReportWhereInput = {
  comment?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
  reasonContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<PostCommentReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type PostCommentReportWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type PostCommentReportsAggregate = {
  __typename?: 'PostCommentReportsAggregate';
  count: Scalars['Int'];
};

export type PostCommentReportsConnection = {
  __typename?: 'PostCommentReportsConnection';
  aggregate: PostCommentReportsAggregate;
};

export enum PostCommentStatus {
  Created = 'CREATED',
  Hidden = 'HIDDEN',
  Reported = 'REPORTED'
}

export type PostCommentUpdateInput = {
  content?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<ImageUpdateManyWithoutPostCommentInput>;
};

export type PostCommentWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  parent?: InputMaybe<PostCommentWhereInput>;
  post?: InputMaybe<PostWhereInput>;
};

export type PostCommentWhereUniqueInput = {
  id: Scalars['Int'];
};

export type PostCommentWhereUserInput = {
  id: Scalars['Int'];
};

export type PostCommentsAggregate = {
  __typename?: 'PostCommentsAggregate';
  count: Scalars['Int'];
};

export type PostCommentsConnection = {
  __typename?: 'PostCommentsConnection';
  aggregate: PostCommentsAggregate;
};

export type PostCreateError = Error & {
  __typename?: 'PostCreateError';
  code: PostCreateErrorCode;
  currentValue?: Maybe<Scalars['Int']>;
  expectedValue?: Maybe<Scalars['Int']>;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum PostCreateErrorCode {
  ExceedMaxSharedPointsPerDay = 'EXCEED_MAX_SHARED_POINTS_PER_DAY',
  HaveNotVerifiedEmail = 'HAVE_NOT_VERIFIED_EMAIL',
  HaveNotVerifiedPhoneNumber = 'HAVE_NOT_VERIFIED_PHONE_NUMBER',
  NotEnoughPoints = 'NOT_ENOUGH_POINTS'
}

export type PostCreateInput = {
  category?: InputMaybe<PostCategoryCreateOneWithoutPostsInput>;
  comments?: InputMaybe<PostCommentCreateManyWithoutPostInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutPostInput>;
  isNewApi?: InputMaybe<Scalars['Boolean']>;
  mentions?: InputMaybe<Array<UserWhereUniqueInput>>;
  points?: InputMaybe<Scalars['Int']>;
  products?: InputMaybe<ProductCreateManyWithoutPostInput>;
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  videoKey?: InputMaybe<Scalars['String']>;
  videos?: InputMaybe<VideoCreateManyWithoutPostInput>;
};

export type PostCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
  create?: InputMaybe<PostCreateWithoutPostCommentsInput>;
};

export type PostCreateOneWithoutReportsInput = {
  connect?: InputMaybe<PostWhereUniqueInput>;
};

export type PostCreateWithoutPostCommentsInput = {
  review?: InputMaybe<ReviewCreateOneWithoutPostInput>;
};

export type PostImagesAggregate = {
  __typename?: 'PostImagesAggregate';
  count: Scalars['Int'];
};

export type PostImagesConnection = {
  __typename?: 'PostImagesConnection';
  aggregate: PostImagesAggregate;
};

export type PostOrError = CommonError | Post | PostCreateError;

export type PostOrderByInput = {
  clicksAggregate?: InputMaybe<Scalars['String']>;
  commentedAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  likesAggregate?: InputMaybe<Scalars['String']>;
  newest?: InputMaybe<OrderBy>;
};

export type PostProductsAggregate = {
  __typename?: 'PostProductsAggregate';
  count: Scalars['Int'];
};

export type PostProductsConnection = {
  __typename?: 'PostProductsConnection';
  aggregate: PostProductsAggregate;
};

export enum PostQueryType {
  Discovery = 'DISCOVERY',
  Following = 'FOLLOWING',
  Normal = 'NORMAL'
}

export type PostReactionsAggregate = {
  __typename?: 'PostReactionsAggregate';
  sum: PostReactionsSum;
};

export type PostReactionsConnection = {
  __typename?: 'PostReactionsConnection';
  aggregate: PostReactionsAggregate;
};

export type PostReactionsSum = {
  __typename?: 'PostReactionsSum';
  value: Scalars['Int'];
};

export type PostRecommentdationEntityUnion = Post | Review;

export type PostReport = {
  __typename?: 'PostReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  post: Post;
  postId: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  status?: Maybe<PostReportStatus>;
  type: PostReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['Int'];
};

export type PostReportCreateInput = {
  post: PostCreateOneWithoutReportsInput;
  reason?: InputMaybe<Scalars['String']>;
  type?: InputMaybe<PostReportType>;
};

export type PostReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum PostReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum PostReportType {
  Advertising = 'ADVERTISING',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Swearing = 'SWEARING'
}

export type PostReportWhereInput = {
  post?: InputMaybe<PostWhereInput>;
  reasonContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<PostReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type PostReportWhereUniqueInput = {
  id: Scalars['Int'];
};

export type PostReportsAggregate = {
  __typename?: 'PostReportsAggregate';
  count: Scalars['Int'];
};

export type PostReportsConnection = {
  __typename?: 'PostReportsConnection';
  aggregate: PostReportsAggregate;
};

export type PostReservedSlot = {
  __typename?: 'PostReservedSlot';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['String'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: PostReservedSlotType;
};

export type PostReservedSlotConnection = {
  __typename?: 'PostReservedSlotConnection';
  total: Scalars['Int'];
};

export type PostReservedSlotCreateInput = {
  post_id: Scalars['Float'];
  slot_name: Scalars['String'];
  slot_position: Scalars['String'];
  type: Scalars['String'];
};

export type PostReservedSlotHistory = {
  __typename?: 'PostReservedSlotHistory';
  id: Scalars['Int'];
  post?: Maybe<Post>;
  post_id: Scalars['Float'];
  slot_id?: Maybe<Scalars['Float']>;
  updated_at: Scalars['DateTime'];
  updated_user: Scalars['Float'];
  user: User;
};

export type PostReservedSlotInput = {
  post_id: Scalars['Float'];
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['String']>;
  type: Scalars['String'];
};

export type PostReservedSlotOrderByInput = {
  id?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export enum PostReservedSlotType {
  Mobile = 'MOBILE',
  Web = 'WEB'
}

export type PostReservedSlotWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  post_id?: InputMaybe<Scalars['Int']>;
  slot_name?: InputMaybe<Scalars['String']>;
  slot_position?: InputMaybe<Scalars['Int']>;
  type?: InputMaybe<Scalars['String']>;
};

export enum PostStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type PostToProduct = {
  __typename?: 'PostToProduct';
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  productId: Scalars['Int'];
  shop?: Maybe<Shop>;
  shopId?: Maybe<Scalars['Int']>;
};

export type PostUpdateInput = {
  category?: InputMaybe<PostCategoryUpdateOneWithoutPostsInput>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutPostInput>;
  products?: InputMaybe<ProductUpdateManyWithoutPostInput>;
  slug?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoUpdateOneWithoutPostInput>;
  videos?: InputMaybe<VideoUpdateManyWithoutPostInput>;
};

export type PostWhereInput = {
  author?: InputMaybe<UserWhereInput>;
  category?: InputMaybe<PostCategoryWhereInput>;
  commentedAtLt?: InputMaybe<Scalars['DateTime']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAtGte?: InputMaybe<Scalars['DateTime']>;
  createdAtLte?: InputMaybe<Scalars['DateTime']>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idGt?: InputMaybe<Scalars['Int']>;
  idGte?: InputMaybe<Scalars['Int']>;
  idLt?: InputMaybe<Scalars['Int']>;
  idLte?: InputMaybe<Scalars['Int']>;
  inId?: InputMaybe<Array<Scalars['Int']>>;
  isBookMark?: InputMaybe<Scalars['Boolean']>;
  productsSome?: InputMaybe<ProductWhereInput>;
  queryType?: InputMaybe<PostQueryType>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<PostStatus>;
  text?: InputMaybe<Scalars['String']>;
  video?: InputMaybe<VideoWhereInput>;
};

export type PostWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type PostsAggregate = {
  __typename?: 'PostsAggregate';
  count: Scalars['Int'];
};

export type PostsConnection = {
  __typename?: 'PostsConnection';
  aggregate: PostsAggregate;
};

export type PresignedUrl = {
  __typename?: 'PresignedUrl';
  key: Scalars['String'];
  url: Scalars['String'];
};

export type Product = {
  __typename?: 'Product';
  alreadySoldByViewer: Scalars['Boolean'];
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Scalars['String']>;
  attributes?: Maybe<ProductAttribute>;
  barcodes: Array<Barcode>;
  barcodesConnection: BarcodesConnection;
  /** @deprecated use numberOfUserBookmarks instead */
  bookmarkedByUser: Scalars['Int'];
  brand: Brand;
  categories: Array<Category>;
  /** @deprecated please use functionsV2 instead */
  functions: Array<SpecialIngredientFunction>;
  /** @deprecated please use functionsConnectionV2 instead, this field will be removed on 01/01/2022 */
  functionsConnection: SpecialIngredientFunctionsConnection;
  functionsConnectionV2: FunctionsConnection;
  functionsV2: Array<Function>;
  highlightProductRanking?: Maybe<HighlightProduct>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredientCautionsConnection: IngredientCautionsConnection;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  isUsedByViewer?: Maybe<Scalars['Boolean']>;
  manufacturer?: Maybe<Scalars['String']>;
  manufacturerAddress?: Maybe<Scalars['String']>;
  measure: Scalars['String'];
  numberOfUserBookmarks: Scalars['Int'];
  numberOfUsers: Scalars['Int'];
  postsConnection: PostsConnection;
  price?: Maybe<Scalars['Int']>;
  priceUnit: Scalars['String'];
  productToIngredients: Array<ProductToIngredient>;
  productToIngredientsConnection: ProductToIngredientsConnection;
  rankings?: Maybe<Array<ProductRanking>>;
  rankingsV2: Array<ProductRanking>;
  reviewedByViewer: Scalars['Boolean'];
  reviews?: Maybe<Array<Review>>;
  reviewsConnection: ReviewsConnection;
  reviewsCountByRate: ReviewsCountByRate;
  soldByShops: Scalars['Boolean'];
  status: ProductStatus;
  stockInfoOfViewer?: Maybe<ShopToProduct>;
  thumbnail?: Maybe<Image>;
  translations: Array<ProductTranslation>;
  type: ProductType;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  videoTitle?: Maybe<Scalars['String']>;
  videoUrl?: Maybe<Scalars['String']>;
  wishedByViewer: Scalars['Boolean'];
};


export type ProductBarcodesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductBarcodesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BarcodeOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BarcodeWhereInput>;
};


export type ProductFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SpecialIngredientFunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SpecialIngredientFunctionWhereInput>;
};


export type ProductFunctionsConnectionV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductFunctionsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type ProductImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type ProductIngredientCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type ProductIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type ProductPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type ProductProductToIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductProductToIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductToIngredientOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductToIngredientWhereInput>;
};


export type ProductRankingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductRankingsV2Args = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductRankingOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRankingWhereInput>;
};


export type ProductReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  energyPerPackage?: Maybe<Scalars['Float']>;
  energyUnit?: Maybe<Unit>;
  netVolumePerPackage?: Maybe<Scalars['Float']>;
  netWeightPerPackage?: Maybe<Scalars['Float']>;
  packagingUnit?: Maybe<PackagingtUnit>;
  rdi?: Maybe<Scalars['Float']>;
  volumeUnit?: Maybe<Unit>;
  weightUnit?: Maybe<Unit>;
};

export type ProductAttributeInput = {
  energyPerPackage?: InputMaybe<Scalars['Float']>;
  energyUnit?: InputMaybe<Unit>;
  netVolumePerPackage?: InputMaybe<Scalars['Float']>;
  netWeightPerPackage?: InputMaybe<Scalars['Float']>;
  packagingUnit?: InputMaybe<PackagingtUnit>;
  rdi?: InputMaybe<Scalars['Float']>;
  volumeUnit?: InputMaybe<Unit>;
  weightUnit?: InputMaybe<Unit>;
};

export type ProductCreateManyWithoutPostInput = {
  connect?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductCreateOneWithoutEditInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreateOneWithoutShopToProductsInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductCreatePayload = CommonError | Product;

export type ProductEdit = {
  __typename?: 'ProductEdit';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  status?: Maybe<ProductEditStatus>;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductEditCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  product: ProductCreateOneWithoutEditInput;
};

export enum ProductEditStatus {
  Created = 'CREATED'
}

export type ProductIngredientInput = {
  amount?: InputMaybe<Scalars['Float']>;
  amountUnit?: InputMaybe<Unit>;
  id: Scalars['ID'];
  level?: InputMaybe<ProductToIngredientLevel>;
  order?: InputMaybe<Scalars['Int']>;
};

export type ProductInput = {
  amount: Scalars['Float'];
  amountUnit: Scalars['String'];
  attribute?: InputMaybe<ProductAttributeInput>;
  barcodes?: InputMaybe<Array<BarcodeInput>>;
  brandId: Scalars['ID'];
  categoryIds: Array<Scalars['ID']>;
  id?: InputMaybe<Scalars['ID']>;
  ingredients?: InputMaybe<Array<ProductIngredientInput>>;
  manufacturer?: InputMaybe<Scalars['String']>;
  manufacturerAddress?: InputMaybe<Scalars['String']>;
  price: Scalars['Int'];
  status?: InputMaybe<ProductStatus>;
  thumbnail: ImageInput;
  translations: Array<ProductTranslationInput>;
  videoTitle?: InputMaybe<Scalars['String']>;
  videoUrl?: InputMaybe<Scalars['String']>;
};

export type ProductOrderByInput = {
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
  productToIngredients?: InputMaybe<ProductToIngredientOrderByInput>;
  reviewsConnection?: InputMaybe<ReviewsAggregateOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ProductRanking = {
  __typename?: 'ProductRanking';
  category: Category;
  product: Product;
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
};

export type ProductRankingOrderByInput = {
  product?: InputMaybe<ProductOrderByInput>;
  ranking?: InputMaybe<OrderBy>;
};

export type ProductRankingWhereInput = {
  category?: InputMaybe<CategoryWhereInput>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductRankingsAggregate = {
  __typename?: 'ProductRankingsAggregate';
  count: Scalars['Int'];
  countV2: Scalars['Int'];
};

export type ProductRankingsConnection = {
  __typename?: 'ProductRankingsConnection';
  aggregate: ProductRankingsAggregate;
};

export type ProductRequest = {
  __typename?: 'ProductRequest';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  isDone: Scalars['Boolean'];
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ProductRequestCreateInput = {
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
};

export type ProductRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductRequestUpdateInput = {
  isDone: Scalars['Boolean'];
};

export type ProductRequestWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type ProductRequestWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ProductRequestsAggregate = {
  __typename?: 'ProductRequestsAggregate';
  count: Scalars['Int'];
};

export type ProductRequestsConnection = {
  __typename?: 'ProductRequestsConnection';
  aggregate: ProductRequestsAggregate;
};

export type ProductSearchOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ProductSearchResult = {
  __typename?: 'ProductSearchResult';
  brandIds: Array<Scalars['Int']>;
  categoryIdParentChildrens: Array<Scalars['Int']>;
  categoryIdRoots: Array<Category>;
  products: Array<Product>;
  total: Scalars['Int'];
};

export enum ProductStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export type ProductToIngredient = {
  __typename?: 'ProductToIngredient';
  attributes?: Maybe<ProductToIngredientAttribute>;
  ingredient: Ingredient;
  ingredientId: Scalars['ID'];
  level?: Maybe<ProductToIngredientLevel>;
  order?: Maybe<Scalars['Int']>;
  productId: Scalars['ID'];
};

export type ProductToIngredientAttribute = {
  __typename?: 'ProductToIngredientAttribute';
  amount?: Maybe<Scalars['Float']>;
  amountUnit?: Maybe<Unit>;
};

export enum ProductToIngredientLevel {
  Major = 'MAJOR',
  Minor = 'MINOR'
}

export type ProductToIngredientOrderByInput = {
  amount?: InputMaybe<OrderBy>;
  ingredient?: InputMaybe<IngredientOrderByInput>;
  level?: InputMaybe<OrderBy>;
  order?: InputMaybe<OrderBy>;
};

export type ProductToIngredientWhereInput = {
  ingredient?: InputMaybe<IngredientWhereInput>;
  level?: InputMaybe<ProductToIngredientLevel>;
  product?: InputMaybe<ProductWhereInput>;
};

export type ProductToIngredientsAggregate = {
  __typename?: 'ProductToIngredientsAggregate';
  count: Scalars['Int'];
};

export type ProductToIngredientsConnection = {
  __typename?: 'ProductToIngredientsConnection';
  aggregate: ProductToIngredientsAggregate;
};

export type ProductTranslation = {
  __typename?: 'ProductTranslation';
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  imageDescription?: Maybe<Scalars['String']>;
  isOriginal: Scalars['Boolean'];
  language: LanguageCode;
  name: Scalars['String'];
  product: Product;
  slug?: Maybe<Scalars['String']>;
};

export type ProductTranslationInput = {
  description: Scalars['String'];
  id?: InputMaybe<Scalars['ID']>;
  imageDescription?: InputMaybe<Scalars['String']>;
  isOriginal?: InputMaybe<Scalars['Boolean']>;
  language: LanguageCode;
  name: Scalars['String'];
  slug: Scalars['String'];
};

export type ProductTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ProductTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum ProductType {
  Cosmetic = 'COSMETIC',
  FunctionalFood = 'FUNCTIONAL_FOOD'
}

export type ProductUpdateManyWithoutPostInput = {
  set?: InputMaybe<Array<ProductWhereUniqueInput>>;
};

export type ProductUpdateOneWithoutEventCommentInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductUpdateOneWithoutLiveStreamProductInput = {
  connect: ProductWhereUniqueInput;
};

export type ProductUpdatePayload = CommonError | Product;

export type ProductWhereInput = {
  _id?: InputMaybe<IntFilter>;
  brand?: InputMaybe<BrandWhereInput>;
  brandFilter?: InputMaybe<Array<Scalars['Int']>>;
  brandTextFilter?: InputMaybe<Scalars['String']>;
  categoriesSome?: InputMaybe<CategoryWhereInput>;
  categoryFilter?: InputMaybe<Array<Scalars['Int']>>;
  fansSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  isFunctionalFood?: InputMaybe<BooleanFilter>;
  isUsedByViewer?: InputMaybe<BooleanFilter>;
  nameContains?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<IntFilter>;
  reviewsSome?: InputMaybe<ReviewWhereInput>;
  shopsSome?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ProductStatus>;
  type?: InputMaybe<ProductType>;
  uid?: InputMaybe<Scalars['ID']>;
  updatedAtGte?: InputMaybe<Scalars['DateTime']>;
};

export type ProductWhereUniqueInput = {
  barcode?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  uid?: InputMaybe<Scalars['ID']>;
};

export type ProductsAggregate = {
  __typename?: 'ProductsAggregate';
  count: Scalars['Int'];
  max: ProductsMax;
};

export type ProductsConnection = {
  __typename?: 'ProductsConnection';
  aggregate: ProductsAggregate;
};

export type ProductsMax = {
  __typename?: 'ProductsMax';
  price: Scalars['Float'];
};

export type Query = {
  __typename?: 'Query';
  analyzeIngredients: IngredientSearchResult;
  bannerImageConnectionAggrs: BannerImageConnection;
  banners: Array<Banner>;
  brand?: Maybe<Brand>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandAdmins?: Maybe<Array<BrandAdmin>>;
  brandTranslations: Array<BrandTranslation>;
  brands: Array<Brand>;
  brandsConnection: BrandsConnection;
  cartItems: Array<CartItem>;
  cast: Cast;
  castComments: Array<CastComment>;
  castCommentsConnection: CastCommentsConnection;
  casts: Array<Cast>;
  castsConnection: CastConnection;
  categories: Array<Category>;
  categoriesConnection: CategoriesConnection;
  category?: Maybe<Category>;
  caution: IngredientCaution;
  cautionTranslations: Array<IngredientCautionTranslation>;
  cautions: Array<IngredientCaution>;
  cautionsConnection: IngredientCautionsConnection;
  changedPostHistorysConnection: PostReservedSlotConnection;
  checkInList: Array<Scalars['Int']>;
  checkPointBonusAvailable: Scalars['Float'];
  childrenUsers: Array<User>;
  childrenUsersConnection: UsersConnection;
  event?: Maybe<Event>;
  eventComments: Array<EventComment>;
  eventCommentsConnection: EventCommentConnection;
  events: Array<Event>;
  eventsConnection: EventConnection;
  feature?: Maybe<Feature>;
  function: Function;
  functionTranslations: Array<FunctionTranslation>;
  functions: Array<Function>;
  functionsConnection: FunctionsConnection;
  funding: Funding;
  fundings: Array<Funding>;
  getBannerImage?: Maybe<BannerImage>;
  getBannerImages: Array<BannerImage>;
  getBrandStore: Array<BrandStore>;
  getChangedPostHistorys: Array<PostReservedSlotHistory>;
  getFeedItems?: Maybe<Array<Recommendation>>;
  getGoodsList: Array<GoodsList>;
  getHistoryVoucher: Array<Voucher>;
  getMyVoucher: Array<Voucher>;
  getNotificationEvent: Array<NotificationEvent>;
  getNotificationEvents: Array<NotificationEvent>;
  getOrderAdmin: Array<VoucherOrder>;
  getOrderDetail: Array<Voucher>;
  getOrdersAdminConnection: VoucherOrderConnection;
  getPointHistoriesStatistic: PointHistoryStatistic;
  getStatisticVoucherOrders: VoucherOrderStatistic;
  getVoucherCategories: Array<VoucherCategories>;
  getVouchersByOrder: Array<Voucher>;
  images: Array<Image>;
  imagesConnection: ImagesConnection;
  ingredient?: Maybe<Ingredient>;
  ingredientTranslations: Array<IngredientTranslation>;
  ingredients: Array<Ingredient>;
  ingredientsConnection: IngredientsConnection;
  liveStream: LiveStreamOrError;
  liveStreamProduct: LiveStreamProduct;
  liveStreamProducts: Array<LiveStreamProduct>;
  liveStreams: Array<LiveStream>;
  livestreamSchedule: LivestreamSchedule;
  livestreamScheduleBanners: Array<LivestreamScheduleBanners>;
  livestreamSchedules: Array<LivestreamSchedule>;
  livestreamSchedulesWithDay: Array<LivestreamScheduleWithDay>;
  livestreamSchedulesWithHour: Array<LivestreamScheduleWithHour>;
  loadReceivedUserByInformation: Array<UserInformation>;
  me: Scalars['Boolean'];
  medias: Array<MediaEntityUnion>;
  mediasConnection: MediasConnection;
  notificationDetailUsersConnection: NotificationEventDetailUsersConnection;
  notificationEventsConnection: NotificationEventConnection;
  order: OrderOrError;
  orders: Array<OrderOrError>;
  ordersConnection: OrdersConnection;
  pointHistories: Array<PointHistory>;
  pointHistoriesConnection: PointHistoriesConnection;
  post?: Maybe<Post>;
  postCategories: Array<PostCategory>;
  postCommentReports: Array<PostCommentReport>;
  postCommentReportsConnection: PostCommentReportsConnection;
  postComments: Array<PostComment>;
  postCommentsConnection: PostCommentsConnection;
  postReports: Array<PostReport>;
  postReportsConnection: PostReportsConnection;
  postReservedSlot?: Maybe<PostReservedSlot>;
  postReservedSlots: Array<PostReservedSlot>;
  posts: Array<Post>;
  postsConnection: PostsConnection;
  product?: Maybe<Product>;
  productRequests: Array<ProductRequest>;
  productRequestsConnection: ProductRequestsConnection;
  productTranslations: Array<ProductTranslation>;
  products: Array<Product>;
  productsConnection: ProductsConnection;
  relatedProducts: Array<Product>;
  resource?: Maybe<Resource>;
  resourceApi?: Maybe<ResourceApi>;
  resourceApis: Array<ResourceApi>;
  resources: Array<Resource>;
  resourcesAPIConnectionAggrs: ResourceApiConnection;
  resourcesConnectionAggrs: ResourceConnection;
  review: Review;
  reviewQuestionSet: ReviewQuestionSet;
  reviewQuestionSets: Array<ReviewQuestionSet>;
  reviewQuestions: Array<ReviewQuestion>;
  reviewQuestionsForProduct: Array<ReviewQuestion>;
  reviewReport: ReviewReport;
  reviewReports: Array<ReviewReport>;
  reviewReportsConnection: ReviewReportsConnection;
  reviews: Array<Review>;
  reviewsConnection: ReviewsConnection;
  roleResource?: Maybe<Array<RoleResource>>;
  roles: Array<Roles>;
  rolesConnectionAggrs: RolesConnection;
  saveEventUserResolver: Scalars['Boolean'];
  savedMedias: Array<MediaEntityUnion>;
  savedMediasConnection: SavedMediasConnection;
  searchBrands: BrandSearchResult;
  searchProducts: ProductSearchResult;
  shippingAddresses: Array<ShippingAddress>;
  shop?: Maybe<Shop>;
  shopToProducts: Array<ShopToProduct>;
  shopToProductsConnection: ShopToProductsConnection;
  shops: Array<Shop>;
  shopsConnection: ShopsConnection;
  streamer: Streamer;
  streamers: Array<Streamer>;
  systemConfig?: Maybe<SystemConfig>;
  systemConfigs: Array<SystemConfig>;
  tagTranslations: Array<TagTranslation>;
  test: Scalars['Boolean'];
  testGetGiftpopCategories: Scalars['String'];
  /** @deprecated use topHighlightShopsV2 instead */
  topHighlightShops: Array<Shop>;
  topHighlightShopsV2: Array<ShopRanking>;
  topRankHighlightProducts: Array<HighlightProduct>;
  trendingBrands: Array<Brand>;
  user?: Maybe<User>;
  userByRole: Array<User>;
  userRequests: Array<UserRequest>;
  userRequestsConnection: UserRequestConnection;
  userResource?: Maybe<UserResource>;
  userResourceApi?: Maybe<UserResourceApi>;
  userResourceApis: Array<UserResourceApi>;
  userResources: Array<UserResource>;
  userResourcesAPIConnectionAggrs: UserResourceApiConnection;
  userResourcesConnectionAggrs: UserResourceConnection;
  users: Array<User>;
  usersConnection: UsersConnection;
  videos: Array<VideoObject>;
  viewCountPost: Scalars['Float'];
  viewReceivedUserByInformation: Array<UserInformationState>;
};


export type QueryAnalyzeIngredientsArgs = {
  productType: ProductType;
  text: Scalars['String'];
};


export type QueryBannerImageConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryBrandArgs = {
  where: BrandWhereUniqueInput;
};


export type QueryBrandAdminArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandAdminsArgs = {
  where: BrandAdminWhereInput;
};


export type QueryBrandTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandTranslationWhereInput>;
};


export type QueryBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryBrandsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QueryCartItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CartItemOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CartItemWhereInput>;
};


export type QueryCastArgs = {
  where: CastWhereUniqueInput;
};


export type QueryCastCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: CastCommentWhereInput;
};


export type QueryCastsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCastsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CastOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CastWhereInput>;
};


export type QueryCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  idFilters?: InputMaybe<Array<Scalars['Int']>>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<CategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CategoryWhereInput>;
};


export type QueryCategoryArgs = {
  where: CategoryWhereUniqueInput;
};


export type QueryCautionArgs = {
  where: CautionWhereUniqueInput;
};


export type QueryCautionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientCautionTranslationWhereInput>;
};


export type QueryCautionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryCautionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<CautionWhereInput>;
};


export type QueryChangedPostHistorysConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryCheckPointBonusAvailableArgs = {
  where: PostCommentWhereUserInput;
};


export type QueryChildrenUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryChildrenUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventCommentWhereInput>;
};


export type QueryEventsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryEventsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<EventOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<EventWhereInput>;
};


export type QueryFunctionArgs = {
  where: FunctionWhereUniqueInput;
};


export type QueryFunctionTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionTranslationWhereInput>;
};


export type QueryFunctionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFunctionsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FunctionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FunctionWhereInput>;
};


export type QueryFundingArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryFundingsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<FundingOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
};


export type QueryGetBannerImageArgs = {
  where: BannerImageWhereInput;
};


export type QueryGetBannerImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BannerImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BannerImageWhereInput>;
};


export type QueryGetBrandStoreArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherStoreWhereInput>;
};


export type QueryGetChangedPostHistorysArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryGetFeedItemsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where: RecommendationPageInput;
};


export type QueryGetGoodsListArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<GoodsListArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<GoodsListWhereInput>;
};


export type QueryGetHistoryVoucherArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherWhereInput>;
};


export type QueryGetMyVoucherArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherWhereInput>;
};


export type QueryGetNotificationEventArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<NotificationEventOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationEventWhereInput>;
};


export type QueryGetNotificationEventsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<NotificationEventOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationEventWhereInput>;
};


export type QueryGetOrderAdminArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryGetOrderDetailArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryGetOrdersAdminConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryGetPointHistoriesStatisticArgs = {
  statisticType: StatisticType;
  where?: InputMaybe<PointHistoryStatisticWhereInput>;
};


export type QueryGetStatisticVoucherOrdersArgs = {
  statisticType: StatisticType;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryGetVoucherCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherCategoriesWhereInput>;
};


export type QueryGetVouchersByOrderArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryImagesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ImageOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ImageWhereInput>;
};


export type QueryIngredientArgs = {
  where: IngredientWhereUniqueInput;
};


export type QueryIngredientTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientTranslationWhereInput>;
};


export type QueryIngredientsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryIngredientsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IngredientOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<IngredientWhereInput>;
};


export type QueryLiveStreamArgs = {
  where: LiveStreamWhereUniqueInput;
};


export type QueryLiveStreamProductArgs = {
  where: LiveStreamProductWhereUniqueInput;
};


export type QueryLiveStreamProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: LiveStreamProductWhereInput;
};


export type QueryLiveStreamsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LiveStreamOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LiveStreamWhereInput>;
};


export type QueryLivestreamScheduleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type QueryLivestreamScheduleBannersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type QueryLivestreamSchedulesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type QueryLivestreamSchedulesWithDayArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type QueryLivestreamSchedulesWithHourArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<LivestreamSchedulesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<LivestreamSchedulesWhereInput>;
};


export type QueryLoadReceivedUserByInformationArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  event_id: Scalars['Float'];
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<MediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: MediaWhereInput;
};


export type QueryNotificationDetailUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  event_id: Scalars['Float'];
  first?: InputMaybe<Scalars['Int']>;
  id_user?: InputMaybe<Scalars['Int']>;
  keyword?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<ViewUserEventStatus>;
};


export type QueryNotificationEventsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<NotificationEventOrderInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<NotificationEventWhereInput>;
};


export type QueryOrderArgs = {
  where: OrderWhereUniqueInput;
};


export type QueryOrdersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryOrdersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<OrderOrderByInput>;
  role?: InputMaybe<Role>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<OrderWhereInput>;
};


export type QueryPointHistoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPointHistoriesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PointHistoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PointHistoryWhereInput>;
};


export type QueryPostArgs = {
  where: PostArgsWhereUniqueInput;
};


export type QueryPostCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCategoryOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCategoryWhereInput>;
};


export type QueryPostCommentReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentReportWhereInput>;
};


export type QueryPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type QueryPostReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReportWhereInput>;
};


export type QueryPostReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReportWhereInput>;
};


export type QueryPostReservedSlotArgs = {
  where: PostReservedSlotWhereInput;
};


export type QueryPostReservedSlotsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostReservedSlotOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostReservedSlotWhereInput>;
};


export type QueryPostsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryPostsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<PostOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostWhereInput>;
};


export type QueryProductArgs = {
  where: ProductWhereUniqueInput;
};


export type QueryProductRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductRequestWhereInput>;
};


export type QueryProductTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductTranslationWhereInput>;
};


export type QueryProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryRelatedProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<Array<ProductOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryResourceArgs = {
  where: ResourceWhereInput;
};


export type QueryResourceApiArgs = {
  where: ResourceApiWhereInput;
};


export type QueryResourceApisArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceApiOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};


export type QueryResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};


export type QueryResourcesApiConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceApiOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};


export type QueryResourcesConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};


export type QueryReviewArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewQuestionSetArgs = {
  where: ReviewQuestionSetWhereUniqueInput;
};


export type QueryReviewQuestionSetsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionSetWhereInput>;
};


export type QueryReviewQuestionsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewQuestionOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewQuestionWhereInput>;
};


export type QueryReviewQuestionsForProductArgs = {
  productId: Scalars['ID'];
};


export type QueryReviewReportArgs = {
  where: ReviewReportWhereUniqueInput;
};


export type QueryReviewReportsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};


export type QueryReviewsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type QueryRoleResourceArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RoleResourceOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RoleResourceWhereUniqueInput>;
};


export type QueryRolesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};


export type QueryRolesConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};


export type QuerySaveEventUserResolverArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  event_id: Scalars['Float'];
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QuerySavedMediasArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySavedMediasConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SavedMediaOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SavedMediaWhereInput>;
};


export type QuerySearchBrandsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type QuerySearchProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ProductSearchOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text: Scalars['String'];
  where?: InputMaybe<ProductWhereInput>;
};


export type QueryShippingAddressesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShippingAddressOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShippingAddressWhereInput>;
};


export type QueryShopArgs = {
  where: ShopWhereUniqueInput;
};


export type QueryShopToProductsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopToProductsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopToProductOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where: ShopToProductWhereInput;
};


export type QueryShopsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryShopsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ShopOrderByInput>;
  orderBys?: InputMaybe<Array<ShopOrderByInput>>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ShopWhereInput>;
};


export type QueryStreamerArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<StreamerWhereInput>;
};


export type QueryStreamersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BaseOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<StreamerWhereInput>;
};


export type QuerySystemConfigArgs = {
  where: SystemConfigWhereInput;
};


export type QuerySystemConfigsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<SystemConfigOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<SystemConfigWhereInput>;
};


export type QueryTagTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagTranslationWhereInput>;
};


export type QueryTestGetGiftpopCategoriesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VoucherArgsOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VoucherOrderWhereInput>;
};


export type QueryTopHighlightShopsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopHighlightShopsV2Args = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryTopRankHighlightProductsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type QueryUserArgs = {
  dbDirect?: InputMaybe<Scalars['Boolean']>;
  where: UserWhereUniqueInput;
};


export type QueryUserByRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};


export type QueryUserRequestsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUserRequestsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserRequestOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserRequestWhereInput>;
};


export type QueryUserResourceArgs = {
  where: UserResourceApiWhereInput;
};


export type QueryUserResourceApiArgs = {
  where: UserResourceApiWhereInput;
};


export type QueryUserResourceApisArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type QueryUserResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type QueryUserResourcesApiConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type QueryUserResourcesConnectionAggrsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type QueryUsersArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryUsersConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<UserOrderByInput>;
  parentUserId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserWhereInput>;
};


export type QueryVideosArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<VideoOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<VideosWhereInput>;
};


export type QueryViewCountPostArgs = {
  where: PostWhereUniqueInput;
};


export type QueryViewReceivedUserByInformationArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  event_id: Scalars['Float'];
  first?: InputMaybe<Scalars['Int']>;
  id_user?: InputMaybe<Scalars['Int']>;
  keyword?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<ViewUserEventStatus>;
};

export enum ReactStatus {
  Dislike = 'DISLIKE',
  Like = 'LIKE',
  None = 'NONE'
}

export type Recommendation = {
  __typename?: 'Recommendation';
  id: Scalars['Int'];
  recommendation?: Maybe<PostRecommentdationEntityUnion>;
  type?: Maybe<Scalars['Float']>;
};

export type RecommendationPageInput = {
  page?: InputMaybe<Scalars['Int']>;
  userID?: InputMaybe<Scalars['Int']>;
};

export type Resource = {
  __typename?: 'Resource';
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUser?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Boolean']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
};

export type ResourceApi = {
  __typename?: 'ResourceAPI';
  createdAt?: Maybe<Scalars['DateTime']>;
  createdUser?: Maybe<Scalars['String']>;
  displayName?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  method?: Maybe<ApiMethod>;
  name?: Maybe<Scalars['String']>;
  resource?: Maybe<Array<Resource>>;
  resourceId?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['DateTime']>;
  urlPath?: Maybe<Scalars['String']>;
};


export type ResourceApiResourceArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};

export type ResourceApiConnection = {
  __typename?: 'ResourceAPIConnection';
  aggregate: ResourceApiConnectionAggregate;
};

export type ResourceApiConnectionAggregate = {
  __typename?: 'ResourceAPIConnectionAggregate';
  count: Scalars['Int'];
};

export type ResourceApiCreateInput = {
  displayName?: InputMaybe<Scalars['String']>;
  method?: InputMaybe<ApiMethod>;
  name?: InputMaybe<Scalars['String']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  urlPath?: InputMaybe<Scalars['String']>;
};

export type ResourceApiError = Error & {
  __typename?: 'ResourceAPIError';
  code: ResourceApiErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ResourceApiErrorCode {
  DisplayNameAlreadyInUse = 'DISPLAY_NAME_ALREADY_IN_USE',
  NameAlreadyInUse = 'NAME_ALREADY_IN_USE'
}

export type ResourceApiOrError = ResourceApi | ResourceApiError;

export type ResourceApiOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ResourceApiWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type ResourceConnection = {
  __typename?: 'ResourceConnection';
  aggregate: ResourceConnectionAggregate;
};

export type ResourceConnectionAggregate = {
  __typename?: 'ResourceConnectionAggregate';
  count: Scalars['Int'];
};

export type ResourceCreateInput = {
  name: Scalars['String'];
};

export type ResourceOrError = CommonError | Resource;

export type ResourceOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type ResourceWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type Review = {
  __typename?: 'Review';
  answers?: Maybe<Array<ReviewAnswer>>;
  bookMark?: Maybe<Scalars['Boolean']>;
  commentsConnection: PostCommentsConnection;
  content: Scalars['String'];
  countViewedReview?: Maybe<Scalars['Float']>;
  createdAt: Scalars['DateTime'];
  hashtags?: Maybe<Array<Hashtag>>;
  id: Scalars['Int'];
  images: Array<Image>;
  imagesConnection: ReviewImagesConnection;
  isDeleted: Scalars['Boolean'];
  isRecommended?: Maybe<Scalars['Boolean']>;
  post?: Maybe<Post>;
  postComments: Array<PostComment>;
  product: Product;
  rate: Scalars['Int'];
  reactionOfViewer: ReactStatus;
  reactionsConnection: ReviewReactionsConnection;
  reports?: Maybe<Array<ReviewReport>>;
  reportsConnection: ReviewReportsConnection;
  shop?: Maybe<Shop>;
  status: ReviewStatus;
  title?: Maybe<Scalars['String']>;
  uid: Scalars['ID'];
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ReviewCommentsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<PostCommentOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<PostCommentWhereInput>;
};


export type ReviewImagesConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
};


export type ReviewPostCommentsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type ReviewReportsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewReportOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewReportWhereInput>;
};

export type ReviewAnswer = {
  __typename?: 'ReviewAnswer';
  answerOption: ReviewAnswerOption;
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
};

export type ReviewAnswerInput = {
  id?: InputMaybe<Scalars['ID']>;
  reviewAnswerOptionId: Scalars['ID'];
  reviewId?: InputMaybe<Scalars['ID']>;
};

export type ReviewAnswerOption = {
  __typename?: 'ReviewAnswerOption';
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  order: Scalars['Int'];
  question: ReviewQuestion;
  summary: Scalars['String'];
};

export type ReviewAnswerOptionInput = {
  content: Scalars['String'];
  id?: InputMaybe<Scalars['ID']>;
  order: Scalars['Int'];
  summary: Scalars['String'];
};

export type ReviewAvgOrderByInput = {
  rate?: InputMaybe<OrderBy>;
};

export type ReviewCreateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageCreateManyWithoutReviewInput>;
  product: ProductWhereUniqueInput;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopCreateOneWithoutReviewsInput>;
};

export type ReviewCreateOneWithoutPostCommentsInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutPostInput = {
  connect?: InputMaybe<ReviewWhereUniqueInput>;
};

export type ReviewCreateOneWithoutReportInput = {
  connect: ReviewWhereUniqueInput;
};

export type ReviewCreatePayload = CommonError | Review;

export type ReviewImagesAggregate = {
  __typename?: 'ReviewImagesAggregate';
  count: Scalars['Int'];
};

export type ReviewImagesConnection = {
  __typename?: 'ReviewImagesConnection';
  aggregate: ReviewImagesAggregate;
};

export type ReviewInput = {
  answers?: InputMaybe<Array<ReviewAnswerInput>>;
  content?: InputMaybe<Scalars['String']>;
  hashtags?: InputMaybe<Array<HashtagInput>>;
  id?: InputMaybe<Scalars['ID']>;
  images?: InputMaybe<Array<ImageInput>>;
  isRecommended?: InputMaybe<Scalars['Boolean']>;
  productId: Scalars['ID'];
  rate: Scalars['Int'];
  shopId?: InputMaybe<Scalars['ID']>;
  title?: InputMaybe<Scalars['String']>;
};

export type ReviewOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
  reactionsConnection?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ReviewQuestion = {
  __typename?: 'ReviewQuestion';
  answerOptions: Array<ReviewAnswerOption>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  language: LanguageCode;
  order: Scalars['Int'];
  reviewQuestionSetId: Scalars['ID'];
};

export type ReviewQuestionCreatePayload = CommonError | ReviewQuestion;

export type ReviewQuestionDeletePayload = CommonError | ReviewQuestion;

export type ReviewQuestionInput = {
  answerOptions?: InputMaybe<Array<ReviewAnswerOptionInput>>;
  content: Scalars['String'];
  id?: InputMaybe<Scalars['ID']>;
  language: LanguageCode;
  order: Scalars['Int'];
  questionSetId?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionOrderByInput = {
  order?: InputMaybe<OrderBy>;
};

export type ReviewQuestionSet = {
  __typename?: 'ReviewQuestionSet';
  id: Scalars['ID'];
  name: Scalars['String'];
  questions: Array<ReviewQuestion>;
};

export type ReviewQuestionSetCreatePayload = CommonError | ReviewQuestionSet;

export type ReviewQuestionSetDeletePayload = CommonError | ReviewQuestionSet;

export type ReviewQuestionSetInput = {
  name?: InputMaybe<Scalars['String']>;
  questions?: InputMaybe<Array<ReviewQuestionInput>>;
};

export type ReviewQuestionSetUpdatePayload = CommonError | ReviewQuestionSet;

export type ReviewQuestionSetWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionSetWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type ReviewQuestionUpdatePayload = CommonError | ReviewQuestion;

export type ReviewQuestionWhereInput = {
  language?: InputMaybe<LanguageCodeFilter>;
  reviewQuestionSetId?: InputMaybe<StringFilter>;
};

export type ReviewReactionsAggregate = {
  __typename?: 'ReviewReactionsAggregate';
  count: Scalars['Int'];
};

export type ReviewReactionsAggregateOrderByInput = {
  count?: InputMaybe<OrderBy>;
};

export type ReviewReactionsConnection = {
  __typename?: 'ReviewReactionsConnection';
  aggregate: ReviewReactionsAggregate;
};

export type ReviewReactionsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewReactionsAggregateOrderByInput>;
};

export type ReviewReport = {
  __typename?: 'ReviewReport';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  review: Review;
  status: ReviewReportStatus;
  type: ReviewReportType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type ReviewReportCreateInput = {
  reason?: InputMaybe<Scalars['String']>;
  review: ReviewCreateOneWithoutReportInput;
  type: ReviewReportType;
};

export type ReviewReportOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export enum ReviewReportStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Rejected = 'REJECTED'
}

export enum ReviewReportType {
  Advertising = 'ADVERTISING',
  NotRelated = 'NOT_RELATED',
  Other = 'OTHER',
  PersonalInformation = 'PERSONAL_INFORMATION',
  Repeat = 'REPEAT',
  Simple = 'SIMPLE',
  Swearing = 'SWEARING'
}

export type ReviewReportWhereInput = {
  reasonContains?: InputMaybe<Scalars['String']>;
  review?: InputMaybe<ReviewWhereInput>;
  status?: InputMaybe<ReviewReportStatus>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewReportWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ReviewReportsAggregate = {
  __typename?: 'ReviewReportsAggregate';
  count: Scalars['Int'];
};

export type ReviewReportsConnection = {
  __typename?: 'ReviewReportsConnection';
  aggregate: ReviewReportsAggregate;
};

export enum ReviewStatus {
  Approved = 'APPROVED',
  Created = 'CREATED',
  Hidden = 'HIDDEN'
}

export type ReviewUpdateInput = {
  content: Scalars['String'];
  hashtags?: InputMaybe<Array<HashtagInput>>;
  images?: InputMaybe<ImageUpdateManyWithoutReviewInput>;
  rate: Scalars['Int'];
  shop?: InputMaybe<ShopUpdateOneWithoutReviewsInput>;
};

export type ReviewUpdatePayload = CommonError | Review;

export type ReviewWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  hashtagsSome?: InputMaybe<HashtagWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  idIn?: InputMaybe<Array<Scalars['Int']>>;
  idIsNull?: InputMaybe<Scalars['Boolean']>;
  isDeleted?: InputMaybe<Scalars['Boolean']>;
  isQuickRating?: InputMaybe<Scalars['Boolean']>;
  product?: InputMaybe<ProductWhereInput>;
  rateOR?: InputMaybe<Array<IntFilter>>;
  shop?: InputMaybe<ShopWhereInput>;
  status?: InputMaybe<ReviewStatus>;
  statusIn?: InputMaybe<Array<ReviewStatus>>;
  user?: InputMaybe<UserWhereInput>;
};

export type ReviewWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export type ReviewsAggregate = {
  __typename?: 'ReviewsAggregate';
  avg: ReviewsAvg;
  count: Scalars['Int'];
};

export type ReviewsAggregateOrderByInput = {
  avg?: InputMaybe<ReviewAvgOrderByInput>;
  count?: InputMaybe<OrderBy>;
};

export type ReviewsAvg = {
  __typename?: 'ReviewsAvg';
  rate: Scalars['Float'];
};

export type ReviewsConnection = {
  __typename?: 'ReviewsConnection';
  aggregate: ReviewsAggregate;
};

export type ReviewsConnectionOrderByInput = {
  aggregate?: InputMaybe<ReviewsAggregateOrderByInput>;
};

export type ReviewsCountByRate = {
  __typename?: 'ReviewsCountByRate';
  five?: Maybe<Scalars['Int']>;
  four?: Maybe<Scalars['Int']>;
  one?: Maybe<Scalars['Int']>;
  three?: Maybe<Scalars['Int']>;
  two?: Maybe<Scalars['Int']>;
};

export enum Role {
  Admin = 'ADMIN',
  Anonymous = 'ANONYMOUS',
  Shop = 'SHOP',
  User = 'USER'
}

export type RoleCreateInput = {
  name: Scalars['String'];
};

export type RoleResource = {
  __typename?: 'RoleResource';
  createdAt?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  resource?: Maybe<Resource>;
  resourceId?: Maybe<Scalars['Int']>;
  role_id?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['Boolean']>;
};

export type RoleResourceCreateInput = {
  roleId?: InputMaybe<Scalars['Int']>;
  roleResourceData: Array<RoleResourceDataInput>;
};

export type RoleResourceDataInput = {
  resourceId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<Scalars['Boolean']>;
};

export type RoleResourceOrderByInput = {
  id?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type RoleResourceWhereUniqueInput = {
  resourceId?: InputMaybe<Scalars['Int']>;
  roleId?: InputMaybe<Scalars['Int']>;
};

export enum RoleStatus {
  Active = 'ACTIVE',
  Deactive = 'DEACTIVE'
}

export type Roles = {
  __typename?: 'Roles';
  createdAt: Scalars['DateTime'];
  createdUser?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  name: Scalars['String'];
};

export type RolesConnection = {
  __typename?: 'RolesConnection';
  aggregate: RolesConnectionAggregate;
};

export type RolesConnectionAggregate = {
  __typename?: 'RolesConnectionAggregate';
  count: Scalars['Int'];
};

export type RolesOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type RolesUpdateInput = {
  createdUser?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
};

export type RolesWhereInput = {
  _id?: InputMaybe<IntFilter>;
  createdUser?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type RolesWhereUniqueInput = {
  id: Scalars['Int'];
};

export type SavedMediaOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
};

export type SavedMediaWhereInput = {
  createdAt?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<MediaType>;
};

export type SavedMediasAggregate = {
  __typename?: 'SavedMediasAggregate';
  count: Scalars['Int'];
};

export type SavedMediasConnection = {
  __typename?: 'SavedMediasConnection';
  aggregate: SavedMediasAggregate;
};

export type ShippingAddress = {
  __typename?: 'ShippingAddress';
  address: Scalars['String'];
  country: Scalars['String'];
  createdAt: Scalars['DateTime'];
  default: Scalars['Boolean'];
  distance?: Maybe<Scalars['Float']>;
  district: Scalars['String'];
  fullName: Scalars['String'];
  id: Scalars['Int'];
  location?: Maybe<Location>;
  phoneNumber?: Maybe<Scalars['String']>;
  province: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  ward: Scalars['String'];
};


export type ShippingAddressDistanceArgs = {
  location: LocationInput;
};

export type ShippingAddressCreateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressCreateOneWithoutOrderInput = {
  connect: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressCreateOneWithoutShopInput = {
  create: ShippingAddressCreateInput;
};

export type ShippingAddressOrError = CommonError | ShippingAddress;

export type ShippingAddressOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type ShippingAddressUpdateInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressUpdateOneWithoutShopInput = {
  create?: InputMaybe<ShippingAddressCreateInput>;
  update?: InputMaybe<ShippingAddressUpdateWithWhereUniqueWithoutShopInput>;
};

export type ShippingAddressUpdateWithWhereUniqueWithoutShopInput = {
  data: ShippingAddressUpdateWithoutShopInput;
  where: ShippingAddressWhereUniqueInput;
};

export type ShippingAddressUpdateWithoutShopInput = {
  address: Scalars['String'];
  country: Scalars['String'];
  default?: InputMaybe<Scalars['Boolean']>;
  district: Scalars['String'];
  districtId?: InputMaybe<Scalars['ID']>;
  fullName: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  province: Scalars['String'];
  provinceId?: InputMaybe<Scalars['ID']>;
  ward: Scalars['String'];
  wardId?: InputMaybe<Scalars['ID']>;
};

export type ShippingAddressWhereInput = {
  default?: InputMaybe<Scalars['Boolean']>;
  district?: InputMaybe<Scalars['String']>;
  nearBy?: InputMaybe<NearByInput>;
  province?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<UserWhereInput>;
};

export type ShippingAddressWhereUniqueInput = {
  id: Scalars['Int'];
};

export type Shop = {
  __typename?: 'Shop';
  cover?: Maybe<Image>;
  createdAt: Scalars['DateTime'];
  description?: Maybe<Scalars['String']>;
  externalLinks?: Maybe<Array<ExternalLink>>;
  id: Scalars['Int'];
  name: Scalars['String'];
  pickupAddress?: Maybe<ShippingAddress>;
  reviewsConnection: ReviewsConnection;
  shopRanking?: Maybe<ShopRanking>;
  status: ShopStatus;
  updatedAt: Scalars['DateTime'];
  user: User;
};


export type ShopReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};

export type ShopCreateInput = {
  avatar?: InputMaybe<ImageCreateOneWithoutShopInput>;
  cover?: InputMaybe<ImageCreateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressCreateOneWithoutShopInput>;
};

export type ShopCreateOneWithoutOrderInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopCreateOneWithoutShopToProductsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopMutationError = Error & {
  __typename?: 'ShopMutationError';
  code: ShopMutationErrorCode;
  message: Scalars['String'];
  status: ErrorStatus;
};

export enum ShopMutationErrorCode {
  NameAlreadyInUse = 'NAME_ALREADY_IN_USE',
  YouAreNotTheOwner = 'YOU_ARE_NOT_THE_OWNER'
}

export type ShopOrMutationError = CommonError | Shop | ShopMutationError;

export type ShopOrderByInput = {
  id?: InputMaybe<OrderBy>;
  status?: InputMaybe<OrderBy>;
  updatedAt?: InputMaybe<OrderBy>;
};

export type ShopRanking = {
  __typename?: 'ShopRanking';
  ranking: Scalars['Int'];
  rankingChange?: Maybe<Scalars['Int']>;
  shop: Shop;
};

export enum ShopStatus {
  Approved = 'APPROVED',
  Blocked = 'BLOCKED',
  Created = 'CREATED'
}

export type ShopToProduct = {
  __typename?: 'ShopToProduct';
  createdAt: Scalars['DateTime'];
  externalLinks?: Maybe<Array<ExternalLink>>;
  price?: Maybe<Scalars['Int']>;
  product: Product;
  shop: Shop;
  stockQuantity?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['DateTime'];
};

export type ShopToProductCreateInput = {
  externalLink?: InputMaybe<ExternalLinkCreateManyWithoutShopToProductInput>;
  price: Scalars['Int'];
  product: ProductCreateOneWithoutShopToProductsInput;
  shop: ShopCreateOneWithoutShopToProductsInput;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  price?: InputMaybe<OrderBy>;
};

export type ShopToProductUpdateInput = {
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopToProductInput>;
  price?: InputMaybe<Scalars['Int']>;
  stockQuantity?: InputMaybe<Scalars['Int']>;
};

export type ShopToProductWhereInput = {
  ecommerceSite?: InputMaybe<EcommerceSite>;
  price?: InputMaybe<IntFilter>;
  product?: InputMaybe<ProductWhereInput>;
  shop?: InputMaybe<ShopWhereInput>;
};

export type ShopToProductWhereUniqueInput = {
  product: ProductWhereUniqueInput;
  shop: ShopWhereUniqueInput;
};

export type ShopToProductsAggregate = {
  __typename?: 'ShopToProductsAggregate';
  count: Scalars['Int'];
};

export type ShopToProductsConnection = {
  __typename?: 'ShopToProductsConnection';
  aggregate: ShopToProductsAggregate;
};

export type ShopUpdateInput = {
  avatar?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  cover?: InputMaybe<ImageUpdateOneWithoutShopInput>;
  description?: InputMaybe<Scalars['String']>;
  externalLink?: InputMaybe<ExternalLinkUpdateManyWithoutShopInput>;
  name: Scalars['String'];
  pickupAddress?: InputMaybe<ShippingAddressUpdateOneWithoutShopInput>;
};

export type ShopUpdateOneWithoutReviewsInput = {
  connect: ShopWhereUniqueInput;
};

export type ShopWhereInput = {
  _id?: InputMaybe<IntFilter>;
  ecommerceSite?: InputMaybe<EcommerceSite>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
  owner?: InputMaybe<UserWhereUniqueInput>;
  pickupAddress?: InputMaybe<ShippingAddressWhereInput>;
  productsSome?: InputMaybe<ProductWhereInput>;
  status?: InputMaybe<ShopStatus>;
};

export type ShopWhereUniqueInput = {
  id: Scalars['Int'];
};

export type ShopsAggregate = {
  __typename?: 'ShopsAggregate';
  count: Scalars['Int'];
};

export type ShopsConnection = {
  __typename?: 'ShopsConnection';
  aggregate: ShopsAggregate;
};

export type SignUpInput = {
  accessToken?: InputMaybe<Scalars['String']>;
  account?: InputMaybe<Scalars['String']>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  oauthProvider?: InputMaybe<OauthProvider>;
  otp?: InputMaybe<Scalars['Int']>;
  password?: InputMaybe<Scalars['String']>;
  referralCode?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
};

export enum SkinType {
  Dry = 'DRY',
  Mixed = 'MIXED',
  Neutral = 'NEUTRAL',
  Oily = 'OILY',
  Sensitive = 'SENSITIVE'
}

export type SkinTypeInput = {
  ALL?: InputMaybe<Scalars['Boolean']>;
  DRY?: InputMaybe<Scalars['Boolean']>;
  MIXED?: InputMaybe<Scalars['Boolean']>;
  NEUTRAL?: InputMaybe<Scalars['Boolean']>;
  OILY?: InputMaybe<Scalars['Boolean']>;
  SENSITIVE?: InputMaybe<Scalars['Boolean']>;
};

export type SkinTypeObject = {
  __typename?: 'SkinTypeObject';
  ALL: Scalars['Boolean'];
  DRY: Scalars['Boolean'];
  MIXED: Scalars['Boolean'];
  NEUTRAL: Scalars['Boolean'];
  OILY: Scalars['Boolean'];
  SENSITIVE: Scalars['Boolean'];
};

export type SpecialIngredientFunction = {
  __typename?: 'SpecialIngredientFunction';
  id: Scalars['Int'];
  symbolUrl?: Maybe<Scalars['String']>;
  type: SpecialIngredientFunctionType;
};

export type SpecialIngredientFunctionOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type SpecialIngredientFunctionTranslationWhereInput = {
  language?: InputMaybe<LanguageCode>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export enum SpecialIngredientFunctionType {
  Advantage = 'ADVANTAGE',
  Disadvantage = 'DISADVANTAGE',
  Recommendation = 'RECOMMENDATION'
}

export type SpecialIngredientFunctionWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  ingredientsSome?: InputMaybe<IngredientWhereInput>;
  translationsSome?: InputMaybe<SpecialIngredientFunctionTranslationWhereInput>;
  type?: InputMaybe<SpecialIngredientFunctionType>;
};

export type SpecialIngredientFunctionsAggregate = {
  __typename?: 'SpecialIngredientFunctionsAggregate';
  count: Scalars['Int'];
  id: Scalars['Int'];
};

export type SpecialIngredientFunctionsConnection = {
  __typename?: 'SpecialIngredientFunctionsConnection';
  aggregate: SpecialIngredientFunctionsAggregate;
  id: Scalars['Int'];
};

export enum StatisticType {
  Day = 'DAY',
  Month = 'MONTH',
  Quarter = 'QUARTER',
  Week = 'WEEK'
}

export type Streamer = {
  __typename?: 'Streamer';
  avatar?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  name: Scalars['String'];
  platforms?: Maybe<PlatformsObject>;
};

export type StreamerCreateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  platforms?: InputMaybe<Platforms>;
};

export type StreamerUpdateInput = {
  avatar?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  platforms?: InputMaybe<Platforms>;
};

export type StreamerWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  textSearch?: InputMaybe<Scalars['String']>;
};

export type StringBox = {
  __typename?: 'StringBox';
  value: Scalars['String'];
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<StringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type SystemConfig = {
  __typename?: 'SystemConfig';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigCreateInput = {
  name: Scalars['String'];
  value: Scalars['String'];
};

export type SystemConfigOrderByInput = {
  createdAt?: InputMaybe<OrderBy>;
  id?: InputMaybe<OrderBy>;
};

export type SystemConfigUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['String']>;
};

export type SystemConfigWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<StringFilter>;
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['Int'];
  translations: Array<TagTranslation>;
};


export type TagTranslationsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<TagTranslationOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<TagTranslationWhereInput>;
};

export type TagCreateManyWithoutEventInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  set?: InputMaybe<Array<TagWhereUniqueInput>>;
};

export type TagTranslation = {
  __typename?: 'TagTranslation';
  content: Scalars['String'];
  id: Scalars['Int'];
  language: LanguageCode;
  tag: Tag;
};

export type TagTranslationOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type TagTranslationWhereInput = {
  contentContains?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Float']>;
  language?: InputMaybe<LanguageCode>;
};

export type TagUpdateManyWithoutEventInput = {
  connect?: InputMaybe<Array<TagWhereUniqueInput>>;
  disconnect?: InputMaybe<Array<TagWhereUniqueInput>>;
  set?: InputMaybe<Array<TagWhereUniqueInput>>;
};

export type TagWhereUniqueInput = {
  id?: InputMaybe<Scalars['Int']>;
};

export enum Unit {
  Cfu = 'CFU',
  Cal = 'cal',
  G = 'g',
  Kcal = 'kcal',
  L = 'l',
  Mcg = 'mcg',
  Mg = 'mg',
  Micronutrient = 'micronutrient',
  Ml = 'ml'
}

export type User = {
  __typename?: 'User';
  account: Scalars['String'];
  /** @deprecated use account instead */
  accountName: Scalars['String'];
  /** @deprecated This field will be removed in Sep, 2021 */
  address?: Maybe<Scalars['String']>;
  avatar?: Maybe<Image>;
  baumannAnswers?: Maybe<Array<BaumannAnswer>>;
  baumannSkinType?: Maybe<BaumannSkinType>;
  birthYear?: Maybe<Scalars['Int']>;
  brandAdmin?: Maybe<BrandAdmin>;
  brandsFollowingConnection: BrandsFollowingConnection;
  createdAt: Scalars['DateTime'];
  currentLevel?: Maybe<Level>;
  deleteNote?: Maybe<Scalars['String']>;
  deleteReason?: Maybe<UserDeleteReason>;
  displayName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  emailVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  firstName?: Maybe<Scalars['String']>;
  followersConnection: FollowersConnection;
  followingsConnection: FollowingsConnection;
  gender?: Maybe<Gender>;
  id: Scalars['Int'];
  isAnonymous: Scalars['Boolean'];
  isBlocked: Scalars['Boolean'];
  isBlockedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowedByViewer instead */
  isFollowed?: Maybe<Scalars['Boolean']>;
  isFollowedByViewer?: Maybe<Scalars['Boolean']>;
  /** @deprecated use isFollowingViewer instead */
  isFollowing?: Maybe<Scalars['Boolean']>;
  isFollowingViewer?: Maybe<Scalars['Boolean']>;
  isOfficial?: Maybe<Scalars['Boolean']>;
  isVerified: Scalars['Boolean'];
  /** @deprecated This field will be removed in Sep, 2021 */
  lastName?: Maybe<Scalars['String']>;
  levelPoints: Scalars['Int'];
  nationality?: Maybe<Scalars['String']>;
  nextLevel?: Maybe<Level>;
  phoneNumber?: Maybe<Scalars['String']>;
  postsConnection: PostsConnection;
  profiles: Array<UserProfile>;
  redeemablePoints: Scalars['Int'];
  referralCode: Scalars['ID'];
  reviewReactionsConnection: ReviewReactionsConnection;
  reviewsConnection: ReviewsConnection;
  role: Scalars['String'];
  roleId?: Maybe<Scalars['Int']>;
  shippingAddressUser?: Maybe<ShippingAddress>;
  shop?: Maybe<Shop>;
  /** @deprecated This field will be replaced by Baumann skin types in Sep, 2021 */
  skinType: SkinType;
  socialMediaAccounts?: Maybe<Array<OauthProvider>>;
  totalPoints: Scalars['Int'];
  totalReactPoints?: Maybe<Scalars['Int']>;
  type?: Maybe<AccountType>;
  uid: Scalars['ID'];
  userPath?: Maybe<Scalars['String']>;
  userResources?: Maybe<Array<UserResource>>;
  userRole?: Maybe<Array<Roles>>;
  wishedProductsConnection: ProductsConnection;
  /** @deprecated This field will be removed in Sep, 2021 */
  zipCode?: Maybe<Scalars['Int']>;
};


export type UserBrandsFollowingConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<BrandOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<BrandWhereInput>;
};


export type UserProfilesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<UserProfileWhereInput>;
};


export type UserReviewsConnectionArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ReviewOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<ReviewWhereInput>;
};


export type UserUserResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};


export type UserUserRoleArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<RolesOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<RolesWhereInput>;
};

export type UserCreateUserAuthPayload = {
  __typename?: 'UserCreateUserAuthPayload';
  token: Scalars['String'];
  user: User;
};

export type UserCreateUserAuthPayloadOrError = AuthError | CommonError | UserCreateUserAuthPayload;

export type UserDeleteInput = {
  note?: InputMaybe<Scalars['String']>;
  reason: UserDeleteReason;
};

export enum UserDeleteReason {
  AppIsSlowAndBugs = 'APP_IS_SLOW_AND_BUGS',
  InformationIsNotHelpful = 'INFORMATION_IS_NOT_HELPFUL',
  IngredientAreNotAccurate = 'INGREDIENT_ARE_NOT_ACCURATE',
  NeverWinGiveaways = 'NEVER_WIN_GIVEAWAYS',
  Other = 'OTHER',
  PostsAreUnhelpfulAndOrOffensive = 'POSTS_ARE_UNHELPFUL_AND_OR_OFFENSIVE',
  ReviewsAreNotHelpful = 'REVIEWS_ARE_NOT_HELPFUL',
  TakeToMuchEffortToEarnPoints = 'TAKE_TO_MUCH_EFFORT_TO_EARN_POINTS'
}

export type UserInformation = {
  __typename?: 'UserInformation';
  account_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
};

export type UserInformationState = {
  __typename?: 'UserInformationState';
  account_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
  is_sent?: Maybe<Scalars['String']>;
};

export type UserLevelInput = {
  ALL?: InputMaybe<Scalars['Boolean']>;
  LV1?: InputMaybe<Scalars['Boolean']>;
  LV2?: InputMaybe<Scalars['Boolean']>;
  LV3?: InputMaybe<Scalars['Boolean']>;
  LV4?: InputMaybe<Scalars['Boolean']>;
  LV5?: InputMaybe<Scalars['Boolean']>;
  LV6?: InputMaybe<Scalars['Boolean']>;
  LV7?: InputMaybe<Scalars['Boolean']>;
  LV8?: InputMaybe<Scalars['Boolean']>;
};

export type UserLevelObject = {
  __typename?: 'UserLevelObject';
  ALL: Scalars['Boolean'];
  LV1: Scalars['Boolean'];
  LV2: Scalars['Boolean'];
  LV3: Scalars['Boolean'];
  LV4: Scalars['Boolean'];
  LV5: Scalars['Boolean'];
  LV6: Scalars['Boolean'];
  LV7: Scalars['Boolean'];
  LV8: Scalars['Boolean'];
};

export type UserOrderByInput = {
  id?: InputMaybe<OrderBy>;
  reviewReactions?: InputMaybe<ReviewReactionsConnectionOrderByInput>;
  reviews?: InputMaybe<ReviewsConnectionOrderByInput>;
};

export type UserProfile = {
  __typename?: 'UserProfile';
  displayName: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  externalId: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  provider: OauthProvider;
  userId: Scalars['Int'];
};

export type UserProfileWhereInput = {
  provider?: InputMaybe<OauthProvider>;
  user?: InputMaybe<UserWhereInput>;
};

export type UserRequest = {
  __typename?: 'UserRequest';
  assignedIDsProductByAdminForUser?: Maybe<Array<Scalars['Float']>>;
  changeBy?: Maybe<Scalars['String']>;
  content: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  images?: Maybe<Array<Scalars['String']>>;
  ownerId: Scalars['Int'];
  product?: Maybe<Product>;
  pushedNotificationAt?: Maybe<Scalars['DateTime']>;
  requestedObjectId?: Maybe<Scalars['Float']>;
  status?: Maybe<UserRequestStatus>;
  statusSendNotification?: Maybe<UserRequestSendNotificationStatus>;
  type: UserRequestType;
  updatedAt: Scalars['DateTime'];
  user: User;
};

export type UserRequestAggreate = {
  __typename?: 'UserRequestAggreate';
  count: Scalars['Int'];
};

export type UserRequestConnection = {
  __typename?: 'UserRequestConnection';
  aggregate: UserRequestAggreate;
};

export type UserRequestInput = {
  brandName?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  images?: InputMaybe<Array<Scalars['String']>>;
  productName?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  type: UserRequestType;
};

export type UserRequestOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum UserRequestSendNotificationStatus {
  Done = 'Done',
  Fail = 'Fail',
  Inprogress = 'Inprogress',
  Unsent = 'Unsent'
}

export enum UserRequestStatus {
  Closed = 'Closed',
  Pending = 'Pending',
  Receive = 'Receive'
}

export enum UserRequestStatusAssign {
  Assigned = 'Assigned',
  Unassigned = 'Unassigned'
}

export enum UserRequestType {
  AddNewProduct = 'AddNewProduct',
  AddNewProductIngredients = 'AddNewProductIngredients',
  EditProductInformation = 'EditProductInformation',
  OthersShop = 'OthersShop',
  ShopReviewty = 'ShopReviewty'
}

export type UserRequestUpdateInput = {
  ids?: InputMaybe<Array<Scalars['Int']>>;
};

export type UserRequestUpdateResponse = {
  __typename?: 'UserRequestUpdateResponse';
  code: Scalars['String'];
  message: Scalars['String'];
  status: Scalars['Float'];
};

export type UserRequestUpdateStatusInput = {
  status: UserRequestStatus;
};

export type UserRequestWhereInput = {
  brandId?: InputMaybe<Scalars['Float']>;
  contentContains?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<Scalars['Int']>;
  idContains?: InputMaybe<Array<Scalars['Float']>>;
  notificationDeliveryStatus?: InputMaybe<UserRequestSendNotificationStatus>;
  ownerId?: InputMaybe<Scalars['Int']>;
  productId?: InputMaybe<Scalars['Int']>;
  productNameContains?: InputMaybe<Scalars['String']>;
  requestedObjectId?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<UserRequestStatus>;
  statusAssign?: InputMaybe<UserRequestStatusAssign>;
  type?: InputMaybe<UserRequestType>;
  userNameContains?: InputMaybe<Scalars['String']>;
};

export type UserRequestWhereUniqueInput = {
  id: Scalars['Int'];
};

export type UserResource = {
  __typename?: 'UserResource';
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['Int']>;
  isSaved?: Maybe<Scalars['Boolean']>;
  resourceId?: Maybe<Scalars['Int']>;
  resources?: Maybe<Array<Resource>>;
  status?: Maybe<RoleStatus>;
  userId?: Maybe<Scalars['Int']>;
  userResourceAPIs?: Maybe<Array<UserResourceApi>>;
};


export type UserResourceResourcesArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};


export type UserResourceUserResourceApIsArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<IamOrderByInput>;
  parentId?: InputMaybe<Scalars['Int']>;
  parentRoleId?: InputMaybe<Scalars['Int']>;
  resourceId?: InputMaybe<Scalars['Int']>;
  skip?: InputMaybe<Scalars['Int']>;
  userId?: InputMaybe<Scalars['Int']>;
};

export type UserResourceApi = {
  __typename?: 'UserResourceAPI';
  apiId?: Maybe<Scalars['Int']>;
  createdAt?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['Int']>;
  isSaved?: Maybe<Scalars['Boolean']>;
  resourceApis?: Maybe<Array<ResourceApi>>;
  resourceId?: Maybe<Scalars['Int']>;
  status?: Maybe<RoleStatus>;
  userId?: Maybe<Scalars['Int']>;
};


export type UserResourceApiResourceApisArgs = {
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  id?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  orderBy?: InputMaybe<ResourceApiOrderByInput>;
  skip?: InputMaybe<Scalars['Int']>;
  text?: InputMaybe<Scalars['String']>;
};

export type UserResourceApiConnection = {
  __typename?: 'UserResourceAPIConnection';
  aggregate: UserResourceApiConnectionAggregate;
};

export type UserResourceApiConnectionAggregate = {
  __typename?: 'UserResourceAPIConnectionAggregate';
  count: Scalars['Int'];
};

export type UserResourceApiUpsertInput = {
  apiId?: InputMaybe<Scalars['Int']>;
  resourceId: Scalars['Int'];
  status: RoleStatus;
  userId: Scalars['Int'];
};

export type UserResourceApiWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  user_id?: InputMaybe<Scalars['Int']>;
};

export type UserResourceConnection = {
  __typename?: 'UserResourceConnection';
  aggregate: UserResourceConnectionAggregate;
};

export type UserResourceConnectionAggregate = {
  __typename?: 'UserResourceConnectionAggregate';
  count: Scalars['Int'];
};

export type UserResourceUpsertInput = {
  resourceId: Scalars['Int'];
  status: RoleStatus;
  userId: Scalars['Int'];
};

export type UserUpdateInput = {
  account?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<ImageUpdateManyWithoutUserInput>;
  baumannSkinType?: InputMaybe<BaumannSkinType>;
  birthYear?: InputMaybe<Scalars['Int']>;
  displayName?: InputMaybe<Scalars['String']>;
  fullName?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Gender>;
  phoneNumber?: InputMaybe<Scalars['String']>;
  role?: InputMaybe<Scalars['String']>;
  roleId?: InputMaybe<Scalars['Int']>;
  skinType?: InputMaybe<SkinType>;
  userPath?: InputMaybe<Scalars['String']>;
  zipCode?: InputMaybe<Scalars['Int']>;
};

export type UserWhereInput = {
  account?: InputMaybe<StringFilter>;
  accountContains?: InputMaybe<Scalars['String']>;
  accountStartsWith?: InputMaybe<Scalars['String']>;
  baumannSkinType?: InputMaybe<BaumannSkinTypeFilter>;
  birthYearOR?: InputMaybe<Array<IntFilter>>;
  displayName?: InputMaybe<StringFilter>;
  followersSome?: InputMaybe<UserWhereInput>;
  followingsSome?: InputMaybe<UserWhereInput>;
  gender?: InputMaybe<Gender>;
  genderIn?: InputMaybe<Array<Gender>>;
  id?: InputMaybe<Scalars['Int']>;
  isBlockedByViewer?: InputMaybe<Scalars['Boolean']>;
  skinType?: InputMaybe<SkinType>;
  skinTypeIn?: InputMaybe<Array<SkinType>>;
  userOnly?: InputMaybe<Scalars['Boolean']>;
};

export type UserWhereUniqueInput = {
  account?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  isOfficial?: InputMaybe<Scalars['Boolean']>;
};

export type UsersAggregate = {
  __typename?: 'UsersAggregate';
  count: Scalars['Int'];
};

export type UsersConnection = {
  __typename?: 'UsersConnection';
  aggregate: UsersAggregate;
};

export type Video = {
  __typename?: 'Video';
  details?: Maybe<VideoDetails>;
  status: VideoStatus;
  thumbnail?: Maybe<VideoThumbnail>;
};

export type VideoCreateManyWithoutPostInput = {
  create?: InputMaybe<Array<VideoCreateWithoutPostInput>>;
};

export type VideoCreateWithoutPostInput = {
  name?: InputMaybe<Scalars['String']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideoDetails = {
  __typename?: 'VideoDetails';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoDetailsUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoObject = {
  __typename?: 'VideoObject';
  createdAt: Scalars['DateTime'];
  id: Scalars['Int'];
  name: Scalars['String'];
  thumbnailUrl: Scalars['String'];
  url: Scalars['String'];
};

export type VideoOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export enum VideoStatus {
  Complete = 'COMPLETE',
  Error = 'ERROR',
  InProgressing = 'IN_PROGRESSING'
}

export type VideoThumbnail = {
  __typename?: 'VideoThumbnail';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoThumbnailUpdateInput = {
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type VideoUpdateManyWithoutPostInput = {
  update?: InputMaybe<Array<VideosUpdateWithoutPostInput>>;
};

export type VideoUpdateOneWithoutPostInput = {
  update: VideoUpdateWithoutPostInput;
};

export type VideoUpdateWithoutPostInput = {
  details: VideoDetailsUpdateInput;
  thumbnail: VideoThumbnailUpdateInput;
};

export type VideoWhereInput = {
  nonNull?: InputMaybe<Scalars['Boolean']>;
  status?: InputMaybe<VideoStatus>;
};

export type VideosUpdateWithoutPostInput = {
  id: Scalars['Int'];
  name?: InputMaybe<Scalars['String']>;
  thumbnailUrl?: InputMaybe<Scalars['String']>;
  url: Scalars['String'];
};

export type VideosWhereInput = {
  review?: InputMaybe<ReviewWhereInput>;
};

export enum ViewUserEventStatus {
  Fail = 'FAIL',
  Pending = 'PENDING',
  Success = 'SUCCESS'
}

export type ViewedPostStatistic = {
  __typename?: 'ViewedPostStatistic';
  click?: Maybe<Scalars['Int']>;
  multiple?: Maybe<Scalars['Int']>;
  single?: Maybe<Scalars['Int']>;
};

export type Voucher = {
  __typename?: 'Voucher';
  barcode?: Maybe<Scalars['String']>;
  brandCode?: Maybe<Scalars['String']>;
  createdAt: Scalars['DateTime'];
  expiryDate: Scalars['DateTime'];
  goodsCode: Scalars['String'];
  goodsList: GoodsList;
  id: Scalars['Int'];
  imgCoupon?: Maybe<Scalars['String']>;
  imgOrigin?: Maybe<Scalars['String']>;
  note: Scalars['String'];
  password?: Maybe<Scalars['String']>;
  pinNo: Scalars['String'];
  pinUrl: Scalars['String'];
  redeemOnDeepLink?: Maybe<Scalars['String']>;
  rpPoint?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  storeCode?: Maybe<Scalars['String']>;
  trId: Scalars['String'];
  usedTime?: Maybe<Scalars['DateTime']>;
  user: User;
  userId: Scalars['Int'];
};

export type VoucherArgsOrderByInput = {
  id?: InputMaybe<OrderBy>;
};

export type VoucherBrand = {
  __typename?: 'VoucherBrand';
  code: Scalars['String'];
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherCategories = {
  __typename?: 'VoucherCategories';
  code: Scalars['String'];
  id: Scalars['Int'];
  name: Scalars['String'];
  name_en: Scalars['String'];
  name_kr: Scalars['String'];
};

export type VoucherCategoriesWhereInput = {
  code?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['Int']>;
  nameContains?: InputMaybe<Scalars['String']>;
};

export type VoucherGiftpopListUpdateInput = {
  voucherList: Array<VoucherGiftpopUpdateInput>;
};

export type VoucherGiftpopUpdateInput = {
  storeCode: Scalars['String'];
  useStatus: Scalars['String'];
  usedTime: Scalars['String'];
  voucherId: Scalars['String'];
};

export type VoucherIssuesInput = {
  goodsId: Scalars['String'];
  quantity: Scalars['Int'];
};

export type VoucherOrder = {
  __typename?: 'VoucherOrder';
  createdAt?: Maybe<Scalars['DateTime']>;
  giftpopMessage?: Maybe<Scalars['String']>;
  giftpopStatus?: Maybe<Scalars['Boolean']>;
  goodsId?: Maybe<Scalars['String']>;
  goodsList: GoodsList;
  id: Scalars['Int'];
  nbVouchers?: Maybe<Scalars['Int']>;
  pointAfter?: Maybe<Scalars['Int']>;
  pointBefore?: Maybe<Scalars['Int']>;
  user: User;
  userId?: Maybe<Scalars['Int']>;
};

export type VoucherOrderAggreate = {
  __typename?: 'VoucherOrderAggreate';
  count: Scalars['Int'];
};

export type VoucherOrderConnection = {
  __typename?: 'VoucherOrderConnection';
  aggregate: VoucherOrderAggreate;
};

export type VoucherOrderStatistic = {
  __typename?: 'VoucherOrderStatistic';
  nbVoucherTotal?: Maybe<Array<Scalars['Int']>>;
  pointTotal?: Maybe<Array<Scalars['Int']>>;
  time?: Maybe<Array<Scalars['String']>>;
};

export type VoucherOrderWhereInput = {
  categoriesCode?: InputMaybe<Scalars['String']>;
  fromDate?: InputMaybe<Scalars['DateTime']>;
  id?: InputMaybe<Scalars['Int']>;
  keyword?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Boolean']>;
  toDate?: InputMaybe<Scalars['DateTime']>;
};

export enum VoucherStatus {
  Expired = 'Expired',
  InUse = 'InUse',
  Used = 'Used'
}

export type VoucherStoreWhereInput = {
  brandCode: Scalars['String'];
};

export type VoucherVendor = {
  __typename?: 'VoucherVendor';
  id: Scalars['Int'];
  logo: Scalars['String'];
  name: Scalars['String'];
};

export type VoucherWhereInput = {
  id?: InputMaybe<Scalars['Int']>;
  status?: InputMaybe<VoucherStatus>;
};

export type FundingDetailQueryVariables = Exact<{
  where: FundingWhereInput;
}>;


export type FundingDetailQuery = { __typename?: 'Query', funding: { __typename?: 'Funding', id: number, coverUrl: string, shortDescription: string, longDescription: string, productId: number, startDate: any, endDate: any, salePrice: number, product: { __typename?: 'Product', price?: number | null, id: number, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } } } };

export type FundingsQueryVariables = Exact<{
  skip?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['ID']>;
  before?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  where?: InputMaybe<FundingWhereInput>;
  orderBy?: InputMaybe<FundingOrderByInput>;
}>;


export type FundingsQuery = { __typename?: 'Query', fundings: Array<{ __typename?: 'Funding', id: number, coverUrl: string, shortDescription: string, longDescription: string, productId: number, startDate: any, endDate: any, salePrice: number, product: { __typename?: 'Product', price?: number | null, id: number, thumbnail?: { __typename?: 'Image', url: string, small: { __typename?: 'Image', url: string } } | null, translations: Array<{ __typename?: 'ProductTranslation', name: string }>, brand: { __typename?: 'Brand', logoUrl: string, id: number, smallLogoUrl?: string | null, translations: Array<{ __typename?: 'BrandTranslation', name: string }> } } }> };


export const FundingDetailDocument = gql`
    query fundingDetail($where: FundingWhereInput!) {
  funding(where: $where) {
    id
    coverUrl
    shortDescription
    longDescription
    productId
    startDate
    endDate
    salePrice
    product {
      price
      thumbnail {
        small: fixed(width: SMALL) {
          url
        }
        url
      }
      translations(where: {language: VI}) {
        name
      }
      brand {
        logoUrl
        smallLogoUrl: fixedLogoUrl(width: SMALL)
        id
        translations {
          name
        }
      }
      id
    }
  }
}
    `;

/**
 * __useFundingDetailQuery__
 *
 * To run a query within a React component, call `useFundingDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingDetailQuery({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useFundingDetailQuery(baseOptions: Apollo.QueryHookOptions<FundingDetailQuery, FundingDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingDetailQuery, FundingDetailQueryVariables>(FundingDetailDocument, options);
      }
export function useFundingDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingDetailQuery, FundingDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingDetailQuery, FundingDetailQueryVariables>(FundingDetailDocument, options);
        }
export type FundingDetailQueryHookResult = ReturnType<typeof useFundingDetailQuery>;
export type FundingDetailLazyQueryHookResult = ReturnType<typeof useFundingDetailLazyQuery>;
export type FundingDetailQueryResult = Apollo.QueryResult<FundingDetailQuery, FundingDetailQueryVariables>;
export const FundingsDocument = gql`
    query fundings($skip: Int = 0, $after: ID, $before: ID, $first: Int = 10, $last: Int, $where: FundingWhereInput, $orderBy: FundingOrderByInput) {
  fundings(
    skip: $skip
    after: $after
    before: $before
    first: $first
    last: $last
    where: $where
    orderBy: $orderBy
  ) {
    id
    coverUrl
    shortDescription
    longDescription
    productId
    startDate
    endDate
    salePrice
    product {
      price
      thumbnail {
        small: fixed(width: SMALL) {
          url
        }
        url
      }
      translations(where: {language: VI}) {
        name
      }
      brand {
        logoUrl
        smallLogoUrl: fixedLogoUrl(width: SMALL)
        id
        translations(where: {language: VI}) {
          name
        }
      }
      id
    }
  }
}
    `;

/**
 * __useFundingsQuery__
 *
 * To run a query within a React component, call `useFundingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFundingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFundingsQuery({
 *   variables: {
 *      skip: // value for 'skip'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      first: // value for 'first'
 *      last: // value for 'last'
 *      where: // value for 'where'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */
export function useFundingsQuery(baseOptions?: Apollo.QueryHookOptions<FundingsQuery, FundingsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FundingsQuery, FundingsQueryVariables>(FundingsDocument, options);
      }
export function useFundingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FundingsQuery, FundingsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FundingsQuery, FundingsQueryVariables>(FundingsDocument, options);
        }
export type FundingsQueryHookResult = ReturnType<typeof useFundingsQuery>;
export type FundingsLazyQueryHookResult = ReturnType<typeof useFundingsLazyQuery>;
export type FundingsQueryResult = Apollo.QueryResult<FundingsQuery, FundingsQueryVariables>;